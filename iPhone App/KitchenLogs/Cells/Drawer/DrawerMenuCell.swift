//
//  DrawerMenuCell.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/18/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import SDWebImage

class DrawerMenuCell: DrawerSuperCell {

    @IBOutlet weak var menuLabel: UILabel!
    @IBOutlet weak var iconIV: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    public override func configureView(menuItem : DrawerMenuVC.MenuViewItem) {
        menuLabel.text = menuItem.name
        iconIV.sd_setImage(with: URL(string: menuItem.icon!))
    }
    
}
