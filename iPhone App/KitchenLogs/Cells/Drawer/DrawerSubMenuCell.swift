//
//  DrawerSubMenuCell.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/18/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit

class DrawerSubMenuCell: DrawerSuperCell {
    
    @IBOutlet weak var subMenuLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public override func configureView(menuItem : DrawerMenuVC.MenuViewItem) {
        subMenuLabel.text = menuItem.name
    }
    
}
