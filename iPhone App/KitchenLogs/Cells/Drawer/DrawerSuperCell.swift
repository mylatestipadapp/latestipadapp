//
//  DrawerSuperCell.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/18/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import KLCommon

protocol DrawerCellDelegate : class {
    func onMenuClicked(item : DrawerMenuVC.MenuViewItem)
    func onSubMenuClicked(item : DrawerMenuVC.MenuViewItem)
}

class DrawerSuperCell: UITableViewCell {

    public weak var delegate : DrawerCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    public func configureView(menuItem : DrawerMenuVC.MenuViewItem) {
        // implementation is in childs
    }
    
    
}
