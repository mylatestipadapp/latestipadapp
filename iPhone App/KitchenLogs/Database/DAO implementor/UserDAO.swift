//
//  UserDAO.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/19/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import KLCommon
import RealmSwift

public class UserDAO : IUserContract {
    
    public func getUser() -> User {
        let realm : Realm! = try! Realm()
        let daoUser : DAOUser = realm.objects(DAOUser.self)[0]
        return daoUser.getUser()
    }
    
    public func saveUser(user: User) {
        let realm : Realm! = try! Realm()
        try! realm.write {
            realm.add(DAOUser.mapUser(user: user))
        }
    }
    
    public func updateUser(user: User) {
        
    }
    
    public func deleteUser(id:String?) -> Bool {
        let realm : Realm! = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(DAOUser.self))
        }
        return false;
    }
}
