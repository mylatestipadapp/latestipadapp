//
//  DAOUser.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/19/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import RealmSwift
import KLCommon

class DAOUser: Object {
    
    dynamic var id : String?
    dynamic var name : String?
    dynamic var phone : String?
    dynamic var email : String?
    dynamic var businessId : String?
    dynamic var userTypeStr : String?
    dynamic var json : String?
    
    
    public static func mapUser(user: User) -> DAOUser {
        let daoUser : DAOUser! = DAOUser()
        daoUser.name = user.name
        daoUser.id = user.id
        daoUser.phone = user.phone
        daoUser.email = user.email
        daoUser.businessId = user.businessId
        daoUser.userTypeStr = daoUser.getStringType(user: user)
        // daoUser.json = user.toJSON()
        return daoUser
    }
    
    public func getUser() -> User {
        let user : User = User()
        user.name = self.name
        user.id = self.id
        user.email = self.email
        user.phone = self.phone
        user.businessId = self.businessId
        user.userType = userType()
        //        user.business =
        return user
    }
    
    public func getStringType(user : User) -> String {
        if user.userType == UserType.ADMIN {
            return "Admin"
        } else if user.userType == UserType.EMPLOY {
            return "Employ"
        } else {
            return "Manager"
        }
    }
    
    public func userType() -> UserType {
        if self.userTypeStr == "Admin"{ return UserType.ADMIN}
        else if self.userTypeStr == "Employ"{ return UserType.EMPLOY}
        else {return UserType.MANAGER}
    }
}
