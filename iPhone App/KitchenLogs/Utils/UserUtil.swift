//
//  UserUtil.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/8/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import KLCommon

class UserUtil: NSObject {
    
    public static func isUserLoggedIn() -> Bool {
        if (!String.isNilOrEmpty(UserDefaultsUtil.getUserToken()) /* && UserDAO().getUser() != nil*/) {return true
        }
        return false
    }
    
    
    public static func performLogout() {
        UserDefaultsUtil.saveUserToken(token: "")
        UserDAO().deleteUser(id:"")
    }
    
    public static func getUserId()-> String {
        return ""
    }
    
    public static func getBusinessId() -> String {
        return ""
    }
    
    public static func getLocationId() -> String {
        return ""
    }
    
    public static func getUser() -> User {
        return UserDAO().getUser();
    }
}
