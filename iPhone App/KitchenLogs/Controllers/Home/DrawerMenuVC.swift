//
//  DrawerMenuVC.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/12/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import KLCommon

protocol DrawerVCDelegate {
    func cellDidSelectWithIndexAndtitle(index: NSInteger,title: String)
    func timeAndDateDidTap()
}

class DrawerMenuVC: UIViewController, DrawerCellDelegate, UITableViewDelegate, UITableViewDataSource {
    private static var MENU_CELL_IDEBNTIFIER :String = "menu_cell_identifier"
    private static var SUBMENU_CELL_IDEBNTIFIER :String = "submenu_cell_identifier"
    
    var drawerMenuItems : [DrawerMenuResponse.Menu] = []
    var drawerMenuDataset : [MenuViewItem] = []
    
    @IBOutlet weak var drawerTableView: UITableView?
    @IBOutlet weak var userNameLabel: UILabel?
    var delegate : DrawerVCDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchDrawerItems()
        setupDrawer()
    }
    
    
    
    func onMenuClicked(item : DrawerMenuVC.MenuViewItem) {
        
    }
    
    func onSubMenuClicked(item : DrawerMenuVC.MenuViewItem) {
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return drawerMenuDataset.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item : MenuViewItem = self.drawerMenuDataset[indexPath.row]
        if item.type == "menu" {
            if item.subMenus != nil && (item.subMenus?.count)! > 0  {
                if item.isCollapsed == true {
                    item.isCollapsed = false
                } else {
                    item.isCollapsed = true
                }
                self.refreshDrawerMenuItems()
            } else {
                self.onMenuClicked(item : item)
            }
        } else {
            self.onSubMenuClicked(item: item)
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item : MenuViewItem = self.drawerMenuDataset[indexPath.row]
        var cell : DrawerSuperCell! = DrawerSuperCell()
        if item.type == "menu" {
            cell = drawerTableView?.dequeueReusableCell(withIdentifier: DrawerMenuVC.MENU_CELL_IDEBNTIFIER, for: indexPath) as? DrawerMenuCell
        } else {
            cell = drawerTableView?.dequeueReusableCell(withIdentifier: DrawerMenuVC.SUBMENU_CELL_IDEBNTIFIER, for: indexPath) as? DrawerSubMenuCell
        }
        cell.delegate = self
        cell.configureView(menuItem: item)
        return cell
    }
    
    
    @IBAction func onLogoutClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "Logout", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onPrivacyPolicyClicked(_ sender: UIButton) {
        let nextViewController : WebPageViewController? = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webVC") as? WebPageViewController
        nextViewController?.title = "Privacy Policy"
        nextViewController?.webURL = "https://www.kitchenlo.gs/privacy-policy/"
        self.present(nextViewController ?? UIViewController(), animated:true, completion:nil)
    }
    
    @IBAction func onTandCClicked(_ sender: UIButton) {
        let nextViewController : WebPageViewController? = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webVC") as? WebPageViewController
        nextViewController?.title = "Terms and Conditions"
        nextViewController?.webURL = "https://www.kitchenlo.gs/terms/"
        self.present(nextViewController ?? UIViewController(), animated:true, completion:nil)
    }
    
    func setupDrawer(){
        let user : User = UserDAO().getUser()
        if user != nil && !String.isNilOrEmpty(user.name){
            self.userNameLabel?.text = user.name
        } else {
            self.userNameLabel?.text = "Guest"
        }
    }
    
    private func performLogout() {
        UserUtil.performLogout()
        let loginVC : LoginVC? = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        self.present(loginVC ?? UIViewController(), animated:true, completion: nil)
    }
    
    private func fetchDrawerItems() {
        let _ = HomeService.getDrawerMenu { (response, error) in
            if(error == nil) {
                let resp : DrawerMenuResponse = response as! DrawerMenuResponse
                if ((resp.code == nil && resp.data != nil) || resp.code == 200) {
                    self.drawerMenuItems = []
                    self.drawerMenuItems.append(contentsOf: resp.data!)
                    self.setupMenu()
                } else {
                    self.view.makeToast(resp.message)
                }
            } else {
                self.view.makeToast((error?.1)!)
            }
        }
    }
    
    private func setupMenu(){
        drawerMenuDataset = []
        for menu in self.drawerMenuItems {
            drawerMenuDataset.append(MenuViewItem(name: menu.label!, type: "menu", menu: menu, subMenu: nil, icon:menu.icon, isCollapsed:false, submenus:menu.subMenu))
            if menu.isCollapsed! && menu.subMenu != nil && (menu.subMenu?.count)! > 0 {
                for subMenu in menu.subMenu! {
                    drawerMenuDataset.append(MenuViewItem(name: subMenu.label!, type: "submenu", menu: nil, subMenu:subMenu, icon:subMenu.icon, isCollapsed:false, submenus:nil))
                }
            }
        }
        drawerTableView?.reloadData()
    }
    
    private func refreshDrawerMenuItems(){
        var tempData : [MenuViewItem] = []
        tempData.append(contentsOf: self.drawerMenuDataset);
        self.drawerMenuDataset.removeAll()
        for menuItem in tempData {
            if (menuItem.type == "menu") {
                self.drawerMenuDataset.append(menuItem)
                if menuItem.isCollapsed && menuItem.subMenus != nil && menuItem.subMenus!.count > 0 {
                    for subMenu in menuItem.subMenus! {
                        self.drawerMenuDataset.append(MenuViewItem(name: subMenu.label!, type: "submenu", menu: nil, subMenu:subMenu, icon:subMenu.icon, isCollapsed:false, submenus:nil))
                    }
                }
            }
        }
        self.drawerTableView?.reloadData()
    }
    
    
    class MenuViewItem {
        public var name : String = ""
        public var type : String = "menu"
        public var menu : DrawerMenuResponse.Menu?
        public var subMenu : DrawerMenuResponse.Menu.SubMenu?
        public var subMenus : [DrawerMenuResponse.Menu.SubMenu]?
        public var isCollapsed : Bool = false
        public var icon : String?
        
        init(name : String, type : String, menu :DrawerMenuResponse.Menu?, subMenu : DrawerMenuResponse.Menu.SubMenu?, icon : String?, isCollapsed : Bool, submenus : [DrawerMenuResponse.Menu.SubMenu]?) {
            self.icon = icon
            self.name = name
            self.type = type
            self.menu = menu
            self.subMenu = subMenu
            self.isCollapsed = isCollapsed
            self.subMenus = submenus
        }
    }
}
