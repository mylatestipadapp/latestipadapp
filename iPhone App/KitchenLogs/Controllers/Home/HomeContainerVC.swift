//
//  HomeContainerVC.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/12/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import KLCommon

class HomeContainerVC: UIViewController, DrawerVCDelegate {
    
    func timeAndDateDidTap() {
        
    }
    
    func cellDidSelectWithIndexAndtitle(index: NSInteger, title: String) {
    
    }
    
    @IBOutlet weak var homeContainer: UIView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var titleImage: UIImageView!
    
    var drawerVc :DrawerMenuVC!
    let sideView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (self.slideMenuController()?.leftViewController as? DrawerMenuVC)?.delegate = self;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onDrawerIconClicked(_ sender: Any) {
        (UIApplication.shared.delegate as? AppDelegate)!.drawerMenu?.openLeft();
    }
    
}
