//
//  HomeRootVC.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/12/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import KLCommon

class HomeRootVC: SlideMenuController {
    
    var drawerVc : DrawerMenuVC?
    var homeContainer : HomeContainerVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func awakeFromNib() {
        drawerVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DrawerViewController") as? DrawerMenuVC
        homeContainer = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeContainerViewController") as? HomeContainerVC
        
        self.mainViewController = homeContainer
        self.leftViewController = drawerVc
        (UIApplication.shared.delegate as? AppDelegate)!.drawerMenu = self.slideMenuController()
        self.changeLeftViewWidth(UIScreen.main.bounds.width * 0.7)
        super.awakeFromNib()
    }
     
}
