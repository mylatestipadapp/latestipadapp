//
//  LoginVC.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/8/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//


import Foundation
import UIKit
import UserNotifications
import Toast_Swift
import KLCommon


class LoginVC: UIViewController,UITextFieldDelegate {
    
    static public let HOME_SEGUE : String = "login_to_home_segue"
    
    @IBOutlet weak var loginViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var emailField: UITextField?
    @IBOutlet weak var passwordField: UITextField?
    @IBOutlet weak var loginButton: UIButton?
    
    @IBOutlet weak var bottomview: UIView?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(UserUtil.isUserLoggedIn()) {
            proceedToHome()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailField?.attributedPlaceholder = NSAttributedString(string: "Email/Username",
                                                               attributes: [NSForegroundColorAttributeName: UIColor.white])
        passwordField?.attributedPlaceholder = NSAttributedString(string: "Your Password",
                                                                  attributes: [NSForegroundColorAttributeName: UIColor.white])
        emailField?.delegate = self
        passwordField?.delegate = self
        emailField?.text = "viren@kitchenlogs.com"
        passwordField?.text = "123456"
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if let email = UserDefaults.standard.value(forKey: "email"){
            emailField?.text = email as? String
        }
        
        if let password = UserDefaults.standard.value(forKey: "password"){
            passwordField?.text = password as? String
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.view.layoutIfNeeded()
            self.loginViewTopConstraint.constant = screenSize.height
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func showLoginView(_ sender: UIButton) {
        view.layoutIfNeeded()
        loginViewTopConstraint.constant = screenSize.height
        sender.isHidden = true
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        self.emailField?.resignFirstResponder()
        self.passwordField?.resignFirstResponder()
        let emailId : String = self.emailField?.text ?? ""
        let password : String = self.passwordField?.text ?? ""
        if  (String.isNilOrEmpty(emailId)){
            self.view.makeToast("Please enter your email id");
            return
        }
        if String.isNilOrEmpty(password) {
            self.view.makeToast("Please enter your password");
            return
        }
        createJWTToken()
    }
    
    func createJWTToken(){
        LoadingOverlay.shared.showOverlay(view:self.view, msg: "")
        let postObj : LoginPost = LoginPost.init(email: self.emailField?.text ?? "", password: (self.passwordField?.text)!, token: "iOS")
        UserDefaultsUtil.saveUserToken(token:"")
        let _ = AuthService.loginUser(post: postObj) { (response, error) in
            if (error == nil) {
                let resp : LoginResponse = response as! LoginResponse
                
                if resp.code == 200 || resp.message == "Success" {
                    UserDAO().saveUser(user: (resp.data?[0].user)!)
                    UserDefaultsUtil.saveUserToken(token: (resp.data?[0].token)!)
                    LoadingOverlay.shared.hideOverlayView()
                    self.proceedToHome()
                } else if resp.code == 400 {
                    let alert = UIAlertController(title: "Incorrect password", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Forgot Password", style: UIAlertActionStyle.default, handler: self.showForgotPasswordAlert))
                    
                    self.present(alert, animated: true, completion: nil)
                    LoadingOverlay.shared.hideOverlayView()
                } else {
                    LoadingOverlay.shared.hideOverlayView()
                    self.view.makeToast(resp.message ?? "")
                }
            } else {
                self.view.makeToast("Failed to communicate to server")
                LoadingOverlay.shared.hideOverlayView()
            }
        }
    }
    
    func showForgotPasswordAlert(action: UIAlertAction) {
        let _ = AuthService.forgotPassword(post: ForgotPasswordPost.init(email: self.emailField?.text ?? "")) { (response, error) in
            if (error == nil) {
                let alert = UIAlertController(title: "Mail Sent", message: "If this email exists in our system, we will send you new password via email.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.view.makeToast("Something went wrong")
            }
        }
    }
    
    func proceedToHome()
    {
        self.performSegue(withIdentifier:LoginVC.HOME_SEGUE, sender: self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y = 0
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
