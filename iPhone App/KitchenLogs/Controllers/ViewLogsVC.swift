//
//  ViewLogsVC.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/22/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import KLCommon

class ViewLogsVC: UIViewController, UITabBarDelegate, UITableViewDataSource {

    
    @IBOutlet weak var logsTable: UITableView!
    
    private var logs : [Any] = []
    static var LOG_CELL_IDENTIFIER : String  = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchLogs()
    }

    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.logs.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let item : MenuViewItem = self.drawerMenuDataset[indexPath.row]
//        if item.type == "menu" {
//            if item.subMenus != nil && (item.subMenus?.count)! > 0  {
//                if item.isCollapsed == true {
//                    item.isCollapsed = false
//                } else {
//                    item.isCollapsed = true
//                }
//                self.refreshDrawerMenuItems()
//            } else {
//                self.onMenuClicked(item : item)
//            }
//        } else {
//            self.onSubMenuClicked(item: item)
//        }
//        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let item : MenuViewItem = self.drawerMenuDataset[indexPath.row]
//        var cell : DrawerSuperCell! = DrawerSuperCell()
//        if item.type == "menu" {
//            cell = drawerTableView?.dequeueReusableCell(withIdentifier: DrawerMenuVC.MENU_CELL_IDEBNTIFIER, for: indexPath) as? DrawerMenuCell
//        } else {
//            cell = drawerTableView?.dequeueReusableCell(withIdentifier: DrawerMenuVC.SUBMENU_CELL_IDEBNTIFIER, for: indexPath) as? DrawerSubMenuCell
//        }
//        cell.delegate = self
//        cell.configureView(menuItem: item)
        return UITableViewCell()
    }

    
    private func fetchLogs() {
        let logPost : ViewLogsPost = ViewLogsPost()
        logPost.businessId = UserUtil.getBusinessId()
        logPost.locationId = UserUtil.getLocationId()
        ChecksService.getViewLogs(post: logPost) { (resp, error) in
            
        }
    }
}
