//
//  APIHelper.swift
//  Pods
//
//  Created by Viren Bhandari on 09/06/2017.
//
//

import UIKit
import Alamofire

open class APIHelper: NSObject {
    //static let BaseURL: String = "https://api.kitchenlogs.com/"  /** Prod base url **/
    //    static let BaseURL : String = "https://dev.kitchenlogs.com/"  /** Dev base url **/
        static let BaseURL : String = "https://apidev.kitchenlogs.com/"
    
    static var AcceptHeader: String { get { return "text/json" } }
    static var ContentType: String { get { return "application/json" } }
    static var ConnectionTimeOut: Int { get { return 30000 } }
    
    static var AuthHeader: String {
      /*  if UserDefaultsUtil.getUserToken() != nil {
            return "Bearer " + UserDefaultsUtil.getUserToken()!
        }*/
        return ""
    }
}


