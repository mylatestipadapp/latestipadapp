//
//  DrawerMenuResponse.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/8/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import Gloss

public class DrawerMenuResponse: Decodable, Encodable {
    
    public var code : String?
    public var message : String?
    public var data : [Menu]?
    
    init() {
        
    }
    
    public required init?(json: JSON) {
        self.code = "code" <~~ json
        self.message = "message" <~~ json
        self.data = "data" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "code" ~~> self.code,
            "message" ~~> self.message,
            "data" ~~> self.data
            ])
    }
    
    public class Menu : Decodable, Encodable {
        
        public var id : String?
        public var label : String?
        public var isDisabled : Bool?
        public var icon : String?
        public var background_color : String?
        public var isDefault : Bool?
        public var menuId: String?
        public var subMenu : [SubMenu]?
        
        init() {
            
        }
        
        public required init?(json: JSON) {
            self.id = "_id" <~~ json
            self.label = "label" <~~ json
            self.isDisabled = "isDisabled" <~~ json
            self.icon = "icon" <~~ json
            self.background_color = "background_color" <~~ json
            self.isDefault = "isDefault" <~~ json
            self.menuId = "menuId" <~~ json
            self.subMenu = "subMenu" <~~ json
        }
        
        public func toJSON() -> JSON? {
            return jsonify([
                "_id" ~~> self.id,
                "label" ~~> self.label,
                "isDisabled" ~~> self.isDisabled,
                "background_color" ~~> self.background_color,
                "menuId" ~~> self.menuId,
                "isDefault" ~~> self.isDefault,
                "subMenu" ~~> self.subMenu
                ])
        }
        
        public class SubMenu : Decodable, Encodable {
            
            public var menuId : Int?
            public var parentId: Int?
            public var isDefault : Bool?
            public var background_color : String?
            public var icon : String?
            public var isDisabled : Bool?
            public var label: String?
            
            init() {
                
            }
            
            public required init?(json: JSON) {
                self.menuId = "menuId" <~~ json
                self.parentId = "parentId" <~~ json
                self.background_color = "background_color" <~~ json
                self.isDisabled = "isDisabled" <~~ json
                self.icon = "icon" <~~ json
                self.isDefault = "isDefault" <~~ json
                self.label = "label" <~~ json
            }
            
            public func toJSON() -> JSON? {
                return jsonify([
                    "menuId" ~~> self.menuId,
                    "parentId" ~~> self.parentId,
                    "background_color" ~~> self.background_color,
                    "isDisabled" ~~> self.isDisabled,
                    "icon" ~~> self.icon,
                    "isDefault" ~~> self.isDefault,
                    "label" ~~> self.label
                    ])
            }
        }
        
    }
}
