//
//  LoginResponse.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/8/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import Gloss

public class LoginResponse: Encodable, Decodable {
    
    public var code: Int?
    public var message: String?
    public var data : [LoginData]?
    
    init() {
        
    }
    
    public required init?(json: JSON) {
        self.code = "code" <~~ json
        self.message = "message" <~~ json
        self.data = "data" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "code" ~~> self.code,
            "message" ~~> self.message,
            "data" ~~> self.data
            ])
    }
    
   public class LoginData: Encodable, Decodable {
        public var token : String?
        public var user : User?
        
        init() {
            
        }
        
        public required init?(json: JSON) {
            self.user = "user" <~~ json
            self.token = "token" <~~ json
        }
        
        public func toJSON() -> JSON? {
            return jsonify([
                "token" ~~> self.token,
                "user" ~~> self.user
                ])
        }
    }
}
