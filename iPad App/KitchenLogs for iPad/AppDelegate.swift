//
//  AppDelegate.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 07/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit
import DropDown
         
import Fabric
import Crashlytics
import AlamofireNetworkActivityLogger
import Alamofire
var notificationTitle = ""
import UserNotifications



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var sliderView:SlideMenuController!
    var isSmallSliderShow:Bool = true;
    var drawerMenuItems : [DrawerMenuResponse.Menu] = []
    var defaultData:AllCheckMetadata?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if #available(iOS 10.0, *) {
            
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            // set the type as sound or badge
            
            center.requestAuthorization(options: [.sound,.alert,.badge]) { (granted, error) in
                // Enable or disable features based on authorization
                if granted {
                    print("Yay!")
                } else {
                    print("D'oh")
                }
            }
            application.registerForRemoteNotifications()
        }
        else {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
        }

        
        NetworkActivityLogger.shared.level = .debug
        NetworkActivityLogger.shared.startLogging()
      if  Helper.fetchString(USER_TOKEN) != ""{
         navigateHomeView()
        } else {
         NaviagteLoginView();
        }
        
        DropDown.startListeningToKeyboard()
        callNotificationApi()
        return true
    }
    
    func callNotificationApi() {
        Alamofire.request("https://dev.kitchenlogs.com/api/settings/getsettings/59c94509661f4451634699c4/59c94493661f4451634699c2", method: .get, parameters: nil, encoding: JSONEncoding.default, headers:nil)
            .responseJSON { response in
//                print(response.request as Any)  // original URL request
//                print(response.response as Any) // URL response
                print("value",response.result.value as Any)   // result of response serialization
                self.fireNotification(val:response.result.value)
        }
    }
    func fireNotification(val:Any) {
        var arr = (val as! Array<Any>)
        var checkArr:NSArray?
        var timeInterval:TimeInterval?
        if arr.count > 0 {
        var dict = arr[0] as! NSDictionary
        var checkArr = dict["checks"] as! NSArray
        var openingDict = checkArr.object(at: 0) as! NSDictionary;
        var notificationDict = openingDict["notificationMin"] as! NSDictionary
        var timeInterval = notificationDict["value"];
        print("checsdfsfk",timeInterval);
    }
        if arr.count > 1 {

        var closingDict = checkArr?.object(at: 1) as! NSDictionary;
        var closingnotificationDict = closingDict["notificationMin"] as! NSDictionary
        var closingtimeInterval = closingnotificationDict["value"];
        
         if #available(iOS 10.0, *) {
            fireiOS10Notification(time: timeInterval as! TimeInterval);

            fireiOS10Notification(time: closingtimeInterval as! TimeInterval);
         }
         else {
            fireiOS9Notification(time:timeInterval as! TimeInterval);

        fireiOS9Notification(time:closingtimeInterval as! TimeInterval);
         }
        }

    }
    func fireiOS9Notification(time:TimeInterval) {
        let notification = UILocalNotification()
        notification.fireDate = NSDate(timeIntervalSinceNow: time) as Date
        // notification.alertBody = "Hey you! Yeah you! Swipe to unlock!"
        // notification.alertAction = "be awesome!"
        notification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(notification)
  
    }
    func fireiOS10Notification(time:TimeInterval) {
        if #available(iOS 10.0, *) {

        let content = UNMutableNotificationContent()
        
        content.title = NSString.localizedUserNotificationString(forKey: "Opening Check", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "Log today's Opening Check!", arguments: nil)
        content.sound = UNNotificationSound.default()
        content.badge = UIApplication.shared.applicationIconBadgeNumber + 1 as NSNumber;
        content.categoryIdentifier = "com.elonchan.localNotification"
        // Deliver the notification in five seconds.
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval:time, repeats: false)
        let request = UNNotificationRequest.init(identifier: "FiveSecond", content: content, trigger: trigger)
        
        // Schedule the notification.
        let center = UNUserNotificationCenter.current()
        center.add(request)
        }
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
    }
    func NaviagteLoginView(){
        let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let navigationController = UINavigationController(rootViewController: loginVC)
        navigationController.setNavigationBarHidden(true, animated: false);
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
    
    func navigateHomeView(){
         self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        Constant.isOpeningScreen = true;

        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyBoard.instantiateViewController(withIdentifier: "HomeCntrl") as! HomeCntrl
        let leftViewController =  MenuViewController(nibName : "MenuViewController" , bundle:  nil)
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController);
        self.sliderView = slideMenuController
        let navigationController = UINavigationController(rootViewController: slideMenuController)
        navigationController.setNavigationBarHidden(true, animated: false);
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        self.sliderView.closeLeft();
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

