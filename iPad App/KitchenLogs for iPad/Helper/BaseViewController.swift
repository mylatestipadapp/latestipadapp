//
//  BaseViewController.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 08/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit


class BaseViewController: UIViewController,TopBarDelegate,TopHeaderDelegate {
    var topView:TopBar!
    var topHeader:TopHeaderView!
    var isPotrait:Bool!
    
    
typealias completion = (_ clickedTitle:String) -> Void;
    override public var traitCollection: UITraitCollection {
        if UIDevice.current.userInterfaceIdiom == .pad && UIDevice.current.orientation.isPortrait {
            return UITraitCollection(traitsFrom:[UITraitCollection(horizontalSizeClass: .compact), UITraitCollection(verticalSizeClass: .regular)])
        }
        return super.traitCollection
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        

        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
          
            isPotrait = false;
        }
        
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
          
            isPotrait = true;
        }


        // Do any additional setup after loading the view.
    }
    
    func gettingOrientationWidthHeight()-> CGRect {
        let screenSize: CGRect = UIScreen.main.bounds
        return screenSize;
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getAppDelegate()->AppDelegate{
        return (UIApplication.shared.delegate as? AppDelegate)!;
    }
    
    
    func rotated() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            print("Landscape")
            isPotrait = false;
        }
        
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            print("Portrait")
            isPotrait = true;
    }
        
        }
    
    
    
    // Alert View Display
    func alert(_ alertTitle:String,alertMessage:String, delegate:UIViewController,firstButton:String,secondButton:String,isSingleOK:Bool, completion: @escaping (_ clickedTitle:String) -> Void ){
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        if isSingleOK == true {
            let noAction = UIAlertAction(title:firstButton, style: .default) { (action) -> Void in
                completion(firstButton);
            }
            alertController.addAction(noAction)
        } else {
            let firstAction = UIAlertAction(title:firstButton, style: .default) { (action) -> Void in
                completion(firstButton);
            }
            let secondAction = UIAlertAction(title:secondButton, style: .default) { (action) -> Void in
                completion(secondButton);
            }
            alertController.addAction(firstAction)
            alertController.addAction(secondAction)
        }
        self.present(alertController, animated: true, completion: nil)
    }

    // Naviagation Method
    func navigateWithMenu(toViewController:String,valAnyObj:AnyObject){
        getAppDelegate().window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        
        if toViewController == "HomeCntrl" {
            let mainViewController = getstoryboardName().instantiateViewController(withIdentifier: "HomeCntrl") as! HomeCntrl
            addMenuClass(mainView: mainViewController);
        }
        else if toViewController == "ViewLogController" {
            let mainViewController = getstoryboardName().instantiateViewController(withIdentifier: "ViewLogController") as! ViewLogController
            addMenuClass(mainView: mainViewController);
            
        }
        else if toViewController == "DeliveryViewController" {
            let mainViewController = getstoryboardName().instantiateViewController(withIdentifier: "DeliveryViewController") as! DeliveryViewController
            addMenuClass(mainView: mainViewController);
            
        }
        else if toViewController == "PestInjuryView" {
            let mainViewController = getstoryboardName().instantiateViewController(withIdentifier: "PestInjuryView") as! PestInjuryView
            mainViewController.type = valAnyObj as? String;
            addMenuClass(mainView: mainViewController);
            
        }else if toViewController == "ProbCheckView" {
            let mainViewController = getstoryboardName().instantiateViewController(withIdentifier: "ProbCheckView") as! ProbCheckView
            addMenuClass(mainView: mainViewController);
            
        }
        else if toViewController == "FoodController" {
            let mainViewController = getstoryboardName().instantiateViewController(withIdentifier: "FoodController") as! FoodController
            //mainViewController.type = valAnyObj as? String;
            addMenuClass(mainView: mainViewController);
            
        }
        else if toViewController == "ApplianceScreenController" {
            let mainViewController = getstoryboardName().instantiateViewController(withIdentifier: "ApplianceScreenController") as! ApplianceScreenController
            Constant.isCleaningScreen = false;
            addMenuClass(mainView: mainViewController);
            
        }
        
    }
    
    // StoryBoard Name
    func getstoryboardName()-> UIStoryboard{
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        return storyBoard;
    }
    
    // Add Menu With Navigation View
    func addMenuClass(mainView:UIViewController){
        let leftViewController =  MenuViewController(nibName : "MenuViewController" , bundle:  nil)
        let slideMenuController = SlideMenuController(mainViewController: mainView, leftMenuViewController: leftViewController);
        getAppDelegate().sliderView = slideMenuController
        self.navigationController?.pushViewController(slideMenuController, animated: false);
    }
    
    
    // Add TopView
    func setTopView(superView:UIView!){
        topView = TopBar()
        topView.controlller = self;
        topView.delegate = self;
        superView.addSubview(topView.view);
        topviewConstraint(vwTop: topView.view!);
    }
    
    func clickedToDateTime(sender:UILabel){
      
    }
    

    
    // TopBar Delegate
    func clickedMenu(){
        
        
    }
    
    // Add TopView
    func setSubTopView(superView:UIView!){
        topHeader = TopHeaderView()
        topHeader.controlller = self;
        topHeader.delegate = self;
        superView.addSubview(topHeader.view);
        topviewConstraint(vwTop: topHeader.view);
    }
    
    
    func topviewConstraint(vwTop:UIView){
        vwTop.translatesAutoresizingMaskIntoConstraints = false;
        let dict:Dictionary = ["view":vwTop];
        let constraint = NSLayoutConstraint.constraints(withVisualFormat:"H:|-0-[view]-0-|" , options:NSLayoutFormatOptions(rawValue: 0), metrics:nil, views:dict )
        let vertical = NSLayoutConstraint.constraints(withVisualFormat:"V:|-0-[view]-0-|" , options:NSLayoutFormatOptions(rawValue: 0), metrics:nil, views:dict)
        NSLayoutConstraint.activate(constraint)
        NSLayoutConstraint.activate(vertical)

    }
    
    func clickedDateTime(sender: UILabel) {
        
    }
    
    
    // KeyBoardWillShow
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    // keyboardWillHide
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.view.frame.origin.y = 0
        }
    }
    
    func getCurrentDate()-> String{
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "dd-MM-yyyy"
         let currentDate  = dateFormatter.string(from: NSDate() as Date)
        return currentDate;
    }

    func getTime(strDate:String)-> String{
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "MMM dd hh:mm a";
        let date  = dateFormatter.date(from: strDate)!;
        print("Date is:\(date)")
         dateFormatter.dateFormat = "HH:mm";
         let time  = dateFormatter.string(from: date)
        let arry = Helper.seperatedByColon(time);
         print("Time is:\(arry)")
        return String(describing: arry[0]);
    }
    func getMins(strDate:String)-> String{
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "MMM dd hh:mm a";
        let date  = dateFormatter.date(from: strDate)!;
        print("Date is:\(date)")
        dateFormatter.dateFormat = "HH:mm";
        let time  = dateFormatter.string(from: date)
        let arry = Helper.seperatedByColon(time);
        print("Time is:\(arry)")
        return String(describing: arry[1]);
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
