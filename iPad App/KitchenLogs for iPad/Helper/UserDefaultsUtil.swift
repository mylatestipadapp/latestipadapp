//
//  UserDefaultsUtil.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/8/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit

class UserDefaultsUtil: NSObject {

    private static var TOKEN_KEY : String = "Token Key";
    
    public static func getUserToken() -> String? {
        return UserDefaults.standard.string(forKey: TOKEN_KEY) ?? nil
    }
    
    public static func saveUserToken(token : String) {
        UserDefaults.standard.set(token, forKey: TOKEN_KEY)
    }
    
}
