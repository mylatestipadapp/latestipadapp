//
//  SJSegmentTab.swift
//  Pods
//
//  Created by Subins on 22/11/16.
//  Copyright © 2016 Subins Jose. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
//    associated documentation files (the "Software"), to deal in the Software without restriction,
//    including without limitation the rights to use, copy, modify, merge, publish, distribute,
//    sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//    substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
//  LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
//  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

var checklistVw = GridView(nibName: "GridView", bundle: nil);
var applianceVw = GridView(nibName: "GridView", bundle: nil);
var foodVw = GridView(nibName: "GridView", bundle: nil);
var fridge = FridgeView(nibName: "FridgeView", bundle: nil);
var freezer = FridgeView(nibName: "FridgeView", bundle: nil);




import Foundation

typealias DidSelectSegmentAtIndex = (_ segment: SJSegmentTab?,_ index: Int,_ animated: Bool) -> Void

open class SJSegmentTab: UIView {

	let kSegmentViewTagOffset = 100
    var superVw:UIView?
	let button = UIButton(type: .custom)

	var didSelectSegmentAtIndex: DidSelectSegmentAtIndex?
	var isSelected = false {
		didSet {
			button.isSelected = isSelected
		}
	}

	convenience init(title: String) {
		self.init(frame: CGRect.zero)
        setTitle(title)
	}

	convenience init(view: UIView) {
		self.init(frame: CGRect.zero)
        superVw = view;

        if view.tag == 1 {
           view.addSubview(checklistVw.view);
            checklistVw.vwCheckList?.isHidden = false
            addConstraintsToView(checklistVw.view)
        }
//
//       
        if view.tag == 2 {
            view.addSubview(applianceVw.view);
            applianceVw.vwAppliance?.isHidden = false
            addConstraintsToView(applianceVw.view)

         }
        if view.tag == 3 {
            view.addSubview(foodVw.view);
            foodVw.vwFood?.isHidden = false
            addConstraintsToView(foodVw.view)
            
        }
        if view.tag == 4 {
            view.addSubview(fridge.view);
            fridge.vwFridge?.isHidden = false
            addConstraintsToView(fridge.view)
            
        }
        if view.tag == 5 {
            view.addSubview(freezer.view);
            freezer.vwFrizzer?.isHidden = false
            addConstraintsToView(freezer.view)
            
        }

        addSubview(view)
        addConstraintsToView(view)
   


	}

	required override public init(frame: CGRect) {
		super.init(frame: frame)

		translatesAutoresizingMaskIntoConstraints = false
		button.frame = bounds
		button.addTarget(self, action: #selector(SJSegmentTab.onSegmentButtonPress(_:)),
		                 for: .touchUpInside)
		//addSubview(button)
		//addConstraintsToView(button)
	}
    func rotated() {
        self.superview?.frame.size.width = UIScreen.main.bounds.size.width
        superVw?.frame.size.width = UIScreen.main.bounds.size.width/2
        checklistVw.view?.frame.size.width = UIScreen.main.bounds.size.width/2
        applianceVw.view?.frame.size.width = UIScreen.main.bounds.size.width/2
    }

	func addConstraintsToView(_ view: UIView) {

		view.translatesAutoresizingMaskIntoConstraints = false
		let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|",
		                                                         options: [],
		                                                         metrics: nil,
		                                                         views: ["view": view])
		let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|",
		                                                           options: [],
		                                                           metrics: nil,
		                                                           views: ["view": view])
        NSLayoutConstraint.activate(verticalConstraints)
        NSLayoutConstraint.activate(horizontalConstraints)
		//addConstraints(verticalConstraints)
		//addConstraints(horizontalConstraints)
	}

	required public init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
    
    open func setTitle(_ title: String) {
        
        button.setTitle(title, for: .normal)
    }

	open func titleColor(_ color: UIColor) {

		button.setTitleColor(color, for: .normal)
	}

	open func titleFont(_ font: UIFont) {

		button.titleLabel?.font = font
	}

	func onSegmentButtonPress(_ sender: AnyObject) {

		let index = tag - kSegmentViewTagOffset
		NotificationCenter.default.post(name: Notification.Name(rawValue: "DidChangeSegmentIndex"),
		                                object: index)

		if didSelectSegmentAtIndex != nil {
			didSelectSegmentAtIndex!(self, index, true)
		}
	}
}
