//
//  CustomDatePicker.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 12/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit
protocol CustomDatePickerDelegate {
    func selectedDatePickerValue(value:String,yearDt:String)
    func clickedDone()
}
class CustomDatePicker: UIViewController {

    @IBOutlet weak var dtPicker: UIDatePicker!
    var delegate:CustomDatePickerDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func clickedToDone(_ sender: Any) {
        delegate.clickedDone();
    }
    
    @IBAction func clickedValueChanged(_ sender: Any) {
        let dtformat = DateFormatter()
        dtformat.dateFormat = "MMM dd hh:mm  a"
        let dateVal = dtformat.string(from: (sender as AnyObject).date)
         dtformat.dateFormat = "dd-MM-yyyy"
        let dateYearVal = dtformat.string(from: (sender as AnyObject).date)
        delegate.selectedDatePickerValue(value: dateVal,yearDt: dateYearVal);
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
