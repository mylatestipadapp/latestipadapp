//
//  ApplianceController.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 9/11/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit
         

protocol ApplianceControllerDelegate {
    func tableViewCell2FridgeDelegate(index:Int, temp: String , comment: String)
    func tableViewCell2FreezerDelegate(index:Int, temp: String , comment: String)
}

class ApplianceController: UITableViewController,ApplianceViewDelegate {
    
    var heightRow = Constant.ApplianceViewHeight;
    var selectedSelection = -1;
    var selectedRow = -1;

    var minCookingTemp = 0
    var reHeatingTemp = 0
    var hotHoldingTemp = 0
    var fridgeTemp = 0
    
    var hotHoldingNameArray = NSMutableArray()
    var cookedNameArray = NSMutableArray()
    var reheatingNameArray = NSMutableArray()
    var dictData : AllCheckMetadata?
    var delegate:ApplianceControllerDelegate?
    var isFridge = false;
    var isFreezer = false;



    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: "ApplianceView", bundle: nil), forCellReuseIdentifier: "ApplianceView")

     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = ApplianceSectionHeader()
        vw.view.frame.size.height = Constant.ApplianceViewSectionHeaderHeight;
          if (section == 0) {
            vw.lblDesc?.text = "FRIDGE (under 8C)"
        }else
        {
            vw.lblDesc?.text = "FREEZER (Under -18C)"
        }
        return vw.view;
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constant.ApplianceViewSectionHeaderHeight;
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if isFridge || isFreezer{
            return 1

        }

        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var numberOfRows = 0
        if isFridge {
            if self.dictData?.entryDefaults?.isEmpty == false {

            if let  numberOfFridges = dictData?.entryDefaults?[0].numberOfFridges {
                numberOfRows = numberOfFridges
            }
            }
        }
        else if isFreezer {
            if self.dictData?.entryDefaults?.isEmpty == false {

            if let  numberOfFreezers = dictData?.entryDefaults?[0].numberOfFreezers {
                numberOfRows = numberOfFreezers
            }
            }
        }
        else {
        if (section == 0) {
            if self.dictData?.entryDefaults?.isEmpty == false {
            if let  numberOfFridges = dictData?.entryDefaults?[0].numberOfFridges {
                numberOfRows = numberOfFridges
            }
            }
        }else
        {
            if self.dictData?.entryDefaults?.isEmpty == false {

            if let  numberOfFreezers = dictData?.entryDefaults?[0].numberOfFreezers {
                numberOfRows = numberOfFreezers
            }
            }
        }
        }
        return numberOfRows;
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "ApplianceView") as? ApplianceView
        if cell == nil {
           cell = UITableViewCell(style: .default, reuseIdentifier: "ApplianceView") as! ApplianceView
        }
        cell?.delegate = self;
        cell?.selectedRow = indexPath.row;
        cell?.selectedSection = indexPath.section;

        cell?.vwBottomHeight?.constant = 0;

        if indexPath.row == selectedRow && indexPath.section == selectedSelection  {
            if heightRow == Constant.ApplianceViewHeight {
                cell?.vwBottomHeight?.constant = 0;
            }
            else {
                cell?.vwBottomHeight?.constant = CGFloat(Constant.ApplianceViewHeight);

            }
        }
        if self.dictData?.entryDefaults?.isEmpty == false {

        fridgeTemp = dictData?.entryDefaults?[0].fridgeTemp ?? 0
        }

        if isFridge {
            cell?.txtTemp?.tag = 555 + indexPath.row
            cell?.txtComment?.tag = 555 + indexPath.row
            cell?.lblTitle?.text = "Fridge  " + "\(indexPath.row + 1)"
            cell?.imgvw?.image = #imageLiteral(resourceName: "fridgeIcon")
            //    cell?.?.text = "8°"
            cell?.txtComment?.placeholder = "\(fridgeTemp)" + "°C or below else add comment"
        }
        else if isFreezer {
            cell?.txtTemp?.tag = indexPath.row
            cell?.txtComment?.tag = indexPath.row
            cell?.lblTitle?.text = "Freezer  " + "\(indexPath.row + 1)"
            cell?.imgvw?.image = #imageLiteral(resourceName: "freezer")
            // cell?.mySubLabel?.text = "-18°"
            cell?.txtComment?.placeholder = "-18°C or below else add comment"
        }
        else {
        if (indexPath.section == 0) {
            cell?.txtTemp?.tag = 555 + indexPath.row
            cell?.txtComment?.tag = 555 + indexPath.row
            cell?.lblTitle?.text = "Fridge  " + "\(indexPath.row + 1)"
            cell?.imgvw?.image = #imageLiteral(resourceName: "fridgeIcon")
        //    cell?.?.text = "8°"
            cell?.txtComment?.placeholder = "\(fridgeTemp)" + "°C or below else add comment"

            
        }
        else {
            cell?.txtTemp?.tag = indexPath.row
            cell?.txtComment?.tag = indexPath.row
            cell?.lblTitle?.text = "Freezer  " + "\(indexPath.row + 1)"
            cell?.imgvw?.image = #imageLiteral(resourceName: "freezer")
           // cell?.mySubLabel?.text = "-18°"
            cell?.txtComment?.placeholder = "-18°C or below else add comment"
        }
        }
        // Configure the cell...
        cell?.selectionStyle = .none


        return cell!
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == selectedSelection && indexPath.row == selectedRow {
            return CGFloat(heightRow);
        }
        return Constant.ApplianceViewHeight;
    }
    
    func dropDownClicked(val: Bool,section: Int,row:Int) {
        selectedSelection = section;
        selectedRow = row;
        if val {
            heightRow = Constant.ApplianceViewHeight;

        }
        else {
            heightRow = Constant.ApplianceViewCellHeight;

        }
        self.tableView.reloadData()
    }

    func textEditingClicked(txtField: UITextField,row:Int) {
        if(txtField.tag >= 555)
        {
            
            let cell1 : ApplianceView? = self.tableView?.cellForRow(at: NSIndexPath(row: txtField.tag - 555, section: 0) as IndexPath) as? ApplianceView
            
            
            
            if(Int(txtField.text ?? "") ?? 0 > fridgeTemp){
                
               // self.dropDowntapped(index: txtField.tag - 555, section: cell1?.dropDownButton?.tag ?? 0)
                
            }
            
            
            self.delegate?.tableViewCell2FridgeDelegate(index: txtField.tag - 555 , temp: cell1?.txtTemp?.text ?? "", comment: cell1?.txtComment?.text ?? "")
        }else
        {
            let cell1 : ApplianceView? = self.tableView?.cellForRow(at: NSIndexPath(row: txtField.tag, section: 1) as IndexPath) as? ApplianceView
            
            if(Int(txtField.text ?? "") ?? 0 >  -18){
                
               // self.dropDowntapped(index: txtField.tag, section: cell1?.dropDownButton?.tag ?? 0)
                
            }
            self.delegate?.tableViewCell2FreezerDelegate(index: txtField.tag, temp: cell1?.txtTemp?.text ?? "", comment: cell1?.txtComment?.text ?? "")
        }

    }
    
    func dropDowntapped(index:Int, section:Int)
    {
        
//        
//        if(gridIndex == 1){
//            
//            var sectionNo = 1
//            var indexNo = index
//            if (index >= 555) {
//                sectionNo = 0
//                indexNo = indexNo - 555
//            }
//            let indexpath = NSIndexPath(row: indexNo, section: section)
//            
//          //  myTableView2?.reloadData()
//            if (indexpath as IndexPath == self.selectedIndexPath) {
//                self.selectedIndexPath = nil
//            }else{
//                self.selectedIndexPath = indexpath as IndexPath
//            }
//        }else if(gridIndex == 2)
//        {
//            var row = index
//            
//            let indexpath = NSIndexPath(row: row, section: section)
//            
//            myTableView3?.reloadData()
//            if (indexpath as IndexPath == self.selectedIndexPath) {
//                self.selectedIndexPath = nil
//            }else{
//                self.selectedIndexPath = indexpath as IndexPath
//            }
//            
//        }
        
        //        let indexpath = NSIndexPath(row: index, section: 1)
        //        if (indexpath as IndexPath == self.selectedIndexPath) {
        //            self.selectedIndexPath = nil
        //        }else{
        //            self.selectedIndexPath = indexpath as IndexPath
        //        }
    }
}
