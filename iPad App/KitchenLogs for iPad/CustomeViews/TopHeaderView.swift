//
//  TopHeaderView.swift
//  
//
//  Created by Manish Saini on 12/09/17.
//
//

import UIKit

protocol TopHeaderDelegate {
    func clickedToDateTime(sender:UILabel)
}
class TopHeaderView: UIViewController {
var controlller:UIViewController!
    @IBOutlet var lblTitle:UILabel?
    @IBOutlet var lblDate:UILabel?
    @IBOutlet var lblTime:UILabel?
    var delegate:TopHeaderDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if Constant.isClosingScreen {
            lblTitle?.text = "Closing Check";

        }
        else if Constant.isCleaningScreen {
            lblTitle?.text = "Cleaning Check";

        }
        else {
            lblTitle?.text = "Opening Check";
        }
        self.lblTitle?.font = UIFont(name: MEDIUM_APPFONT, size: Constant.NormalfontSize)
        self.lblDate?.font = UIFont(name: REGULAR_APPFONT, size: Constant.SmallfontSize)
        self.lblTime?.font = UIFont(name: REGULAR_APPFONT, size: Constant.SmallfontSize)


    }
    func setTitle(str:String) {
        lblTitle?.text = str;
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnDateAndTimeClick() {
        delegate?.clickedToDateTime(sender:lblTime!)
    }
    

    /*
     // MARK: - Navigationfunc clickedToDateTime(sender:UILabel){
     
     }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
