//
//  CheckListCell.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 9/13/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit
protocol ChecklistTableViewCellDelegate {
   func crossButtonclickedChecklistTableViewCell(index:Int,crossSelected:Bool)
    func radioButtonClickedChecklistTableViewCell(index:Int ,radioSelected:String)
    
}
class CheckListCell: UITableViewCell {

    @IBOutlet weak var btnRadio: UIButton?
    @IBOutlet weak var btnCross: UIButton?
    @IBOutlet weak var imgNa: UIImageView?
    @IBOutlet weak var lblDesc: UILabel?
    var delegate : ChecklistTableViewCellDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        lblDesc?.font = UIFont(name: MEDIUM_APPFONT, size: Constant.NormalfontSize)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnCrossClick(_ sender: UIButton) {
        if (btnCross?.isSelected ?? false){
            btnCross?.isSelected = false
            imgNa?.isHidden = true
            btnRadio?.isHidden = false
            
            self.alpha = 1
            btnRadio?.isUserInteractionEnabled = true
        }else
        {
            btnRadio?.isHidden = true
            
            btnCross?.isSelected = true
            imgNa?.isHidden = false
            self.alpha = 0.5
            btnRadio?.isUserInteractionEnabled = false
        }
        self.delegate?.crossButtonclickedChecklistTableViewCell(index: sender.tag,crossSelected:(btnCross?.isSelected)!)


    }
    @IBAction func btnRadioClick(_ sender: UIButton) {
        var value = "NO"
        if(sender.isSelected)
        {
            value = "NO"
            sender.isSelected = false
            btnRadio?.setImage(#imageLiteral(resourceName: "androidRadioButtonOn"), for: UIControlState.normal)
            
        }else
        {
            value = "YES"
            sender.isSelected = true
            btnRadio?.setImage(#imageLiteral(resourceName: "shapeg"), for: UIControlState.normal)
            
        }
        self.delegate?.radioButtonClickedChecklistTableViewCell(index: btnCross?.tag ?? 0 ,radioSelected: value)

    }
    
}
