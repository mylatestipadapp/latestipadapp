//
//  GridView.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 9/9/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import Foundation
protocol GridViewDelegate {
    func btnCheckListClick()
    func btnApplianceClick()
    func btnFoodClick()
}

class GridView: UIViewController {
    @IBOutlet var imgvwRadioCheckList:UIImageView?;
    @IBOutlet var imgvwRadioAppliance:UIImageView?;
    @IBOutlet var imgvwRadioFood:UIImageView?;
    
    @IBOutlet var lblCheckList:UILabel?;
    @IBOutlet var lblAppliance:UILabel?;
    @IBOutlet var lblFood:UILabel?;
    @IBOutlet var vwCheckList:UIView?;
    @IBOutlet var vwAppliance:UIView?;
    @IBOutlet var vwFood:UIView?;

    var delegate:GridViewDelegate?




    override func awakeFromNib() {
      
    }
    override func viewDidLoad() {
        super.viewDidLoad();

    }
    @IBAction func btnCheckListClick() {
        reset()
        imgvwRadioCheckList?.image = UIImage(named: "androidRadioButtonChecked")
        self.lblCheckList?.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        delegate?.btnCheckListClick()
        
    }
    @IBAction func btnApplianceClick() {
        reset()
        imgvwRadioAppliance?.image = UIImage(named: "androidRadioButtonChecked")
        self.lblAppliance?.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        delegate?.btnApplianceClick()
    }
    @IBAction func btnFoodClick() {
        reset()
        imgvwRadioFood?.image = UIImage(named: "androidRadioButtonChecked")
        self.lblFood?.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        delegate?.btnFoodClick()

    }
    func reset() {
        if Constant.isCleaningScreen {
            lblCheckList?.text = "After use";
            lblAppliance?.text = "End of day";
            lblFood?.text = "Deep Clean";
        }
            else {
            lblCheckList?.text = "Checklist";
            lblAppliance?.text = "Appliance";
            lblFood?.text = "Food";
        }

        imgvwRadioCheckList?.image = UIImage(named: "radio")
        imgvwRadioAppliance?.image = UIImage(named: "radio")
        imgvwRadioFood?.image = UIImage(named: "radio")
        self.lblCheckList?.font = UIFont(name: MEDIUM_APPFONT, size: Constant.NormalfontSize)
        self.lblAppliance?.font = UIFont(name: MEDIUM_APPFONT, size: Constant.NormalfontSize)
        self.lblFood?.font = UIFont(name: MEDIUM_APPFONT, size: Constant.NormalfontSize)

    }
    
}
