//
//  TopBar.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 9/10/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit
protocol TopBarDelegate {
    func clickedMenu();
}
class TopBar: UIViewController {
    var controlller:UIViewController!
    var delegate:TopBarDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func clickedToMenu(_ sender: Any) {
        let appDel = UIApplication.shared.delegate as? AppDelegate;
        appDel?.sliderView.openLeft();
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
