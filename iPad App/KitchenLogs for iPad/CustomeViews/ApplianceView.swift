//
//  ApplianceView.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 9/10/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit

protocol ApplianceViewDelegate {
    func dropDownClicked(val:Bool,section:Int,row:Int)
    func textEditingClicked(txtField:UITextField,row:Int)

}
class ApplianceView: UITableViewCell {

    @IBOutlet var vwBottom:UIView?
    @IBOutlet var vwBottomHeight:NSLayoutConstraint?
    var isOpen = false;
    var delegate:ApplianceViewDelegate?
    @IBOutlet var vwMain:UIView?
    
    @IBOutlet var lblTitle:UILabel?
    @IBOutlet var txtTemp:UITextField?
    @IBOutlet var txtComment:UITextField?
    @IBOutlet var imgvw:UIImageView?
    var selectedSection = -1;
    var selectedRow = -1;

    


    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.vwMain?.layer.borderColor = UIColor.darkGray.cgColor;
        
        self.lblTitle?.font = UIFont(name: MEDIUM_APPFONT, size: Constant.NormalfontSize)
        self.txtTemp?.font = UIFont(name: MEDIUM_APPFONT, size: Constant.NormalfontSize)
        self.txtComment?.font = UIFont(name: MEDIUM_APPFONT, size: Constant.NormalfontSize)
    }
    @IBAction func btnDropDownClick() {
        if isOpen {
            self.vwBottomHeight?.constant = 0
            // self.contentView.frame.size.height = 115;

            UIView.animate(withDuration: 2.0, animations:{() in self.contentView.layoutIfNeeded()}, completion: {(finish) in
               


            })
        }
        else {
            self.vwBottomHeight?.constant = CGFloat(Constant.ApplianceViewHeight)
            //self.contentView.frame.size.height = 230;
            self.delegate?.dropDownClicked(val: self.isOpen,section: self.selectedSection, row: self.selectedRow);


            UIView.animate(withDuration: 2.0, animations:{() in self.contentView.layoutIfNeeded()}, completion: {(finish) in


            })
        }
        self.delegate?.dropDownClicked(val: self.isOpen,section: self.selectedSection,row: self.selectedRow);
        self.isOpen = !self.isOpen;



        
    }
   
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.delegate?.textEditingClicked(txtField: textField,row: selectedRow);
    }
   
    
}
