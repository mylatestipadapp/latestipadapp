//
//  ApplianceSectionHeader.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 9/12/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit

class ApplianceSectionHeader: UIViewController {

    @IBOutlet var lblDesc:UILabel?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.lblDesc?.font = UIFont(name: MEDIUM_APPFONT, size: Constant.NormalfontSize)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
