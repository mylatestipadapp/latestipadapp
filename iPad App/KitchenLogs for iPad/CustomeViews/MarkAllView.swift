//
//  MarkAllView.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 9/9/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import Foundation
         

protocol MarkAllViewDelegate {
    func MarkAllViewDelegateRadioClicked(index:Int, gridNo:NSInteger , isSelected: String)
    func markAllGridViewCellDelegateTapped()

   // func MarkAllViewFridgeDelegate(index:Int, temp: String , comment: String)
   // func MarkAllViewFreezerDelegate(index:Int, temp: String , comment: String)
    
   // func MarkAllViewFoodDelegate(index:Int, temp: String , comment: String , section : Int)
    
}

class MarkAllView: UITableViewController,MarkAllHeaderDelegate,ChecklistTableViewCellDelegate,CleaningCheckListCellDelegate {
    
   // @IBOutlet var tblMarkAll:UITableView?
  //  @IBOutlet var lblMarkAll:UILabel?
    var vw:MarkAllHeader?;
    var delegate : MarkAllViewDelegate?
    var isMarkAll :Bool?
    var tabDict = NSMutableDictionary()
    var tab1Array = NSMutableArray()
    var tab1CrossArray = NSMutableArray()


    //var tab2Array = NSMutableArray()
   // var tab3Array = NSMutableArray()


    var reloaded:Bool? {
        willSet {
            self.tableView.reloadData()
  
        }
        didSet {
        }

    }
    var dictData:AllCheckMetadata? {
        willSet {
            self.tableView.reloadData()
  
        }
        didSet {
            if dictData !== oldValue {
            }
        }
    }


    override func viewDidLoad() {
        self.tableView.register(UINib(nibName: "CheckListCell", bundle: nil), forCellReuseIdentifier: "CheckListCell");
        self.tableView.register(UINib(nibName: "CleaningCheckListCell", bundle: nil), forCellReuseIdentifier: "CleaningCheckListCell");

        //self.lblMarkAll?.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        vw = MarkAllHeader(nibName: "MarkAllHeader", bundle: nil);
        vw?.delegate = self
        vw?.view.backgroundColor = UIColor.red
       
    }
    func markAllTapped() {
        self.tab1Array.removeAllObjects()
        if Constant.isOpeningScreen {
            for obj in (self.dictData?.openingCheck?[0].items)! {
                self.tab1Array.add("YES");
            }
        }
        if Constant.isClosingScreen {
            for obj in (self.dictData?.closingCheck?[0].items)! {
                self.tab1Array.add("YES");
            }
        }
        if Constant.isCleaningScreen {
            for obj in (self.dictData?.cleaningCheck?[0].categories?[0].items)! {
                self.tab1Array.add("YES");
            }
        }
       
        self.delegate?.markAllGridViewCellDelegateTapped()

    }
    func crossButtonclickedChecklistTableViewCell(index:Int,crossSelected:Bool) {
        tab1CrossArray.replaceObject(at: index, with: crossSelected);
 
    }

    func radioButtonClickedChecklistTableViewCell(index: Int, radioSelected: String) {
       // tabDict.setValue(radioSelected, forKey: String(index))
        isMarkAll = false;
        print(index);
        tab1Array.replaceObject(at: index, with: radioSelected);
        self.delegate?.MarkAllViewDelegateRadioClicked(index: index, gridNo:0 , isSelected: radioSelected)
        
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 60));

        header.backgroundColor = UIColor.getGreenColor()
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: header.frame.width, height: header.frame.height));
        lbl.backgroundColor = UIColor.clear
        lbl.textAlignment = .center;
        
        lbl.text = "MARK ALL AS DONE"
        lbl.textColor = UIColor.white
        lbl.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize);
        var btn = UIButton(type: .custom);
        btn.frame = lbl.frame
        btn.addTarget(self, action:#selector(MarkAllView.markAllTapped), for: .touchUpInside)
        header.addSubview(lbl);
        header.addSubview(btn);

        Helper.setZeroSpaceConstraint(vw: lbl);
        
        return header
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60;
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Constant.isOpeningScreen {
            return dictData?.openingCheck?[0].items?.count ?? 0;
 
        }
        if Constant.isClosingScreen {
            return dictData?.closingCheck?[0].items?.count ?? 0;
  
        }
        if Constant.isCleaningScreen {
            return dictData?.cleaningCheck?[0].categories?[0].items?.count ?? 0;
            
        }
        return 0
  
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if Constant.isCleaningScreen {
             var cell = tableView.dequeueReusableCell(withIdentifier: "CleaningCheckListCell") as? CleaningCheckListCell
            if cell == nil {
                cell = CleaningCheckListCell(style: .default, reuseIdentifier: "CleaningCheckListCell")
            }
            cell?.delegate = self
            cell?.selectionStyle = .none
            cell?.btnCross?.tag = indexPath.row
            cell?.btnRadio?.setImage(#imageLiteral(resourceName: "androidRadioButtonOn"), for: UIControlState.normal)
            cell?.imgNa?.isHidden = true
            cell?.btnRadio?.isHidden = false
            
            cell?.alpha = 1
            cell?.btnRadio?.isUserInteractionEnabled = true
            if (isMarkAll ?? false) {
                cell?.btnRadio?.isSelected = true
                cell?.btnRadio?.setImage(#imageLiteral(resourceName: "shapeg"), for: UIControlState.normal)
            }
            
            if  tab1Array.object(at: indexPath.row) as? String == "YES" {
                cell?.btnRadio?.isSelected = true
                cell?.btnRadio?.setImage(#imageLiteral(resourceName: "shapeg"), for: UIControlState.normal)
            }
            if  (tab1CrossArray.object(at: indexPath.row) as? Bool)! {
                cell?.btnRadio?.isHidden = true
                
                cell?.btnCross?.isSelected = true
                cell?.imgNa?.isHidden = false
                cell?.alpha = 0.5
                cell?.btnRadio?.isUserInteractionEnabled = false
            }
            if let  cellString = dictData?.cleaningCheck?[0].categories?[0].items?[indexPath.row].task {
                 cell?.lblTitle?.text = cellString
            }
            
            if let  cellString = dictData?.cleaningCheck?[0].categories?[0].items?[indexPath.row].checkDescription  {
                cell?.lblDesc?.text = cellString
                
            }
            return cell!

        }
        else {
         var cell = tableView.dequeueReusableCell(withIdentifier: "CheckListCell") as? CheckListCell
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "CheckListCell") as? CheckListCell
        }
            cell?.delegate = self
            cell?.selectionStyle = .none
            cell?.btnCross?.tag = indexPath.row
            cell?.btnRadio?.setImage(#imageLiteral(resourceName: "androidRadioButtonOn"), for: UIControlState.normal)
            cell?.imgNa?.isHidden = true
            cell?.btnRadio?.isHidden = false
            
            cell?.alpha = 1
            cell?.btnRadio?.isUserInteractionEnabled = true
            if (isMarkAll ?? false) {
                cell?.btnRadio?.isSelected = true
                cell?.btnRadio?.setImage(#imageLiteral(resourceName: "shapeg"), for: UIControlState.normal)
            }
            
            if  tab1Array.object(at: indexPath.row) as? String == "YES" {
                cell?.btnRadio?.isSelected = true
                cell?.btnRadio?.setImage(#imageLiteral(resourceName: "shapeg"), for: UIControlState.normal)
            }
            if  (tab1CrossArray.object(at: indexPath.row) as? Bool)! {
                cell?.btnRadio?.isHidden = true
                
                cell?.btnCross?.isSelected = true
                cell?.imgNa?.isHidden = false
                cell?.alpha = 0.5
                cell?.btnRadio?.isUserInteractionEnabled = false
            }
            if Constant.isOpeningScreen {
                cell?.lblDesc?.text = dictData?.openingCheck?[0].items?[indexPath.row].checkDescription ?? ""
            }
            if Constant.isClosingScreen {
                cell?.lblDesc?.text = dictData?.closingCheck?[0].items?[indexPath.row].checkDescription ?? ""
            }
            return cell!
        }
       
       
     
       
       
        

        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if Constant.isCleaningScreen {
            return 209
        }
        return 109;
    }

}
