//
//  GridView.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 9/9/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import Foundation

protocol FridgeViewDelegate {
    func btnFridgeClicked()
    func btnFreezerClicked()

}

class FridgeView: UIViewController {
    
    @IBOutlet var lblFridge:UILabel?;
    @IBOutlet var lblFreezer:UILabel?;
    @IBOutlet var vwFridge:UIView?;
    @IBOutlet var vwFrizzer:UIView?;

    var delegate:FridgeViewDelegate?


    override func awakeFromNib() {
    }
    override func viewDidLoad() {
        lblFridge?.font = UIFont(name: BOLD_APPFONT, size: Constant.NormalfontSize)
        lblFreezer?.font = UIFont(name: BOLD_APPFONT, size: Constant.NormalfontSize)


    }
    @IBAction func btnFridgeClick() {
        delegate?.btnFridgeClicked()
        lblFridge?.textColor = UIColor.black
    }
    @IBAction func btnFrizzerClick() {
        delegate?.btnFreezerClicked()
        lblFreezer?.textColor = UIColor.black
    }
    func reset() {
        lblFridge?.textColor = UIColor.lightGray
        lblFreezer?.textColor = UIColor.lightGray
    }
   
   
    
}
