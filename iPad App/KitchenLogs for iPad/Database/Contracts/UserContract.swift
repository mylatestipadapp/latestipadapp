//
//  UserContract.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/19/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import KLCommon

protocol IUserContract : class {
    func saveUser(user : User)
    func getUser() -> User
    func updateUser(user : User)
}

class UserContract: NSObject {

}
