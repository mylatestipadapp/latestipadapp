//
//  FoodController.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 9/14/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import Foundation
import Gloss

class FoodController: BaseViewController,UITableViewDelegate,UITableViewDataSource,ApplianceViewDelegate,CustomDatePickerDelegate {
    
    var cookedNameArray:NSMutableArray? = NSMutableArray()
    var hotHoldingNameArray:NSMutableArray? = NSMutableArray()
    var reheatingNameArray:NSMutableArray? = NSMutableArray()
    var minCookingTemp = 0
    var hotHoldingTemp = 0
    var reHeatingTemp = 0
    var completeData:AllCheckMetadata?
    var selectedRow = -1;
    var fridgeTemp = 0
    var foodArray = [JSON]()
    var foodArray2 = [JSON]()
    var foodArray3 = [JSON]()
    var tempMessegaTitle = "Temperature out of range"
    var yearDt:String!
    var entryDefaultsId = ""
    var bussIdOpening = ""
    var locIDOpening = ""









   
    
    @IBOutlet weak var xConstraint: NSLayoutConstraint!
    var heightRow = Constant.ApplianceViewHeight;
    var selectedSelection = -1;
    @IBOutlet var tblFoodList:UITableView?
    @IBOutlet weak var vwTopView: UIView!
    @IBOutlet weak var vwDatePicker: UIView!
    var customPicker:CustomDatePicker!
    var dtVal:UILabel?
    @IBOutlet var btnSave:UIButton?


    let vw = TopHeaderView()

    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        vw.delegate = self;
        self.bussIdOpening = Helper.fetchString(BuisnessIdOpening)
        self.locIDOpening = Helper.fetchString(LocationIdOpening)

        

        self.tblFoodList?.register(UINib(nibName: "ApplianceView", bundle: nil), forCellReuseIdentifier: "ApplianceView")
        setTopView(superView: vwTopView);
        btnSave?.backgroundColor = UIColor.getGreenColor()
        rotated();
        completeData = getAppDelegate().defaultData;
        if getAppDelegate().defaultData?.entryDefaults?.isEmpty == false {

        self.fridgeTemp = getAppDelegate().defaultData?.entryDefaults?[0].fridgeTemp ?? 0;

        hotHoldingNameArray?.removeAllObjects()
        cookedNameArray?.removeAllObjects()
        reheatingNameArray?.removeAllObjects()
        minCookingTemp = Int(self.completeData?.entryDefaults?[0].minCookingTemp ?? 0)
        reHeatingTemp = Int(self.completeData?.entryDefaults?[0].reHeatingTemp ?? 0)
        hotHoldingTemp = Int(self.completeData?.entryDefaults?[0].hotHoldingTemp ?? 0)
        let foods = self.completeData?.entryDefaults?[0].foods
        
        for i in 0..<(foods?.count ?? 0)! {
            if(foods?[i].hotHolding ?? false){
                self.hotHoldingNameArray?.add(foods?[i].name ?? "")
            }
            if(foods?[i].cooked ?? false){
                self.cookedNameArray?.add(foods?[i].name ?? "")
            }
            if(foods?[i].reheating ?? false){
                self.reheatingNameArray?.add(foods?[i].name ?? "")
            }

     }
        self.setfoodArrayData1(dataArray: (cookedNameArray?.count)! , isSelected: "NO")
        self.setfoodArrayData2(dataArray: (hotHoldingNameArray?.count)! , isSelected: "NO")
        self.setfoodArrayData3(dataArray: (reheatingNameArray?.count)! , isSelected: "NO")
        }
    }
    
    override func rotated() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            xConstraint.constant = Constant.horizontalLeading;
        }
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            xConstraint.constant = Constant.verticalLeading;
            
        }
    }
    
    // UITableView Delegates
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {

            vw.view.frame.size.height = Constant.topHeaderHeight;
            vw.setTitle(str: "Food")

            return vw.view;
        }
        let vwAppliance = ApplianceSectionHeader()
        vwAppliance.view.frame.size.height = Constant.ApplianceViewSectionHeaderHeight;
        if(section == 1){
            vwAppliance.lblDesc?.text = "COOKED (Above 82C)"
            
            
        }else if(section == 2)
        {
            vwAppliance.lblDesc?.text = "HOT-HOLDING (Above 63C)"
            
        }else if(section == 3)
        {
            vwAppliance.lblDesc?.text = "REHEATING (Above 75C)"
            
        }
        return vwAppliance.view;
    }
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return Constant.topHeaderHeight;
        }
        return Constant.ApplianceViewSectionHeaderHeight;
    }
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return 0
        }
        if (section == 1) {
            return cookedNameArray?.count ?? 0
            
        }else if(section == 2)
        {
            return hotHoldingNameArray?.count ?? 0
            
        }else if(section == 3)
        {
            return reheatingNameArray?.count ?? 0
            
        }
        return 0
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "ApplianceView") as? ApplianceView
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "ApplianceView") as? ApplianceView
        }
        (cell as! ApplianceView).delegate = self;
        cell?.tag = indexPath.section;
        (cell as! ApplianceView).vwBottomHeight?.constant = 0;
        let cellIndex = indexPath.row
        cell?.selectedRow = indexPath.row;
        cell?.selectedSection = indexPath.section;

        var cellString:String?
        if (indexPath.section == 1) {
            cellString = cookedNameArray? [indexPath.row] as? String ?? ""
            cell?.lblTitle?.text = "\(minCookingTemp)" + "°"
            cell?.txtComment?.placeholder = "Temperature above " + "\(minCookingTemp)" + "°C for 6 seconds else add comment"
            
            
            
        }else if(indexPath.section == 2)
        {
            cellString = hotHoldingNameArray?[indexPath.row] as? String ?? ""
            cell?.lblTitle?.text = "\(hotHoldingTemp)" + "°"
            cell?.txtComment?.placeholder = "Temperature above " + "\(hotHoldingTemp)" + "°C for 6 seconds else add comment"
            
        }else if(indexPath.section == 3)
        {
            cellString = reheatingNameArray?[indexPath.row] as? String ?? ""
            cell?.lblTitle?.text = "\(reHeatingTemp)" + "°"
            cell?.txtComment?.placeholder = "Temperature above " + "\(reHeatingTemp)" + "°C for 6 seconds else add comment"
            
        }
      //  cell?.dropDownButton?.tag = indexPath.section
        cell?.lblTitle?.text = cellString
        cell?.imgvw?.image = #imageLiteral(resourceName: "defaultIcon")
        cell?.txtTemp?.tag = cellIndex
        cell?.txtComment?.tag = cellIndex
        if cellString?.caseInsensitiveCompare("chicken") == ComparisonResult.orderedSame   {
            cell?.imgvw?.image = #imageLiteral(resourceName: "group2")
            
        }
        cell?.txtTemp?.tag = indexPath.section;
        cell?.txtComment?.tag = indexPath.section;
        
        
        //            cell.setupCellContentWithHeight(height: 25 * 40)
        //                cell?.myLabel?.text = "\(foodCheckArray[indexPath.row])"
      //  cell?.enterCommentTextField?.delegate = self
       // cell?.enterTempTextField?.delegate = self

        if indexPath.section == selectedSelection && indexPath.row == selectedRow {
            if heightRow == Constant.ApplianceViewHeight {
                (cell as! ApplianceView).vwBottomHeight?.constant = 0;
            }
            else {
                (cell as! ApplianceView).vwBottomHeight?.constant = CGFloat(Constant.ApplianceViewHeight);
                
            }
        }
        
        // Configure the cell...
        cell?.selectionStyle = .none

        
        return cell!
    }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == selectedSelection && indexPath.row == selectedRow {
            //tableView.beginUpdates()
             return CGFloat(heightRow);
            //tableView.endUpdates()
            }
        return Constant.ApplianceViewHeight;
    }
    
    func dropDownClicked(val: Bool,section: Int,row:Int) {
        selectedSelection = section;
        selectedRow = row
        if val {
            heightRow = Constant.ApplianceViewHeight;
            
        }
        else {
            heightRow = Constant.ApplianceViewCellHeight;
            
        }
        self.tblFoodList?.reloadData()
    }
    func showingCustomDatePicker(){
        vwDatePicker.isHidden = false;
        customPicker = CustomDatePicker(nibName: "CustomDatePicker", bundle: nil);
        customPicker.delegate = self;
        self.vwDatePicker.addSubview(customPicker.view);
        self.topviewConstraint(vwTop: customPicker.view);
        
    }
    
   func selectedDatePickerValue(value:String,yearDt:String) {
        dtVal?.text = value;
    }
    
    func clickedDone() {
        vwDatePicker.isHidden = true;
    }
    override func clickedToDateTime(sender: UILabel) {
        dtVal = sender;
        showingCustomDatePicker();
        
    }
//    func textEditingClicked(txtField: UITextField) {
//    if(txtField.tag >= 555)
//    {
//    
//    let cell1 : ApplianceView? = self.tblFoodList?.cellForRow(at: NSIndexPath(row: txtField.tag - 555, section: 0) as IndexPath) as? ApplianceView
//    
//    
//    
//    if(Int(txtField.text ?? "") ?? 0 > fridgeTemp){
//    
//    // self.dropDowntapped(index: txtField.tag - 555, section: cell1?.dropDownButton?.tag ?? 0)
//    
//    }
//    
//    
//    self.delegate?.tableViewCell2FridgeDelegate(index: txtField.tag - 555 , temp: cell1?.txtTemp?.text ?? "", comment: cell1?.txtComment?.text ?? "")
//    }else
//    {
//    let cell1 : ApplianceView? = self.tableView?.cellForRow(at: NSIndexPath(row: txtField.tag, section: 1) as IndexPath) as? ApplianceView
//    
//    if(Int(txtField.text ?? "") ?? 0 >  -18){
//    
//    // self.dropDowntapped(index: txtField.tag, section: cell1?.dropDownButton?.tag ?? 0)
//    
//    }
//    self.delegate?.tableViewCell2FreezerDelegate(index: txtField.tag, temp: cell1?.txtTemp?.text ?? "", comment: cell1?.txtComment?.text ?? "")
//    }
//    
//    }

    func textEditingClicked(txtField: UITextField,row:Int) {
    let section = txtField.tag
    
    let row = row;
    
    let indexpath = NSIndexPath(row: row, section: section)
    let cell1 : ApplianceView? = self.tblFoodList?.cellForRow(at: indexpath as IndexPath) as? ApplianceView
    // let cell: AppliancesTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "AppliancesTableViewCell") as? AppliancesTableViewCell
    
    if(section == 1){
    foodArray[row] = (["temp":cell1?.txtTemp?.text ?? "", "comment":cell1?.txtComment?.text ?? ""])
    
    }else if(section == 2){
    foodArray2[row] = (["temp":cell1?.txtTemp?.text ?? "", "comment":cell1?.txtComment?.text ?? ""])
    
    }else if(section == 3)
    {
    foodArray3[row] = (["temp":cell1?.txtTemp?.text ?? "", "comment":cell1?.txtComment?.text ?? ""])
        }
    let temp = cell1?.txtTemp?.text ?? ""
    let comment = cell1?.txtComment?.text ?? ""
    if(section ==  1)
    {
    if(Int(temp) ?? 0 < minCookingTemp && comment == ""){
    let alert = UIAlertController(title: tempMessegaTitle, message: "Entered value should be Above or equal to " + "\(minCookingTemp)" + "°" + ", else enter the comment", preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
    super.present(alert, animated: true, completion: nil)
    
    //self.dropDowntapped(index: textField.tag, section: cell1?.dropDownButton?.tag ?? 0)
    
    }
   // self.dropDowntapped(index: row, section: section - 1)
    
    
    }else if(section == 2)
    {
    if(Int(temp) ?? 0 < hotHoldingTemp && comment == ""){
    let alert = UIAlertController(title: tempMessegaTitle, message: "Entered value should be Above or equal to " + "\(hotHoldingTemp)" + "°" + ", else enter the comment", preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
    super.present(alert, animated: true, completion: nil)
    
    //self.dropDowntapped(index: textField.tag, section: cell1?.dropDownButton?.tag ?? 0)
    
    }
   // self.dropDowntapped(index: row, section: section - 1)
    
    //            self.dropDowntapped(index: textField.tag, section: cell1?.dropDownButton?.tag)
    }else if(section == 3)
    {
    if(Int(temp ) ?? 0 < reHeatingTemp && comment == ""){
    let alert = UIAlertController(title: tempMessegaTitle, message: "Entered value should be Above or equal to " + "\(reHeatingTemp)" + "°" + ", else enter the comment", preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
    super.present(alert, animated: true, completion: nil)
    
    //self.dropDowntapped(index: textField.tag, section: cell1?.dropDownButton?.tag ?? 0)
    
    }
    //self.dropDowntapped(index: row, section: section - 1)
        }
    }
    func setfoodArrayData1(dataArray:Int , isSelected:String){
        foodArray.removeAll()
        for _ in 0 ..< dataArray {
            foodArray.append(["temp":"", "comment":""])
        }
    }
    
    func setfoodArrayData2(dataArray:Int , isSelected:String){
        foodArray2.removeAll()
        for _ in 0 ..< dataArray {
            foodArray2.append(["temp":"", "comment":""])
        }
    }
    func setfoodArrayData3(dataArray:Int , isSelected:String){
        foodArray3.removeAll()
        for _ in 0 ..< dataArray {
            foodArray3.append(["temp":"", "comment":""])
        }
    }
    
    func postFoodCheck() {
        if self.yearDt == nil {
            self.yearDt = getCurrentDate();
            return;
        }
        var assetsArray = [ApplianceParam]()
        
        var cookedArray = [CookedType]()
        var reHeatingArray = [CookedType]()
        var hotHoldingArray = [CookedType]()
        
        for i in 0..<(self.cookedNameArray?.count ?? 0) {
            let name = "\(self.self.cookedNameArray?[i])"
            if foodArray.count > 0 {
                let temp = self.self.foodArray[i]["temp"]
                let comment = self.self.foodArray[i]["comment"]
                var cookedItem = CookedType.init(name: name, temperature: temp as? String ?? "", comment: comment as! String ?? "", commentRequired: false, type: "cooked")
                cookedArray.append(cookedItem);
            }
        }
        for i in 0..<(self.hotHoldingNameArray?.count ?? 0) {
            let name = "\(self.self.hotHoldingNameArray?[i])"
            let temp = self.foodArray2[i]["temp"]
            let comment = self.foodArray2[i]["comment"]
            var cookedItem = CookedType.init(name: name, temperature: temp as! String ?? "", comment: comment as? String ?? "", commentRequired: false, type: "hotHolding")
            hotHoldingArray.append(cookedItem);
        }
        
        for i in 0..<(self.reheatingNameArray?.count ?? 0) {
            let name = "\(self.reheatingNameArray?[i])"
            let temp = self.foodArray3[i]["temp"]
            let comment = self.foodArray3[i]["comment"]
            var cookedItem = CookedType.init(name: name, temperature: temp as! String ?? "", comment: comment as? String ?? "", commentRequired: false, type: "hotHolding")
            reHeatingArray.append(cookedItem);
        }
        var _createdBy = CreatedBy.init(userType:"site-admin" , name: entryDefaultsId, businessId: bussIdOpening, authenticated: true)
        var templateName = ""
        if self.completeData?.entryDefaults?.isEmpty == false {

         templateName = self.completeData?.entryDefaults?[0].name ?? ""
        }
        
        let hrs = self.getTime(strDate: (dtVal?.text!) ?? "");
        let mins = self.getMins(strDate: dtVal?.text ?? "");
        
        let param = FoodPost.init(templates: templateName, entryTm: yearDt, entryMin: mins, locationIds: self.locIDOpening, entryHr: hrs, buisnessId: self.bussIdOpening, assetEntries: assetsArray, completed: true,createdBy:[_createdBy])
        
        let post = ChecksService.postFood(post: param, callback: { (response, error) in
            if error == nil {
                print("post Food",response);

            }
            LoadingOverlay.shared.hideOverlayView()
            let alert = UIAlertController(title: "Saved", message: "", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            super.present(alert, animated: true, completion: nil)

        })

    }
    @IBAction func btnSaveClick() {
        LoadingOverlay.shared.showOverlay(view:self.view, msg: "")
        var isDataMissing = false
        for i in 0..<foodArray.count{
            if(foodArray[i]["temp"] as? String == "" && foodArray[i]["comment"] as? String == "")
            {
                isDataMissing = true
                break
            }
        }
        if(isDataMissing) {
            let alert = UIAlertController(title: "Missing fields", message: "Please complete the form", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            super.present(alert, animated: true, completion: nil)
            LoadingOverlay.shared.hideOverlayView()
            return
        }
        
        
        let cookedArray = NSMutableArray()
        let reHeatingArray = NSMutableArray()
        let hotHoldingArray = NSMutableArray()
        
        var missing = true;
        
        
        for i in 0..<(cookedNameArray?.count ?? 0) {
            //                let commentRequiredCooked = completeData?["entryDefaults"][0]["foods"][i]["cooked"].string ?? ""
            let name = "\(cookedNameArray?[i])"
            var temp = foodArray[i]["temp"] as? String
            var comment = foodArray[i]["comment"] as? String
            cookedArray.add(["name":name,"type":"cooked","temperature":temp ?? "0","commentRequired":false,"comment":comment ?? "0"])
            if temp == "" {
                temp = "0"
            }
            if comment == "" {
                comment = ""
            }
            
            if Int(temp!)! >= minCookingTemp || (comment?.characters.count)! > 0  {
                missing = false;
            }
            
        }
        if missing {
            
            
            for i in 0..<(hotHoldingNameArray?.count ?? 0) {
                //                let commentRequiredHotHold = completeData?["entryDefaults"][0]["foods"][i]["hotHolding"].string ?? ""
                let name = "\(hotHoldingNameArray?[i])"
                var temp = foodArray2[i]["temp"] as? String
                var comment = foodArray2[i]["comment"] as? String
                hotHoldingArray.add(["name":name,"type":"hotHolding","temperature":temp ?? "0","commentRequired":false,"comment":comment ?? "0"])
                if temp == "" {
                    temp = "0"
                }
                if comment == "" {
                    comment = ""
                }
                if Int(temp!)! >= hotHoldingTemp || (comment?.characters.count)! > 0 {
                    missing = false;
                }
                
            }
        }
        
        if missing {
            
            for i in 0..<(reheatingNameArray?.count ?? 0) {
                //                let commentRequiredReHeat = completeData?["entryDefaults"][0]["foods"][i]["reheating"].string ?? ""
                let name = "\(reheatingNameArray?[i])"
                var temp = foodArray3[i]["temp"] as? String
                var comment = foodArray3[i]["comment"] as? String
                reHeatingArray.add(["name":name,"type":"reHeating","temperature":temp ?? "0","commentRequired":false,"comment":comment ?? "0"])
                if temp == "" {
                    temp = "0"
                }
                if comment == "" {
                    comment = ""
                }
                if Int(temp!)! >= reHeatingTemp || (comment?.characters.count)! > 0 {
                    missing = false;
                }
            }
        }
        
        if missing {
            LoadingOverlay.shared.hideOverlayView()
            let alert = UIAlertController(title: "Missing fields", message: "Please complete the form", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            super.present(alert, animated: true, completion: nil)
            
            return;
        }
        postFoodCheck()
    }
    
}
