//
//  ApplianceScreenController.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 9/14/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import Foundation
import Gloss

class ApplianceScreenController: BaseViewController,SJSegmentedViewControllerDelegate,CustomDatePickerDelegate,FridgeViewDelegate,ApplianceControllerDelegate {
    
    @IBOutlet weak var xConstraint: NSLayoutConstraint!
    @IBOutlet weak var vwTopView: UIView!
    var tblLoaded = false;
    
    var segment:SJSegmentedViewController?
    @IBOutlet weak var vwDatePicker: UIView!
    var customPicker:CustomDatePicker!
    var dtVal:UILabel?
    @IBOutlet var vwContent:UIView?
    @IBOutlet var btnSave:UIButton?
    var tempMessegaTitle = "Temperature out of range"
    var frigdeArray = [JSON]()
    var freezerArray = [JSON]()
    var fridgeTemp = 0
    var yearDt:String!
    var completeData:AllCheckMetadata?
    var entryDefaultsId = ""
    var bussIdOpening = ""
    var locIDOpening = ""







    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        setTopView(superView: vwTopView);
        createSegmentViews();
        rotated();
        if getAppDelegate().defaultData?.entryDefaults?.isEmpty == false {

        self.fridgeTemp = getAppDelegate().defaultData?.entryDefaults?[0].fridgeTemp ?? 0;
        }
        self.completeData = getAppDelegate().defaultData;
        self.bussIdOpening = Helper.fetchString(BuisnessIdOpening)
        self.locIDOpening = Helper.fetchString(LocationIdOpening)


        

    }
    
    
    override func rotated() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            xConstraint.constant = Constant.horizontalLeading;
        }
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            xConstraint.constant = Constant.verticalLeading;
            
        }
    }
    
    
    func createSegmentViews(){
        
        let cntrl1 = ApplianceController(nibName: "ApplianceController", bundle: nil);
        cntrl1.dictData = getAppDelegate().defaultData
        cntrl1.isFridge = true
        cntrl1.delegate = self
        
        cntrl1.navigationItem.titleView = UIView();
        cntrl1.navigationItem.titleView?.tag = 4;
        
        let cntrl2 = ApplianceController(nibName: "ApplianceController", bundle: nil);
        cntrl2.isFreezer = true
        cntrl2.dictData = getAppDelegate().defaultData
        cntrl2.delegate = self
        

        cntrl2.navigationItem.titleView = UIView();
        cntrl2.navigationItem.titleView?.tag = 5;
        
        let header = TopHeaderView(nibName: "TopHeaderView", bundle: nil);
        header.delegate = self;
        if getAppDelegate().defaultData?.entryDefaults?.isEmpty == false {

        self.setfridgeArrayData(dataArray: getAppDelegate().defaultData?.entryDefaults?[0].numberOfFridges ?? 0 , isSelected: "NO")
        self.setfreezerArrayData(dataArray: getAppDelegate().defaultData?.entryDefaults?[0].numberOfFreezers ?? 0 , isSelected: "NO")
        }
        segment = SJSegmentedViewController(headerViewController: header, segmentControllers: [cntrl1,cntrl2]);
        
        segment?.segmentViewHeight = Constant.FridgeVwHeight;
        segment?.delegate = self
        
        segment?.headerViewHeight = Constant.topHeaderHeight;
        segment?.showsHorizontalScrollIndicator = false;
        segment?.showsVerticalScrollIndicator = false;
        segment?.segmentShadow.color = UIColor.white
        
        self.vwContent?.addSubview((segment?.view)!);
        freezer.delegate = self
        fridge.delegate = self
        freezer.reset()
        fridge.btnFridgeClick()
        btnSave?.backgroundColor = UIColor.getGreenColor()
        Helper.setZeroSpaceConstraint(vw: (segment?.view)!)
        header.setTitle(str: "Appliance")
    }
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        
    }
    func didSelectSegmentAtIndex(_ index: Int) {
        
    }

    func showingCustomDatePicker(){
        vwDatePicker.isHidden = false;
        customPicker = CustomDatePicker(nibName: "CustomDatePicker", bundle: nil);
        customPicker.delegate = self;
        self.vwDatePicker.addSubview(customPicker.view);
        self.topviewConstraint(vwTop: customPicker.view);
        
    }
    
    func selectedDatePickerValue(value:String,yearDt:String) {
        dtVal?.text = value;
    }
    
    func clickedDone() {
        vwDatePicker.isHidden = true;
    }
    override func clickedToDateTime(sender: UILabel) {
        dtVal = sender;
        showingCustomDatePicker();
        
    }
    func btnFridgeClicked() {
       freezer.reset()
        segment?.segmentedScrollView.contentView?.movePageToIndex(0, animated: true);

    }
    func btnFreezerClicked() {
        fridge.reset()
        segment?.segmentedScrollView.contentView?.movePageToIndex(1, animated: true);

    }
    func tableViewCell2FreezerDelegate(index: Int, temp: String, comment: String) {
        
        if(Int(temp) ?? 0 > -18 && comment == ""){
            
            let alert = UIAlertController(title: tempMessegaTitle, message: "Entered value should be less than or equal to " + "\(-18)" + "°" + ", else enter the comment", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            super.present(alert, animated: true, completion: nil)
            
        }
        
        freezerArray[index] = ["temp":temp, "comment":comment]
        
    }
    
    func tableViewCell2FridgeDelegate(index: Int, temp: String, comment: String) {
        
        if(Int(temp) ?? 0 > fridgeTemp && comment == ""){
            
            let alert = UIAlertController(title: tempMessegaTitle, message: "Entered value should be less than or equal to " + "\(fridgeTemp)" + "°" + ", else enter the comment", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            super.present(alert, animated: true, completion: nil)
            
        }
        
        frigdeArray[index] = ["temp":temp, "comment":comment]
        
    }
    func setfridgeArrayData(dataArray:Int , isSelected:String){
        frigdeArray.removeAll()
        for _ in 0 ..< dataArray {
            frigdeArray.append(["temp":"", "comment":""])
        }
    }
    func setfreezerArrayData(dataArray:Int , isSelected:String){
        freezerArray.removeAll()
        for _ in 0 ..< dataArray {
            freezerArray.append(["temp":"", "comment":""])
        }
    }
    
    func postApplianceCheck() {
        if self.yearDt == nil {
            self.yearDt = getCurrentDate();
            return;
        }
        var assetsArray = [ApplianceParam]()
        if self.completeData?.entryDefaults?.isEmpty == false {

        let assetsArrayarr1 = self.completeData?.entryDefaults?[0].numberOfFridges ?? 0
        for i in 0..<assetsArrayarr1 {
            let temp = self.frigdeArray[i]["temp"] ?? ""
            let comment = self.frigdeArray[i]["comment"] ?? ""
            let commentrequired = ((self.frigdeArray[i]["comment"] as! String).characters.count ?? 0) > 0 ? true : false
            let _assets = ApplianceParam.init(type: "fridge", temperature: temp as! String, comment: comment as! String, commentRequired: commentrequired, unit: "C")
            assetsArray.append(_assets)
        }
        let assetsArrayarr2 = self.completeData?.entryDefaults?[0].numberOfFreezers ?? 0
        
        for i in 0..<assetsArrayarr2 {
            let temp = self.freezerArray[i]["temp"] ?? ""
            let comment = self.freezerArray[i]["comment"] ?? ""
            let commentrequired = ((self.freezerArray[i]["comment"] as! String).characters.count ?? 0) > 0 ? true : false
            let _assets = ApplianceParam.init(type: "freezer", temperature: temp as! String, comment: comment as! String, commentRequired: commentrequired, unit: "C")
            assetsArray.append(_assets)
        }
 
        var _createdBy = CreatedBy.init(userType:"site-admin" , name: entryDefaultsId, businessId: bussIdOpening, authenticated: true)
        
        
        let templateName = self.completeData?.entryDefaults?[0].name ?? ""

        let hrs = self.getTime(strDate: (dtVal?.text!) ?? "");
        let mins = self.getMins(strDate: dtVal?.text ?? "");
        
        let param = AppliancePost.init(templates: templateName, entryTm: yearDt, entryMin: mins, locationIds: self.locIDOpening, entryHr: hrs, buisnessId: self.bussIdOpening, assetEntries: assetsArray, completed: true,createdBy:[_createdBy])
        
        let post = ChecksService.postAppliance(post: param, callback: { (response, error) in
            if error == nil {
                print("post Appliance",response);
            }
            LoadingOverlay.shared.hideOverlayView()
            let alert = UIAlertController(title: "Saved", message: "", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            super.present(alert, animated: true, completion: nil)

        })
        }
    }
    @IBAction func btnSaveClick() {
        LoadingOverlay.shared.showOverlay(view:self.view, msg: "")
        
        
        var isDataMissing = false
        var missing = true;
        for i in 0..<frigdeArray.count{
            var temp = frigdeArray[i]["temp"] as? String;
            var comment = frigdeArray[i]["comment"] as? String;
            
            if(temp == "" && comment == "")
            {
                isDataMissing = true
                break
            }
            if temp == "" {
                temp = "500"
            }
            if comment == "" {
                comment = ""
            }
            
            if Int(temp!)! <= fridgeTemp || (comment?.characters.count)! > 0  {
                missing = false;
            }
        }
        
        for i in 0..<freezerArray.count{
            var temp = freezerArray[i]["temp"] as? String;
            var comment = freezerArray[i]["comment"] as? String;
            if(temp == "" && comment == "")
            {
                isDataMissing = true
                break
            }
            if missing {
                if temp == "" {
                    temp = "500"
                }
                if comment == "" {
                    comment = ""
                }
                
                if Int(temp!)! <= -18 || (comment?.characters.count)! > 0  {
                    missing = false;
                }
            }
        }
     
        if(missing) {
            let alert = UIAlertController(title: "Missing fields", message: "Please complete the form", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            super.present(alert, animated: true, completion: nil)
            LoadingOverlay.shared.hideOverlayView()
            return
        }

        postApplianceCheck()
    }

}
