//
//  PestInjuryView.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 14/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit
import DropDown
import KLCommon
class PestInjuryView: BaseViewController,CustomDatePickerDelegate{

    @IBOutlet weak var xConstaint: NSLayoutConstraint!
    @IBOutlet weak var vwDatePicker: UIView!
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var btnSaveLog: UIButton!
    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var txtSummary: UITextField!
    @IBOutlet weak var vwSubTop: UIView!
    @IBOutlet weak var btnPestInjury: UIButton!
    @IBOutlet weak var vwTop: UIView!
    var yearDt:String!
    var dateTime:UILabel!
    var customPicker:CustomDatePicker!
    var type:String!
    var dropDown:DropDown!
    override func viewDidLoad() {
        super.viewDidLoad()
        dropDown = DropDown()
        setTopView(superView: vwTop);
        setSubTopView(superView: self.vwSubTop)
    
        if type == "INJURY" {
        topHeader.setTitle(str: "Injury Log")
        } else {
        topHeader.setTitle(str: "Pest Control")
        }
        setDesignFont();
        rotated();
        // Do any additional setup after loading the view.
    }
    
    
    func setDesignFont(){
        if type == "INJURY" {

        self.btnPestInjury.setTitle("Type of injury", for: .normal);
        }
        else {
            self.btnPestInjury.setTitle("Sign of Pest", for: .normal);
  
        }
        txtSummary.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        lblSummary.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        btnPestInjury.titleLabel?.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)!
        btnSaveLog.titleLabel?.font = UIFont(name: BOLD_APPFONT, size: Constant.SaveLogfontSize)!
        vwBottom.backgroundColor = UIColor.getGreenColor();
        lblSummary.textColor = UIColor.getGreenColor();
        
    }
    
    override func rotated() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            xConstaint.constant = Constant.horizontalLeading;
        }
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            xConstaint.constant = Constant.verticalLeading;
            
        }
    }


    override func clickedToDateTime(sender: UILabel) {
        dateTime = sender;
        showingCustomDatePicker();
    
    }

   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickedToPestInjury(_ sender: Any) {
         if type == "INJURY" {
        showingDropDownList(listArray: ["Minor","Serious","Death"], supperView: self.btnPestInjury, type: "INJURY")
         } else {
        showingDropDownList(listArray: ["Droppings","Cockroaches","Others"], supperView: self.btnPestInjury, type: "")
        }
    }
    
    // Showing List View
    func showingDropDownList(listArray:[String],supperView:UIView,type:String){
        dropDown.anchorView = supperView;
        //  dropDown.dataSource = listArray;
        dropDown.width = Constant.DROPDOWN_WIDTH;
        dropDown.dataSource = listArray
        dropDown.direction = .bottom
        // Action triggered on selection
        dropDown?.reloadAllComponents()
        dropDown.show()
        // self.view.addSubview(dropDown);
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if type == "INJURY" {
                self.btnPestInjury.setTitle(item, for: .normal);
            } 
        }
        
        
    }
    
    
    func showingCustomDatePicker(){
        vwDatePicker.isHidden = false;
        customPicker = CustomDatePicker(nibName: "CustomDatePicker", bundle: nil);
        customPicker.delegate = self;
        self.vwDatePicker.addSubview(customPicker.view);
        self.topviewConstraint(vwTop: customPicker.view);
        
    }
    
    func selectedDatePickerValue(value:String,yearDt:String) {
        dateTime.text = value;
        self.yearDt = yearDt;
        print("Date Time is:\(dateTime.text)")
        
    }
    
    func clickedDone() {
        vwDatePicker.isHidden = true;
    }



    @IBAction func clikedToSaveLog(_ sender: Any) {
        if type == "INJURY" {
            postInjuryApi();
        } else {
           postPestApi(); 
        }
    }
    
    func postPestApi(){
        if self.yearDt == nil {
            self.yearDt = getCurrentDate();
            return;
        }
       let hrs = self.getTime(strDate: dateTime.text!);
        let mins = self.getMins(strDate: dateTime.text!);
        LoadingOverlay.shared.showOverlay(view:self.view, msg: "")
        let pest = PestPost.init(locationId: Helper.fetchString(LocationIdOpening), businessId: Helper.fetchString(BuisnessIdOpening), date: self.yearDt, hrs: hrs, mins: mins, summary:txtSummary.text ?? "", signofPest:self.btnPestInjury.titleLabel?.text ?? "");
        let _ = ChecksService.postPest(post: pest) {(response, error) in
            if(error == nil) {
                LoadingOverlay.shared.hideOverlayView()
                print("response Code is:\(response?.code)")
                if (response?.code == 200) {
                self.alert("", alertMessage: "Saved Data Successfully", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                    
                })
                } else {
                  LoadingOverlay.shared.hideOverlayView()   
                }
   
            }else {
                self.alert("", alertMessage:"Failed to communicate to server", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                    
                })
                LoadingOverlay.shared.hideOverlayView()
            }
        }
    }
    
    func postInjuryApi(){
        if self.yearDt == nil {
            self.yearDt = getCurrentDate();
            return;
        }
        let hrs = self.getTime(strDate: dateTime.text!);
        let mins = self.getMins(strDate: dateTime.text!);
        LoadingOverlay.shared.showOverlay(view:self.view, msg: "")
        
        let injury = InjuryPost.init(locationId: Helper.fetchString(LocationIdOpening), businessId: Helper.fetchString(BuisnessIdOpening), date: self.yearDt, hrs: hrs, mins: mins, summary: txtSummary.text ?? "", injuryType: self.btnPestInjury.titleLabel?.text ?? "")
        let _ = ChecksService.postInjury(post: injury) {(response, error) in
            if(error == nil) {
                LoadingOverlay.shared.hideOverlayView()
                print("response Code is:\(response?.code)")
                if (response?.code == 200) {
                    self.alert("", alertMessage: "Saved Data Successfully", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                        
                    })
                } else {
                    LoadingOverlay.shared.hideOverlayView()
                }
                
            }else {
                self.alert("", alertMessage:"Failed to communicate to server", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                    
                })
                LoadingOverlay.shared.hideOverlayView()
            }
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
