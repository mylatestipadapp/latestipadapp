//
//  ViewLogController.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 10/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit
import KLCommon


class ViewLogController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var xConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblViewLog: UITableView!
    @IBOutlet weak var vwTopView: UIView!
    var viewLogsItems : [ViewLogsResponse.ViewLogs] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        getViewLog();
        setTopView(superView: vwTopView);
        NotificationCenter.default.addObserver(self, selector: #selector(ViewLogController.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        self.tblViewLog.register(UINib(nibName: "ViewLogCell", bundle: nil), forCellReuseIdentifier: "ViewLogCell")
        rotated();
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.st
    }
    
    
    override func rotated() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            xConstraint.constant = Constant.horizontalLeading;
        }
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            xConstraint.constant = Constant.verticalLeading;
            
        }
    }
    
    
    // UITableView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewLogsItems.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:ViewLogCell?
        let cellIdentifier = "ViewLogCell"
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ViewLogCell
        if cell == nil {
            cell = ViewLogCell(style: UITableViewCellStyle.value1, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none;
        cell?.bindData(viewLog: viewLogsItems[indexPath.row]);
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func getViewLog(){
        let viewLog = ViewLogsPost.init(locationId: Helper.fetchString(LocationIdOpening), businessId: Helper.fetchString(BuisnessIdOpening))
        let _ = ChecksService.getViewLogs(post: viewLog) {(response, error) in
          if(error == nil) {
            let resp : ViewLogsResponse = response as! ViewLogsResponse
            if ((resp.code == nil && resp.data != nil) || resp.code == 200) {
                self.viewLogsItems = [];
                self.viewLogsItems.append(contentsOf: resp.data!)
                
                self.tblViewLog.reloadData();
            }
            }
        }
            
        
    }

    


    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
