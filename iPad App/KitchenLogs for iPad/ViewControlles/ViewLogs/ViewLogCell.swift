//
//  ViewLogCell.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 10/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit
import KLCommon
class ViewLogCell: UITableViewCell {
    @IBOutlet weak var namelabel: UILabel?
    @IBOutlet weak var templateLabel: UILabel?
    @IBOutlet weak var timeLabel: UILabel?
    @IBOutlet weak var datelabel: UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        setDesignFont();
        // Configure the view for the selected state
    }
    
    func setDesignFont(){
        namelabel?.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        templateLabel?.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        timeLabel?.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        datelabel?.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        timeLabel?.textColor = UIColor.getGreenColor();
        datelabel?.textColor = UIColor.getGreenColor();
    }
    
    func bindData(viewLog:ViewLogsResponse.ViewLogs){
        
        namelabel?.text = viewLog.name
        templateLabel?.text = viewLog.type
        timeLabel?.text = viewLog.entryTime
        datelabel?.text = viewLog.entryDate
 
        
    }
    
    
}
