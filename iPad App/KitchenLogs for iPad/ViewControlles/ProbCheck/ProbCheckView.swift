//
//  ProbCheckView.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 14/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit
import KLCommon
class ProbCheckView: BaseViewController,UITableViewDelegate,UITableViewDataSource,CustomDatePickerDelegate,AfterUseTableViewCellDelegate {
    @IBOutlet weak var vwDatePicker: UIView!
    @IBOutlet weak var btnSaveLog: UIButton!
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var slider: UISwitch!
    @IBOutlet weak var LblProbeCheck: UILabel!
    @IBOutlet weak var vwSlide: UIView!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var vwSubTop: UIView!
    @IBOutlet weak var vwTableHeader: UIView!
    @IBOutlet weak var xConstraint: NSLayoutConstraint!
    @IBOutlet weak var vwTop: UIView!
    var isIceTest:Bool!
    var isWaterTest:Bool!
    var isProbe:Bool!
    var customPicker:CustomDatePicker!
    var dateTime:UILabel!
    var yearDt:String!
    var probeArray = [ProbeObject]();
    override func viewDidLoad() {
        super.viewDidLoad()
        isIceTest = false;
        isWaterTest = false;
        slider.isOn = false;
        let probbj1 = ProbeObject(title: "ICE TEST", des: "Put probe in ice water and see that the probe shows 0 degree", isSelected: false);
        let probbj2 = ProbeObject(title: "BOILING WATER TEST", des: "Put probe in boiling water and see it shows 100 degree", isSelected: false);
        probeArray = [probbj1,probbj2];
        setTopView(superView: vwTop);
        setSubTopView(superView: self.vwSubTop)
        topHeader.setTitle(str: "Probe Check")
        self.tblList.register(UINib(nibName: "ProbCheckCell", bundle: nil), forCellReuseIdentifier: "ProbCheckCell")
        self.tblList.tableHeaderView = vwTableHeader;
        setDesignFont()
        rotated();
        

        // Do any additional setup after loading the view.
    }
    
    
    override func rotated() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            xConstraint.constant = Constant.horizontalLeading;
        }
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            xConstraint.constant = Constant.verticalLeading;
            
        }
    }
    
    
    @IBAction func clickedToSlider(sender:UISwitch!) {
        if (sender.isOn == true){
            print("on")
            isProbe = true;
        }
        else{
            print("off")
            isProbe = false;
        }
        
    }
    
    
    func setDesignFont(){
        LblProbeCheck.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        btnSaveLog.titleLabel?.font = UIFont(name: BOLD_APPFONT, size: Constant.SaveLogfontSize)!
        vwBottom.backgroundColor = UIColor.getGreenColor();
        
        
    }
    
    
    override func clickedToDateTime(sender: UILabel) {
        dateTime = sender;
        showingCustomDatePicker();
    }
    
    @IBAction func clickedToSaveLog(_ sender: Any) {
        postProbeApi();
    }
    
    
    // UITableView Delegates
    
    // UITable View numberOfRowsInSection Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return probeArray.count;
    }
    
    // UITable View cellForRowAt Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:ProbCheckCell?
        let cellIdentifier = "ProbCheckCell"
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ProbCheckCell
        if cell == nil {
            cell = ProbCheckCell(style: UITableViewCellStyle.value1, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none;
        cell?.delegate = self;
        cell?.radioButton?.tag = indexPath.row;
        cell?.bindData(probObj: probeArray[indexPath.row], index: indexPath.row);
        return cell!
        
    }
    
    // UITable View didSelectRowAt Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
    // Cell Delegates
    func crossButtonclicked(index:Int) {
        
    }
    
    func radioButtonClickedAfterUseTableViewCell(index:Int,updateObj:ProbeObject) {
        if index == 0 {
            if updateObj.isSelected == true {
                isIceTest = true;
            } else {
               isIceTest = false;
            }
        } else {
            if updateObj.isSelected == true {
                isWaterTest = true;
            } else {
                isWaterTest = false;
            }
        }
        if isWaterTest == true && isIceTest == true {
            slider.isOn = true;
            
        } else {
            slider.isOn = false;
        }
        
        probeArray[index] = updateObj;
        print("Update Object:\(updateObj.isSelected)")
        tblList.reloadData();
    }
    
    
    func showingCustomDatePicker(){
        vwDatePicker.isHidden = false;
        customPicker = CustomDatePicker(nibName: "CustomDatePicker", bundle: nil);
        customPicker.delegate = self;
        self.vwDatePicker.addSubview(customPicker.view);
        self.topviewConstraint(vwTop: customPicker.view);
        
    }
    
    func selectedDatePickerValue(value:String,yearDt:String) {
        dateTime.text = value;
        self.yearDt = yearDt;
        
    }
    
    func clickedDone() {
        vwDatePicker.isHidden = true;
    }
    
    
    func postProbeApi(){
        if self.yearDt == nil {
            self.yearDt = getCurrentDate();
            return;
        }
        let hrs = self.getTime(strDate: dateTime.text!);
        let mins = self.getMins(strDate: dateTime.text!);
        LoadingOverlay.shared.showOverlay(view:self.view, msg: "")
        
        let probe = ProbePost.init(locationId: Helper.fetchString(LocationIdOpening), businessId: Helper.fetchString(BuisnessIdOpening), date: self.yearDt, hrs: hrs, mins: mins, isIceTestWorking: isIceTest, isBoilingWaterTestWorking: isWaterTest)
        let _ = ChecksService.postProbeCheck(post: probe)  {(response, error) in
            if(error == nil) {
                LoadingOverlay.shared.hideOverlayView()
                print("response Code is:\(response?.code)")
                if (response?.code == 200) {
                    self.alert("", alertMessage: "Saved Data Successfully", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                        
                    })
                } else {
                    LoadingOverlay.shared.hideOverlayView()
                }
                
            }else {
                self.alert("", alertMessage:"Failed to communicate to server", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                    
                })
                LoadingOverlay.shared.hideOverlayView()
            }
   
        }
        
        
        
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

class ProbeObject:NSObject {
    var title:String!
    var des:String!
    var isSelected:Bool!
    
    // Init Method For Address
    init(title:String, des:String,isSelected:Bool) {
        self.title = title;
        self.des = des;
        self.isSelected = isSelected;
    }
}




