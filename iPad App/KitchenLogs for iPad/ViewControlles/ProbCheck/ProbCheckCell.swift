//
//  ProbCheckCell.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 14/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit
protocol AfterUseTableViewCellDelegate {
    func crossButtonclicked(index:Int)
    func radioButtonClickedAfterUseTableViewCell(index:Int,updateObj:ProbeObject)
}
class ProbCheckCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descLabel: UILabel?
    @IBOutlet weak var nnaImageView: UIImageView?

    @IBOutlet weak var radioButton: UIButton?
    @IBOutlet weak var crossButton: UIButton?
    var probObject:ProbeObject!
    var indexRow:Int!

    var delegate : AfterUseTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        nnaImageView?.isHidden = true
        setDesignFont();
        // Initialization code
    }
    
    func bindData(probObj:ProbeObject,index:Int){
        self.probObject = probObj;
        indexRow = index;
        titleLabel?.text = probObj.title;
        descLabel?.text = probObj.des;
        if probObj.isSelected == true {
        radioButton?.setImage(#imageLiteral(resourceName: "shapeg"), for: UIControlState.normal)
        } else {
         radioButton?.setImage(#imageLiteral(resourceName: "androidRadioButtonOn"), for: UIControlState.normal)
        }
    }
    
    func setDesignFont(){
        titleLabel?.font = UIFont(name: BOLD_APPFONT, size: Constant.NormalfontSize)
        descLabel?.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        
    }

    @IBAction func radioButtonClicked(_ sender: UIButton) {
        if self.probObject.isSelected == true {
            self.probObject.isSelected = false;
        } else {
            self.probObject.isSelected = true;
        }
        self.delegate?.radioButtonClickedAfterUseTableViewCell(index: indexRow ,updateObj: self.probObject)
    }
    
    @IBAction func crossButtonClicked(_ sender: UIButton) {
        
        if(crossButton?.isSelected ?? false){
            crossButton?.isSelected = false
            nnaImageView?.isHidden = true
            radioButton?.isHidden = false
            self.alpha = 1
            radioButton?.isUserInteractionEnabled = true
        }else
        {
            crossButton?.isSelected = true
            nnaImageView?.isHidden = false
            self.alpha = 0.5
            radioButton?.isHidden = true

            radioButton?.isUserInteractionEnabled = false
        }
        self.delegate?.crossButtonclicked(index: sender.tag)

        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
