//
//  MenuViewController.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 10/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit
import KLCommon



class MenuViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,LinkLabelGastureDelegate {
    
    @IBOutlet weak var txView: UITextView!
    
    @IBOutlet weak var imgLeft: UIImageView!
    
    let contentOffSet:CGFloat = 300;
    let originContent:CGFloat = 0;
    @IBOutlet weak var lblPrivacy: LinkAttributatedLabel!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var lblWelcomeUser: UILabel!
    var controller:UIViewController!
    var sectionArray = [String]()
    var menuArray = [String]()
    var sectionImageArray = [String]()
    var isAddcheckOpen:Bool!
    var isRightImageHidden:Bool = false
    //var drawerMenuItems : [DrawerMenuResponse.Menu] = []
    var drawerMenuDataset : [MenuViewItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        attributedString();
        
        isAddcheckOpen = false;
        lblWelcomeUser.font=UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        txView.font=UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        lblPrivacy.font=UIFont(name: REGULAR_APPFONT, size: Constant.SmallfontSize)
        btnLogout.titleLabel?.font=UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        self.view.backgroundColor = UIColor.getGreenColor();
        NotificationCenter.default.addObserver(self, selector: #selector(self.landscape(notif:)), name: NSNotification.Name(rawValue: LANDSCAPE_NOTIFY), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.potrait(notif:)), name: NSNotification.Name(rawValue: POTRAIT_NOTIFY), object: nil)
    sectionArray = ["Opening Check","Cleaning Check","Delivery Check","Closing Check","Add Check","View Log"]
    menuArray = ["Food","Appliances","Pest Control","Injury Log","Probe Check"]
    sectionImageArray = ["icons8FullMoon100",
                           "icons8FullMoon100-1",
                           "icons8FullMoon100-2",
                           "icons8FullMoon100-3",
                           "icons8FullMoon100-4","icons8FullMoon100-4","icons8FullMoon100-4","icons8FullMoon100-4","icons8FullMoon100-4","icons8FullMoon100-4","icons8FullMoon100-4","icons8FullMoon100-4","icons8FullMoon100-4","icons8FullMoon100-4","icons8FullMoon100-4","icons8FullMoon100-4","icons8FullMoon100-4"]
    self.tblMenu.register(UINib(nibName: "MenuCustomCell", bundle: nil), forCellReuseIdentifier: "MenuCustomCell")
    NotificationCenter.default.addObserver(self, selector: #selector(self.rotatedMenu), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    self.tblMenu.backgroundColor = UIColor.getGreenColor();
        if  self.getAppDelegate().drawerMenuItems.count == 0 || self.getAppDelegate().drawerMenuItems == nil {
         fetchDrawerItems();
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
      func rotatedMenu() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            print("Landscape")
            isRightImageHidden = false;
            tblMenu.reloadData()
 
            
        }
        
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            print("Portrait")
            isRightImageHidden = true;
            tblMenu.reloadData()
        }
    }
    

    
    func landscape(notif: NSNotification){
        isRightImageHidden = false
    }
    func potrait(notif: NSNotification){
        isRightImageHidden = true;
    }
    
    
  // UITable View numberOfSections Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
            return getAppDelegate().drawerMenuItems.count;
    }
    
    // UITable View viewForHeaderInSection Delegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
            let headerSubView = UIView()
            headerView.frame = CGRect(x: self.view.frame.origin.x , y: self.view.frame.origin.y, width: gettingOrientationWidthHeight().width - Constant.menuHeaderViewTrailing, height: Constant.menuHeaderHeight)
            headerSubView.frame = CGRect(x: self.view.frame.origin.x , y: self.view.frame.origin.y, width: headerView.frame.size.width, height: Constant.menuHeaderHeight)
            headerSubView.backgroundColor = UIColor.getGreenColor();
            headerView.backgroundColor = UIColor.getGreenColor();
            // Create UILabel
            let addLabel:UILabel = UILabel()
            addLabel.frame = CGRect(x: Constant.menuLabelX, y: Constant.menuLabelY, width: Constant.menuLabelWidth, height: Constant.menuLabelHeight)
            addLabel.textAlignment = .left
            let menuObj = getAppDelegate().drawerMenuItems[section];
           addLabel.text = menuObj.label;
            addLabel.textColor=UIColor.white;
            addLabel.font=UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
            addLabel.backgroundColor=UIColor.clear
            headerSubView.addSubview(addLabel)
            // Create UIbutton
            let button:UIButton = UIButton(frame: headerView.frame)
            button.backgroundColor = UIColor.clear;
            button.setTitle("", for: .normal)
            button.addTarget(self, action:#selector(self.buttonClicked(sender:)), for: .touchUpInside)
            button.tag = section;
            headerSubView.addSubview(button)
            headerView.addSubview(headerSubView);
            // Create Left Icons
            let image = UIImage(named:sectionImageArray[section])
            let imageView = UIImageView(frame: CGRect(x: Constant.menuImageX,y: Constant.menuImageY, width: Constant.menuImageWidth , height: Constant.menuImageHeight))
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            headerView.addSubview(imageView)
        // Create Right Icons
           let imageRight = UIImage(named:sectionImageArray[section])
           let imageViewRight = UIImageView(frame: CGRect(x:headerView.frame.size.width - Constant.menuRightImageTrailing,y: Constant.menuImageY, width: Constant.menuImageWidth*1.5 , height: Constant.menuImageHeight*1.5))
        imageViewRight.image = imageRight
        imageViewRight.contentMode = .scaleAspectFit
        headerView.addSubview(imageViewRight)
        
        
        if !isRightImageHidden {
            imageViewRight.isHidden = false;
       
        } else {
           imageViewRight.isHidden = true;
        }
        return headerView
    }
    
    
    
    
    
    // UITable View numberOfRowsInSection Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let menuObj = getAppDelegate().drawerMenuItems[section];
        if menuObj.isCollapsed == true {
            return (menuObj.subMenu?.count)!;
        } else {
            return 0;
        }
//        if section == 4 {
//        return (isAddcheckOpen == true) ? 5 : 0;
//        } else {
//            return 0;
//        }
    }
    
    // UITable View cellForRowAt Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:MenuCustomCell?
        let cellIdentifier = "MenuCustomCell"
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MenuCustomCell
        if cell == nil {
            cell = MenuCustomCell(style: UITableViewCellStyle.value1, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none;
        let menuObj = getAppDelegate().drawerMenuItems[indexPath.section];
        let subMenuObj = menuObj.subMenu?[indexPath.row];
        cell?.lblTitle.text = subMenuObj?.label;
        return cell!
 
    }
    
    // UITable View didSelectRowAt Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 0 {
//         navigateWithMenu(toViewController: "FoodController",valAnyObj:"" as AnyObject);
//        } else if indexPath.row == 1 {
//        navigateWithMenu(toViewController: "ApplianceScreenController",valAnyObj:"" as AnyObject);
//        }else if indexPath.row == 2 {
//        navigateWithMenu(toViewController: "PestInjuryView",valAnyObj:"Sign of Pest" as AnyObject);
//            
//        }else if indexPath.row == 3 {
//        navigateWithMenu(toViewController: "PestInjuryView",valAnyObj:"INJURY" as AnyObject);
//        }else if indexPath.row == 4 {
//        navigateWithMenu(toViewController: "ProbCheckView",valAnyObj:"" as AnyObject);
//        }
    }
    

    
    // Section Header Button Clicked for Navigate to specific Screen
    func buttonClicked(sender:UIButton){
        let menuObj = getAppDelegate().drawerMenuItems[sender.tag];
        if menuObj.isCollapsed == false {
        isAddcheckOpen = false;
        } else {
            if getAppDelegate().isSmallSliderShow == true {
                return;
            }
        }
        tblMenu.reloadData();
        if menuObj.label == "Opening Check" {
            resetScreenIdentifier()
            Constant.isOpeningScreen = true;
            navigateWithMenu(toViewController: "HomeCntrl",valAnyObj:"" as AnyObject);
        } else if menuObj.label == "Cleaning Check" {
            resetScreenIdentifier()
            Constant.isCleaningScreen = true;
            navigateWithMenu(toViewController: "HomeCntrl",valAnyObj:"" as AnyObject);
        }
        else if menuObj.label == "Delivery Check" {
        navigateWithMenu(toViewController: "DeliveryViewController",valAnyObj:"" as AnyObject);
        }
        else if menuObj.label == "Closing Check" {
            resetScreenIdentifier()
            Constant.isClosingScreen = true;
            navigateWithMenu(toViewController: "HomeCntrl",valAnyObj:"" as AnyObject);
        }
        else if menuObj.label == "View Log" {
         navigateWithMenu(toViewController: "ViewLogController",valAnyObj:"" as AnyObject);
        }
        else if menuObj.label == "Food" {
         navigateWithMenu(toViewController: "FoodController",valAnyObj:"" as AnyObject);
        }
        else if menuObj.label == "Appliance" {
        navigateWithMenu(toViewController: "ApplianceScreenController",valAnyObj:"" as AnyObject);
        }
        else if menuObj.label == "Pest Control" {
        navigateWithMenu(toViewController: "PestInjuryView",valAnyObj:"Sign of Pest" as AnyObject);
        }
        else if menuObj.label == "Injury Log" {
        navigateWithMenu(toViewController: "PestInjuryView",valAnyObj:"INJURY" as AnyObject);
        }
        else if menuObj.label == "Probe Checks" {
        navigateWithMenu(toViewController: "ProbCheckView",valAnyObj:"" as AnyObject);
        }
        else if menuObj.label == "Tasks" {
         navigateWithMenu(toViewController: "ViewLogController",valAnyObj:"" as AnyObject);
        }
        
    }
    
    func resetScreenIdentifier() {
        Constant.isOpeningScreen = false;
        Constant.isCleaningScreen = false;
        Constant.isClosingScreen = false;
    }


    // Make URL Link on Label with Attribute
    func attributedString(){
        // Terms and Condition Label Code
        let underlineAttributedString = NSMutableAttributedString(string: lblPrivacy.text!);
        underlineAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range:NSRange(location:0,length:14))
        underlineAttributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range:NSRange(location:0,length:14))
        underlineAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range:NSRange(location:17,length:20))
        underlineAttributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range:NSRange(location:17,length:20))
        lblPrivacy.attributedText = underlineAttributedString;
        lblPrivacy.delegate = self
    }
    
    @IBAction func clickedToLogout(_ sender: Any) {
        self.alert("Logout", alertMessage: "Are you sure?", delegate: self, firstButton: "YES", secondButton: "NO", isSingleOK: false) { (title) in
            if title == "YES" {
                self.naviagteTOLoginView();
            }
        }
    }
    
    // After Logout navigate to LoginView
    func naviagteTOLoginView(){
        UserUtil.performLogout()
        Helper.removeUserDefault(USER_TOKEN);
        let loginVC : LoginVC? = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        UIApplication.topViewController()?.navigationController?.pushViewController(loginVC!, animated: true);
    }
    
    // Attributed Label Delegate
    func attributedLabelCallBack(strVal:String){
        if strVal == "TERMS & CONDITIONS" {
            Helper.OpenSafariWithURl(TERM_CONDITIONS);
            
        }
        else if strVal == "PRIVACY POLICY" {
            Helper.OpenSafariWithURl(PRIVACY_POLICY);
            
        }
    }
    
    
    private func fetchDrawerItems() {
        let _ = HomeService.getDrawerMenu { (response, error) in
            if(error == nil) {
                let resp : DrawerMenuResponse = response as! DrawerMenuResponse
                if ((resp.code == nil && resp.data != nil) || resp.code == 200) {
                     self.getAppDelegate().drawerMenuItems = []
                     self.getAppDelegate().drawerMenuItems.append(contentsOf: resp.data!)
                    
                    self.tblMenu.reloadData();
                } else {
                    self.alert("", alertMessage: resp.message, delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true) { (title) in}
                }
            } else {
                self.alert("", alertMessage: (error?.1)!, delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true) { (title) in}
            }
        }
    }


    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    class MenuViewItem {
        public var name : String = ""
        public var type : String = "menu"
        public var menu : DrawerMenuResponse.Menu?
        public var subMenu : DrawerMenuResponse.Menu.SubMenu?
        public var subMenus : [DrawerMenuResponse.Menu.SubMenu]?
        public var isCollapsed : Bool = false
        public var icon : String?
        
        init(name : String, type : String, menu :DrawerMenuResponse.Menu?, subMenu : DrawerMenuResponse.Menu.SubMenu?, icon : String?, isCollapsed : Bool, submenus : [DrawerMenuResponse.Menu.SubMenu]?) {
            self.icon = icon
            self.name = name
            self.type = type
            self.menu = menu
            self.subMenu = subMenu
            self.isCollapsed = isCollapsed
            self.subMenus = submenus
        }
    }

}
