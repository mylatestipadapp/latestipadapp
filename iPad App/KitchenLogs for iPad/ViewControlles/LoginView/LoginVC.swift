
//
//  DeliveryViewController.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 12/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import Foundation
import UIKit
import KLCommon


class LoginVC: BaseViewController {
    
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    
    
    override func viewDidLoad() {
        setDesignFont();
    }
    
    func setDesignFont(){
        txtEmail.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        txtPassword.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        btnLogin.titleLabel?.font = UIFont(name: BOLD_APPFONT, size: Constant.NormalfontSize)!
        btnLogin.layer.cornerRadius = 40;
    }
 
    @IBAction func clickedToLogin(_ sender: Any) {
        self.txtEmail?.resignFirstResponder()
        self.txtPassword?.resignFirstResponder()
        let emailId : String = self.txtEmail?.text ?? ""
        let password : String = self.txtPassword?.text ?? ""
        if  (String.isNilOrEmpty(emailId)){
            alert("", alertMessage: "Please enter your email id", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                
            })
            
            return
        }
        if String.isNilOrEmpty(password) {
            alert("", alertMessage: "Please enter your password", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                
            })
            return
        }
        createJWTToken()
      
        
    }
    
    func createJWTToken(){
        LoadingOverlay.shared.showOverlay(view:self.view, msg: "")
        let postObj : LoginPost = LoginPost.init(email: self.txtEmail?.text ?? "", password: (self.txtPassword?.text)!, token: "iOS")
        Helper.saveStringInDefault(USER_TOKEN, value: "");
        let _ = AuthService.loginUser(post: postObj) { (response, error) in
            if (error == nil) {
                let resp : LoginResponse = response as! LoginResponse
                print("Response is:\(resp.code)")
                
                if resp.code == 200 || resp.message == "Success" {
                    UserDAO().saveUser(user: (resp.data?[0].user)!)
                    Helper.saveStringInDefault(USER_TOKEN, value: (resp.data?[0].token)!);
                    LoadingOverlay.shared.hideOverlayView()
                    self.navigateWithMenu(toViewController: "HomeCntrl",valAnyObj:"" as AnyObject);
                } else if resp.code == 400 {
                    let alert = UIAlertController(title: "Incorrect password", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Forgot Password", style: UIAlertActionStyle.default, handler: self.showForgotPasswordAlert))
                    
                    self.present(alert, animated: true, completion: nil)
                    LoadingOverlay.shared.hideOverlayView()
                } else {
                    LoadingOverlay.shared.hideOverlayView()
                    self.alert("", alertMessage: resp.message ?? "", delegate: self, firstButton: "", secondButton: "", isSingleOK: true, completion: { (title) in
                        
                    })
                }
            } else {
                self.alert("", alertMessage:"Failed to communicate to server", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                    
                })
                LoadingOverlay.shared.hideOverlayView()
            }
        }
    }
    
    func showForgotPasswordAlert(action: UIAlertAction) {
        let _ = AuthService.forgotPassword(post: ForgotPasswordPost.init(email: self.txtEmail?.text ?? "")) { (response, error) in
            if (error == nil) {
                self.alert("Mail Sent", alertMessage:"If this email exists in our system, we will send you new password via email.", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                    
                })
                
            } else {
                self.alert("", alertMessage:"omething went wrong", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                    
                })
            }
        }
    }

    
}
