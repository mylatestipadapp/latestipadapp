//
//  OpeningCntrl.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 9/9/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import Foundation
         
import Gloss
extension Array {

}

class HomeCntrl: BaseViewController,CustomDatePickerDelegate,GridViewDelegate,SJSegmentedViewControllerDelegate,MarkAllViewDelegate,ApplianceControllerDelegate {
 

    var tempMessegaTitle = "Temperature out of range"
    var fridgeTemp = 0

    @IBOutlet weak var xConstraint: NSLayoutConstraint!
    @IBOutlet var vwTop:UIView?
    @IBOutlet var vwContent:UIView?
    @IBOutlet weak var vwDatePicker: UIView!
    var customPicker:CustomDatePicker!
    var dtVal:UILabel?
    var segment:SJSegmentedViewController?
    @IBOutlet var btnSave:UIButton?
    
    var hotHoldingNameArray = NSMutableArray()
    var cookedNameArray = NSMutableArray()
    var reheatingNameArray = NSMutableArray()
    var foodArray = [JSON]()
    var foodArray2 = [JSON]()
    var foodArray3 = [JSON]()
    var completeData:AllCheckMetadata?
    var locIDOpening = ""
    var bussIdOpening = ""
    
    var locIDClosing = ""
    var bussIdClosing = ""
    
    var locIDCleaning = ""
    var bussIdCleaning = ""
    
    var cleaningId = ""
    var entryDefaultsId = ""
    
    var frigdeArray = [JSON]()
    var freezerArray = [JSON]()
    var isProbeWorking : Bool?
    
    var tab1Array = NSMutableArray()
    var tab2Array = NSMutableArray()
    var tab3Array = NSMutableArray()
    
    var isMarAllTab1 = false
    var isMarAllTab2 = false
    var isMarAllTab3 = false
    
    var selectedDateMid = ""
    var yearDt:String!
    var reloaded = false;
    var checklist:MarkAllView?
    var appliance:ApplianceController?
    var currentGridIndex = 0
    var markAllEnabled : Bool?
    
    var endOfDay:MarkAllView?
    var deepClean:MarkAllView?


    

    override func viewDidLoad() {
        super.viewDidLoad();
        setTopView(superView: self.vwTop);
        createSegmentView();
        rotated();
        getDefaultAllApi()
        if Constant.isOpeningScreen || Constant.isClosingScreen {
           btnSave?.setTitle("NEXT", for: .normal)
        }
        
    }
  
    func getDefaultAllApi() {
        LoadingOverlay.shared.showOverlay(view:self.view, msg: "")
        ChecksService.getDefaultChecksTempletes{ (response, error) in
            if (error == nil) {
                let resp : AllChecksResponse = response as! AllChecksResponse
                if resp.code == 200 || resp.message == "Success" {
                print(resp.data?.toJSON())
              


                self.hotHoldingNameArray.removeAllObjects()
                self.cookedNameArray.removeAllObjects()
                self.reheatingNameArray.removeAllObjects()
                self.completeData = resp.data;
                    self.getAppDelegate().defaultData = self.completeData;
                    self.checklist?.dictData = self.completeData;
                    self.endOfDay?.dictData = self.completeData;
                    self.deepClean?.dictData = self.completeData;


                    self.appliance?.dictData = self.completeData;
                    if !(self.completeData?.entryDefaults?.isEmpty)! {
                    self.fridgeTemp = self.completeData?.entryDefaults?[0].fridgeTemp ?? 0;
                    }
                    if Constant.isOpeningScreen {
                    for obj in (self.completeData?.openingCheck?[0].items)! {
                        self.checklist?.tab1Array.add("NO");
                        self.checklist?.tab1CrossArray.add(false)
                    }
                }
                    if Constant.isCleaningScreen {
                        for obj in (self.completeData?.cleaningCheck?[0].categories?[0].items)! {
                            self.checklist?.tab1Array.add("NO");
                            self.checklist?.tab1CrossArray.add(false)
                            self.endOfDay?.tab1Array.add("NO");
                            self.endOfDay?.tab1CrossArray.add(false)
                            self.deepClean?.tab1Array.add("NO");
                            self.deepClean?.tab1CrossArray.add(false)

                        }
                    }
                    if Constant.isClosingScreen {
                        for obj in (self.completeData?.closingCheck?[0].items)! {
                            self.checklist?.tab1Array.add("NO");
                            self.checklist?.tab1CrossArray.add(false)
                            
                        }
                    }
                    self.checklist?.tableView.reloadData()
                    self.endOfDay?.tableView.reloadData()
                    self.deepClean?.tableView.reloadData()

                    self.appliance?.tableView.reloadData()

                    self.locIDOpening = self.completeData?.openingCheck?[0].locationId ?? "null"
                    self.bussIdOpening = self.completeData?.openingCheck?[0].businessId ?? "null"
                    self.locIDClosing = self.completeData?.closingCheck?[0].locationId ?? "null"
                    self.bussIdClosing = self.completeData?.closingCheck?[0].businessId ?? "null"
                    self.locIDCleaning = self.completeData?.cleaningCheck?[0].locationId ?? "null"
                    self.bussIdCleaning = self.completeData?.cleaningCheck?[0].businessId ?? "null"
                    self.cleaningId = self.completeData?.cleaningCheck?[0].id ?? ""

                    
                    Helper.saveStringInDefault(LocationIdOpening, value: self.locIDOpening)
                    Helper.saveStringInDefault(BuisnessIdOpening, value: self.bussIdOpening)
                    Helper.saveStringInDefault(LocationIdClosing, value: self.locIDClosing)
                    Helper.saveStringInDefault(BuisnessIdClosing, value: self.bussIdClosing)
                    Helper.saveStringInDefault(LocationIdCleaning, value: self.locIDCleaning)
                    Helper.saveStringInDefault(BuisnessIdCleaning, value: self.bussIdCleaning)
                    if resp.data?.entryDefaults?.isEmpty == false {

                let foods = resp.data?.entryDefaults?[0].foods
            
                for i in 0..<(foods?.count ?? 0)! {
                    if(foods?[i].hotHolding ?? false){
                        self.hotHoldingNameArray.add(foods?[i].name ?? "")
                    }
                    if(foods?[i].cooked ?? false){
                        self.cookedNameArray.add(foods?[i].name ?? "")
                    }
                    if(foods?[i].reheating ?? false){
                        self.reheatingNameArray.add(foods?[i].name ?? "")
                    }
                        }
                
                self.setfoodArrayData1(dataArray: self.cookedNameArray.count , isSelected: "NO")
                self.setfoodArrayData2(dataArray: self.hotHoldingNameArray.count , isSelected: "NO")
                self.setfoodArrayData3(dataArray: self.reheatingNameArray.count , isSelected: "NO")
             //   self.setRadioButtonsTab1(dataArray: self.completeData?["opening"][0]["items"].array?.count ?? 0 , isSelected: "NO")
                // appliances
                        if self.completeData?.entryDefaults?.isEmpty == false {

                self.setfridgeArrayData(dataArray: self.completeData?.entryDefaults?[0].numberOfFridges ?? 0 , isSelected: "NO")
                self.setfreezerArrayData(dataArray: self.completeData?.entryDefaults?[0].numberOfFreezers ?? 0 , isSelected: "NO")
                }
                    }
                    LoadingOverlay.shared.hideOverlayView()

            }
            }
            LoadingOverlay.shared.hideOverlayView()

    }

}
    func setfridgeArrayData(dataArray:Int , isSelected:String){
        frigdeArray.removeAll()
        for _ in 0 ..< dataArray {
            frigdeArray.append(["temp":"", "comment":""])
        }
    }
    func setfreezerArrayData(dataArray:Int , isSelected:String){
        freezerArray.removeAll()
        for _ in 0 ..< dataArray {
            freezerArray.append(["temp":"", "comment":""])
        }
    }
        func setfoodArrayData1(dataArray:Int , isSelected:String){
        foodArray.removeAll()
        for _ in 0 ..< dataArray {
            foodArray.append(["temp":"", "comment":""])
        }
    }
    
    func setfoodArrayData2(dataArray:Int , isSelected:String){
        foodArray2.removeAll()
        for _ in 0 ..< dataArray {
            foodArray2.append(["temp":"", "comment":""])
        }
    }
    func setfoodArrayData3(dataArray:Int , isSelected:String){
        foodArray3.removeAll()
        for _ in 0 ..< dataArray {
            foodArray3.append(["temp":"", "comment":""])
        }
    }
    override func rotated() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            xConstraint.constant = Constant.horizontalLeading;
        }
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            xConstraint.constant = Constant.verticalLeading;
            
        }
    }
    
    
    func postCleaningCheck() {
        if self.yearDt == nil {
            self.yearDt = getCurrentDate();
            return;
        }
        var array1 = [CheckItems]()
        let array1Count = self.completeData?.cleaningCheck?[0].categories?[0].items?.count ?? 0;
        for i in 0..<array1Count {
            let dict = CheckItems.init(id: (self.completeData?.cleaningCheck?[0].categories?[0].items?[i].id)!, checkDescription:(self.completeData?.cleaningCheck?[0].categories?[0].items?[i].checkDescription)! , task: self.completeData?.cleaningCheck?[0].categories?[0].items?[i].task ?? "",state:"false")
            array1.append(dict)
            
        }
        var array2 = [CheckItems]()
        let array2Count = self.completeData?.cleaningCheck?[0].categories?[1].items?.count ?? 0;
        for i in 0..<array2Count {
            let dict = CheckItems.init(id: (self.completeData?.cleaningCheck?[0].categories?[1].items?[i].id)!, checkDescription:(self.completeData?.cleaningCheck?[0].categories?[1].items?[i].checkDescription)! , task: self.completeData?.cleaningCheck?[0].categories?[1].items?[i].task ?? "",state:"false")
            array2.append(dict);
            
        }
        var array3 = [CheckItems]()
        let array3Count = self.completeData?.cleaningCheck?[0].categories?[2].items?.count ?? 0;
        for i in 0..<array3Count {
            let dict = CheckItems.init(id: (self.completeData?.cleaningCheck?[0].categories?[2].items?[i].id)!, checkDescription:(self.completeData?.cleaningCheck?[0].categories?[2].items?[i].checkDescription)! , task: self.completeData?.cleaningCheck?[0].categories?[2].items?[i].task ?? "",state:"false")
            array3.append(dict);
            
        }
        let afteruseID = self.completeData?.cleaningCheck?[0].categories?[0].id ?? ""
        let endDay = self.completeData?.cleaningCheck?[0].categories?[1].id ?? ""
        let deepClean = self.completeData?.cleaningCheck?[0].categories?[2].id ?? ""
        
        let templateName = self.completeData?.cleaningCheck?[0].name ?? ""
        let template = Template.init(id: cleaningId, name: templateName);
        let checkCatgoryAfterUse = CheckCategory.init(id: afteruseID, name: "After Use", items: array1, checkAllEnabled: false,camelCaseName:"After Use")
        let checkCatgoryendDay = CheckCategory.init(id: endDay, name: "End Day", items: array2, checkAllEnabled: false,camelCaseName:"End Day")
        let checkCatgorydeepClean = CheckCategory.init(id: deepClean, name: "Deep Clean", items: array3, checkAllEnabled:false,camelCaseName:"Deep Clean")
        
        let hrs = self.getTime(strDate: (dtVal?.text!) ?? "");
        let mins = self.getMins(strDate: dtVal?.text ?? "");
        
        let param = CleaningPost.init(templates: template, categorie: [checkCatgoryAfterUse,checkCatgoryendDay,checkCatgorydeepClean], entryTm: self.yearDt, entryMin: mins, locationIds: self.locIDCleaning, entryHr: hrs)
        
       let post = ChecksService.postCleaning(post: param, callback:{ (response, error) in
        if error == nil {
            print("post cleaning",response);
            let alert = UIAlertController(title: "Saved", message: "", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            super.present(alert, animated: true, completion: nil)
        }
       })
        
    }
    
    func postOpeningCheck() {
        if self.yearDt == nil {
            self.yearDt = getCurrentDate();
            return;
        }
        var fridgeArr = [FridgePost]()
        
        let assetsArrayarr1 = self.completeData?.entryDefaults?[0].numberOfFridges ?? 0
        for i in 0..<assetsArrayarr1 {
            let temp = self.frigdeArray[i]["temp"]
            let comment = self.frigdeArray[i]["comment"]
            let commentrequired = ((self.frigdeArray[i]["comment"] as! String).characters.count ?? 0) > 0 ? true : false
            let _fridge = FridgePost.init(fridgeNum: i+1, temperature: temp as! String, comment: comment as! String, commentRequired: commentrequired)
            fridgeArr.append(_fridge)
        }
        var freezerArr = [FreezerPost]()
        let assetsArrayarr2 = self.completeData?.entryDefaults?[0].numberOfFreezers ?? 0

        for i in 0..<assetsArrayarr2 {
            let temp = self.freezerArray[i]["temp"]
            let comment = self.freezerArray[i]["comment"]
            let commentrequired = ((self.freezerArray[i]["comment"] as! String).characters.count ?? 0) > 0 ? true : false
            let _freezer = FreezerPost.init(freezerNum: i+1, temperature: temp as! String, comment: comment as! String, commentRequired: commentrequired)
            freezerArr.append(_freezer)
        }
        var array1 = [CheckItems]()
        let arr1Count = self.completeData?.openingCheck?[0].categories?[0].items?.count ?? 0
        for i in 0..<arr1Count {
            let openingData = self.completeData?.openingCheck?[0].categories?[0].items?[i].checkDescription ?? ""
            let checkItem = CheckItems.init(id: "", checkDescription: openingData, task: "", state: self.tab1Array[i] as! String)
            array1.append(checkItem)
        }
        var cookedArray = [CookedType]()
        var reHeatingArray = [CookedType]()
        var hotHoldingArray = [CookedType]()
        
        for i in 0..<self.cookedNameArray.count {
            let name = "\(self.self.cookedNameArray[i])"
            if foodArray.count > 0 {
                let temp = self.self.foodArray[i]["temp"]
                let comment = self.self.foodArray[i]["comment"]
                var cookedItem = CookedType.init(name: name, temperature: temp as! String ?? "", comment: comment as! String ?? "", commentRequired: false, type: "cooked")
                cookedArray.append(cookedItem);
            }
        }
        for i in 0..<self.hotHoldingNameArray.count {
            let name = "\(self.self.hotHoldingNameArray[i])"
            let temp = self.foodArray2[i]["temp"]
            let comment = self.foodArray2[i]["comment"]
            var cookedItem = CookedType.init(name: name, temperature: temp as! String ?? "", comment: comment as! String ?? "", commentRequired: false, type: "hotHolding")
            hotHoldingArray.append(cookedItem);
        }
        
        for i in 0..<self.reheatingNameArray.count {
            let name = "\(self.reheatingNameArray[i])"
            let temp = self.foodArray3[i]["temp"]
            let comment = self.foodArray3[i]["comment"]
            var cookedItem = CookedType.init(name: name, temperature: temp as! String ?? "", comment: comment as! String ?? "", commentRequired: false, type: "hotHolding")
            reHeatingArray.append(cookedItem);
        }
        let openingID = self.completeData?.openingCheck?[0].id ?? ""
        let templateName = self.completeData?.openingCheck?[0].name ?? ""
        let type = self.completeData?.openingCheck?[0].type ?? ""
        
        let templateNm = self.completeData?.closingCheck?[0].name ?? ""
        let template = Template.init(id: "", name: templateNm);
        let hrs = self.getTime(strDate: (dtVal?.text!) ?? "");
        let mins = self.getMins(strDate: dtVal?.text ?? "");
        
        let param = OpeningPost.init(templates: template, cooking:cookedArray.count > 0 ? cookedArray : [], entryTm: self.yearDt, entryMin: mins, locationIds: self.locIDOpening, entryHr: hrs, hotHolding: hotHoldingArray.count > 0 ? hotHoldingArray : [], reHeating: reHeatingArray.count > 0 ? reHeatingArray : [], fridges: fridgeArr.count > 0 ? fridgeArr : [], freezers: freezerArr.count > 0 ? freezerArr : [], items: array1, type: type, id: openingID)
        let post = ChecksService.postOpening(post: param, callback:{ (response, error) in
            if error == nil {
                print("post Opening",response);
                let alert = UIAlertController(title: "Saved", message: "", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                super.present(alert, animated: true, completion: nil)
            }
        })

    }

    func postClosingCheck() {
        if self.yearDt == nil {
            self.yearDt = getCurrentDate();
            return;
        }
        var fridgeArr = [FridgePost]()
        
        let assetsArrayarr1 = self.completeData?.entryDefaults?[0].numberOfFridges ?? 0
        for i in 0..<assetsArrayarr1 {
            let temp = self.frigdeArray[i]["temp"]
            let comment = self.frigdeArray[i]["comment"]
            let commentrequired = ((self.frigdeArray[i]["comment"] as! String).characters.count ?? 0) > 0 ? true : false
            let _fridge = FridgePost.init(fridgeNum: i+1, temperature: temp as! String, comment: comment as! String, commentRequired: commentrequired)
            fridgeArr.append(_fridge)
        }
        var freezerArr = [FreezerPost]()
        let assetsArrayarr2 = self.completeData?.entryDefaults?[0].numberOfFreezers ?? 0
        
        for i in 0..<assetsArrayarr2 {
            let temp = self.freezerArray[i]["temp"]
            let comment = self.freezerArray[i]["comment"]
            let commentrequired = ((self.freezerArray[i]["comment"] as! String).characters.count ?? 0) > 0 ? true : false
            let _freezer = FreezerPost.init(freezerNum: i+1, temperature: temp as! String, comment: comment as! String, commentRequired: commentrequired)
            freezerArr.append(_freezer)
        }
        var array1 = [CheckItems]()
        let arr1Count = self.completeData?.closingCheck?[0].categories?[0].items?.count ?? 0
        for i in 0..<arr1Count {
            let openingData = self.completeData?.closingCheck?[0].categories?[0].items?[i].checkDescription ?? ""
            let checkItem = CheckItems.init(id: "", checkDescription: openingData, task: "", state: self.tab1Array[i] as! String)
            array1.append(checkItem)
        }
        var cookedArray = [CookedType]()
        var reHeatingArray = [CookedType]()
        var hotHoldingArray = [CookedType]()
        
        for i in 0..<self.cookedNameArray.count {
            let name = "\(self.self.cookedNameArray[i])"
            if foodArray.count > 0 {
                let temp = self.self.foodArray[i]["temp"]
                let comment = self.self.foodArray[i]["comment"]
                var cookedItem = CookedType.init(name: name, temperature: temp as? String ?? "", comment: comment as! String ?? "", commentRequired: false, type: "cooked")
                cookedArray.append(cookedItem);
            }
        }
        for i in 0..<self.hotHoldingNameArray.count {
            let name = "\(self.self.hotHoldingNameArray[i])"
            let temp = self.foodArray2[i]["temp"]
            let comment = self.foodArray2[i]["comment"]
            var cookedItem = CookedType.init(name: name, temperature: temp as! String ?? "", comment: comment as? String ?? "", commentRequired: false, type: "hotHolding")
            hotHoldingArray.append(cookedItem);
        }
        
        for i in 0..<self.reheatingNameArray.count {
            let name = "\(self.reheatingNameArray[i])"
            let temp = self.foodArray3[i]["temp"]
            let comment = self.foodArray3[i]["comment"]
            var cookedItem = CookedType.init(name: name, temperature: temp as! String ?? "", comment: comment as? String ?? "", commentRequired: false, type: "hotHolding")
            reHeatingArray.append(cookedItem);
        }
        let closingID = self.completeData?.closingCheck?[0].id ?? ""
        let templateName = self.completeData?.closingCheck?[0].name ?? ""
        let type = self.completeData?.closingCheck?[0].type ?? ""
        
        let templateNm = self.completeData?.closingCheck?[0].name ?? ""
        let template = Template.init(id: "", name: templateNm);
        let hrs = self.getTime(strDate: (dtVal?.text!) ?? "");
        let mins = self.getMins(strDate: dtVal?.text ?? "");
        
        let param = ClosingPost.init(templates: template, cooking:cookedArray.count > 0 ? cookedArray : [], entryTm: self.yearDt, entryMin: mins, locationIds: self.locIDClosing, entryHr: hrs, hotHolding: hotHoldingArray.count > 0 ? hotHoldingArray : [], reHeating: reHeatingArray.count > 0 ? reHeatingArray : [], fridges: fridgeArr.count > 0 ? fridgeArr : [], freezers: freezerArr.count > 0 ? freezerArr : [], items: array1, type: type, id: closingID)
        let post = ChecksService.postClosing(post: param, callback:{ (response, error) in
            if error == nil {
                print("post Closing",response);
                let alert = UIAlertController(title: "Saved", message: "", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                super.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    


    
  // Add Controllers into SegmentView
    func createSegmentView(){
        applianceVw.reset()
        foodVw.reset()
        checklist = MarkAllView(nibName: "MarkAllView", bundle: nil);
        checklist?.reloaded = reloaded
        //checklist?.dictData = self.getAppDelegate().defaultData;
        checklist?.navigationItem.titleView = UIView() ;
        checklist?.navigationItem.titleView?.tag = 1;
        checklist?.delegate = self
        
        appliance = ApplianceController(nibName: "ApplianceController", bundle: nil);
        appliance?.delegate = self;
        appliance?.navigationItem.titleView = UIView();
        appliance?.navigationItem.titleView?.tag = 2;
       // appliance?.dictData = self.getAppDelegate().defaultData;

        
        let header = TopHeaderView(nibName: "TopHeaderView", bundle: nil);
        header.delegate = self;
        if Constant.isCleaningScreen {
            endOfDay = MarkAllView(nibName: "MarkAllView", bundle: nil);
            endOfDay?.navigationItem.titleView = UIView() ;
            endOfDay?.delegate = self
            endOfDay?.navigationItem.titleView?.tag = 2;
            
            deepClean = MarkAllView(nibName: "MarkAllView", bundle: nil);
            deepClean?.delegate = self
            deepClean?.navigationItem.titleView = UIView() ;
            deepClean?.navigationItem.titleView?.tag = 3;
            segment = SJSegmentedViewController(headerViewController: header, segmentControllers: [checklist!,endOfDay!,deepClean!]);
        }
        else {
            segment = SJSegmentedViewController(headerViewController: header, segmentControllers: [checklist!,appliance!]);
        }
        segment?.segmentViewHeight = Constant.GridHeight;
        segment?.delegate = self
        
        segment?.headerViewHeight = Constant.topHeaderHeight;
        segment?.showsHorizontalScrollIndicator = false;
        segment?.showsVerticalScrollIndicator = false;
        btnSave?.backgroundColor = UIColor.getGreenColor()
        
        
        self.vwContent?.addSubview((segment?.view)!);
        
        checklistVw.delegate = self;
        applianceVw.delegate = self;
        foodVw.delegate = self;
        checklistVw.btnCheckListClick()
        
        Helper.setZeroSpaceConstraint(vw: (segment?.view)!)
    }
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
       
        
    }
    func didSelectSegmentAtIndex(_ index: Int) {
        if Constant.isOpeningScreen || Constant.isClosingScreen {
            if index == 0 {
                btnSave?.setTitle("NEXT", for: .normal)
            }
            else {
                btnSave?.setTitle("SAVE", for: .normal)
            }
        }
    }

    
    func showingCustomDatePicker(){
        vwDatePicker.isHidden = false;
        customPicker = CustomDatePicker(nibName: "CustomDatePicker", bundle: nil);
        customPicker.delegate = self;
        self.vwDatePicker.addSubview(customPicker.view);
        self.topviewConstraint(vwTop: customPicker.view);
        
    }
    
    func selectedDatePickerValue(value:String,yearDt:String) {
        dtVal?.text = value;
        self.yearDt = yearDt;
        
    }
    
    func clickedDone() {
        vwDatePicker.isHidden = true;

    }
     override func clickedToDateTime(sender: UILabel) {
        dtVal = sender;
        showingCustomDatePicker();
        
    }
 

    func btnCheckListClick() {
        applianceVw.reset()
        foodVw.reset()
        segment?.segmentedScrollView.contentView?.movePageToIndex(0, animated: true);
        currentGridIndex = 0
        didSelectSegmentAtIndex(0);
        

    }
    func btnApplianceClick() {
        checklistVw.reset()
        foodVw.reset()
        currentGridIndex = 1

        segment?.segmentedScrollView.contentView?.movePageToIndex(1, animated: true);
        didSelectSegmentAtIndex(1);

    }

    func btnFoodClick() {
        checklistVw.reset()
        applianceVw.reset()
        currentGridIndex = 2

        segment?.segmentedScrollView.contentView?.movePageToIndex(2, animated: true);
        didSelectSegmentAtIndex(2);

    }
    func resetHeaderViews() {
        checklistVw.reset()
        applianceVw.reset()
        foodVw.reset()
    }
    func MarkAllViewDelegateDelegateRadioClicked(index: Int, gridNo: NSInteger, isSelected: String) {
        
        switch currentGridIndex {
        case 0:
            tab1Array[index] = isSelected
        case 1:
            tab2Array[index] = isSelected
        case 2:
            tab3Array[index] = isSelected
        default:
            print("gadbad jhale")
        }
        
    }
    func setRadioButtonsTab1(dataArray:Int , isSelected:String){
        tab1Array.removeAllObjects()
        for _ in 0 ..< dataArray {
            tab1Array.add(isSelected)
        }
    }
    
    func setRadioButtonsTab2(dataArray:Int, isSelected:String){
        tab2Array.removeAllObjects()
        for _ in 0 ..< dataArray {
            tab2Array.add(isSelected)
        }
    }
    
    func setRadioButtonsTab3(dataArray:Int, isSelected:String){
        tab3Array.removeAllObjects()
        for _ in 0 ..< dataArray {
            tab3Array.add(isSelected)
        }
    }
    func markAllGridViewCellDelegateTapped()
    {
        if Constant.isCleaningScreen
        {
            switch currentGridIndex {
            case 0:
                
                isMarAllTab1 = true
                self.setRadioButtonsTab1(dataArray: self.completeData?.cleaningCheck?[0].categories?[0].items?.count ?? 0 , isSelected: "YES")
            case 1:
                isMarAllTab2 = true
                self.setRadioButtonsTab2(dataArray: self.completeData?.cleaningCheck?[0].categories?[1].items?.count ?? 0 ,isSelected: "YES")
            case 2:
                isMarAllTab3 = true
                self.setRadioButtonsTab3(dataArray: self.completeData?.cleaningCheck?[0].categories?[2].items?.count ?? 0 , isSelected: "YES")
            default:
                print("mark all ", index)
            }
        }else if Constant.isOpeningScreen
        {
            isMarAllTab1 = true
            self.setRadioButtonsTab1(dataArray: self.completeData?.openingCheck?[0].items?.count ?? 0 , isSelected: "YES")
            checklist?.isMarkAll = true
        }else if Constant.isClosingScreen
        {
            isMarAllTab1 = true
            self.setRadioButtonsTab1(dataArray: self.completeData?.closingCheck?[0].items?.count ?? 0 , isSelected: "YES")
        }
        markAllEnabled = true
        checklist?.tableView.reloadData();
    }
    func MarkAllViewDelegateRadioClicked(index:Int, gridNo:NSInteger , isSelected: String) {
        
    }
    func tableViewCell2FreezerDelegate(index: Int, temp: String, comment: String) {
        
        if(Int(temp) ?? 0 > -18 && comment == ""){
            
            let alert = UIAlertController(title: tempMessegaTitle, message: "Entered value should be less than or equal to " + "\(-18)" + "°" + ", else enter the comment", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            super.present(alert, animated: true, completion: nil)
            
        }
        
        freezerArray[index] = ["temp":temp, "comment":comment]
        
    }
    
    func tableViewCell2FridgeDelegate(index: Int, temp: String, comment: String) {
        
        if(Int(temp) ?? 0 > fridgeTemp && comment == ""){
            
            let alert = UIAlertController(title: tempMessegaTitle, message: "Entered value should be less than or equal to " + "\(fridgeTemp)" + "°" + ", else enter the comment", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            super.present(alert, animated: true, completion: nil)
            
        }
        
        frigdeArray[index] = ["temp":temp, "comment":comment]
        
    }
    @IBAction func btnSaveClick() {
        if btnSave?.title(for: .normal) == "NEXT" {
            if Constant.isOpeningScreen || Constant.isClosingScreen {
                btnApplianceClick()
            }
            else {
//                if currentGridIndex == 0 {
//                    btnApplianceClick()
//                }
//                if currentGridIndex == 1 {
//                    btnFoodClick()
//                }
                if Constant.isOpeningScreen {
                    postOpeningCheck()
                }
                if Constant.isClosingScreen {
                    postClosingCheck()
                }
                if Constant.isCleaningScreen {
                    postCleaningCheck()
                }
            }
        }

    }

}
