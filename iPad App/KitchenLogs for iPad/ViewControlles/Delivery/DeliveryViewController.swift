//
//  DeliveryViewController.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 12/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import UIKit
import DropDown
import KLCommon
class DeliveryViewController: BaseViewController,CustomDatePickerDelegate,UITextFieldDelegate {
    

    @IBOutlet weak var vwInjury: UIView!
    
    
    @IBOutlet weak var xConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSaveLog: UIButton!
    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var btnSlide: UISwitch!
    @IBOutlet weak var lblTemparture: UILabel!
    @IBOutlet weak var vwDatePicker: UIView!
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var txtAddSummary: UITextField!
    @IBOutlet weak var txtSetTemperature: UITextField!
    @IBOutlet weak var lblFahrenheit: UILabel!
    @IBOutlet weak var lblCelsius: UILabel!
    @IBOutlet weak var btnPackagingState: UIButton!
    @IBOutlet weak var btnDateTime: UIButton!
    @IBOutlet weak var btnTypeInjury: UIButton!
    @IBOutlet weak var txtInvoice: UITextField!
    @IBOutlet weak var vwDateTime: UIView!
    @IBOutlet weak var vwTopView: UIView!
    var selectedSupplierId:String!
    var customPicker:CustomDatePicker!
    var isDeliveryDateTime:Bool!
    var dateTime:UILabel!
    var dropDown:DropDown!
    var yearDt:String!
    var userDt:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        vwDatePicker.isHidden = true;
        setTopView(superView: vwTopView);
        setSubTopView(superView: self.vwDateTime)
        topHeader.setTitle(str: "Delivery Check")
        dropDown = DropDown()
        //addMenuView();
        setDesignFont()
        rotated();
        
        // Do any additional setup after loading the view.
    }
    
    
 
    
    override func rotated() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            xConstraint.constant = Constant.horizontalLeading;
        }
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
             xConstraint.constant = Constant.verticalLeading;

        }
    }
    
    
    func setDesignFont(){
        lblSummary.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        lblTemparture.font = UIFont(name: REGULAR_APPFONT, size: Constant.NormalfontSize)
        btnSaveLog.titleLabel?.font = UIFont(name: BOLD_APPFONT, size: Constant.SaveLogfontSize)!
        vwBottom.backgroundColor = UIColor.getGreenColor();
        lblSummary.textColor = UIColor.getGreenColor();
        lblTemparture.textColor = UIColor.getGreenColor();
        
    }
    
    
    
   override func clickedToDateTime(sender: UILabel) {
     //sender.text = "";
    isDeliveryDateTime = false;
    dateTime = sender;
    showingCustomDatePicker();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickedToInjury(_ sender: Any) {
        
        
        var supplierArray = [String]();
        for obj in self.getAppDelegate().defaultData.suppliers! {
            print("Showing Obj :\(obj.name)")
            supplierArray.append(obj.name!);
        }
        
        showingDropDownList(listArray: supplierArray, supperView: self.btnTypeInjury, type: "INJURY")
        
    }
    
    @IBAction func clickedToDateTime(_ sender: Any) {
        isDeliveryDateTime = true;
        showingCustomDatePicker();
        
    }
    
    @IBAction func clickedToPackagingState(_ sender: Any) {
        showingDropDownList(listArray: ["pristine","packaging damaged","packaging opened","content damaged"], supperView: btnPackagingState, type: "PACKAGING STATE")
        
    }
    
    @IBAction func clickedSlider(_ sender: Any) {
    }
    @IBAction func clickedToCelius(_ sender: Any) {
    }

    @IBAction func clickedToFahrenheit(_ sender: Any) {
    }
    
    
    @IBAction func clickedToSaveLogs(_ sender: Any) {
        postDeliveryApi();
    }
    
    
    // Showing List View
    func showingDropDownList(listArray:[String],supperView:UIView,type:String){
        dropDown.anchorView = supperView;
        //  dropDown.dataSource = listArray;
        dropDown.width = Constant.DROPDOWN_WIDTH;
        dropDown.dataSource = listArray;
        dropDown.direction = .bottom
        // Action triggered on selection
        dropDown?.reloadAllComponents()
        dropDown.show()
       // self.view.addSubview(dropDown);
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if type == "INJURY" {
                for obj in self.getAppDelegate().defaultData.entryDefaults! {
                    if obj.name == item {
                        self.selectedSupplierId = obj.id;

                    }
                }
                self.btnTypeInjury.setTitle(item, for: .normal);
            } else if type == "PACKAGING STATE" {
                self.btnPackagingState.setTitle(item, for: .normal);
            }
        }
        
       
    }
    
    func showingCustomDatePicker(){
        vwDatePicker.isHidden = false;
        customPicker = CustomDatePicker(nibName: "CustomDatePicker", bundle: nil);
        customPicker.delegate = self;
        self.vwDatePicker.addSubview(customPicker.view);
        self.topviewConstraint(vwTop: customPicker.view);
        
    }
    
     func selectedDatePickerValue(value:String,yearDt:String) {
        if isDeliveryDateTime == true {
        btnDateTime.setTitle(value, for: .normal);
        self.userDt = yearDt;
        } else {
        dateTime.text = value;
        self.yearDt = yearDt;
        }
    }
    
    func clickedDone() {
      vwDatePicker.isHidden = true;
    }

    
    func postDeliveryApi(){
        if self.yearDt == nil {
            self.yearDt = getCurrentDate();
            return;
        }
        let hrs = self.getTime(strDate: (btnDateTime.titleLabel?.text!)!);
        let mins = self.getMins(strDate: (btnDateTime.titleLabel?.text!)!);
        LoadingOverlay.shared.showOverlay(view:self.view, msg: "")
        let temp = Temperature(unit: "C", value: txtSetTemperature.text ?? "");
        
        let delivery = DeliveryPost.init(locationId: Helper.fetchString(LocationIdOpening), entryTime:self.yearDt, useByDate:self.userDt, supplier:self.selectedSupplierId ?? "", packagingState:(btnPackagingState.titleLabel?.text!)!, temperature: temp, entryHrs:hrs, useByDt: self.userDt, useByMins:mins, invoiceNumber: txtInvoice.text ?? "", comment:txtAddSummary.text ?? "", entryhrs:hrs ?? "", entrymins:mins ?? "");
        
        
        let _ = ChecksService.postDelivery(post: delivery) {(response, error) in
            if(error == nil) {
                LoadingOverlay.shared.hideOverlayView()
                print("response Code is:\(response?.code)")
                if (response?.code == 200) {
                    self.alert("", alertMessage: "Saved Data Successfully", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                        
                    })
                } else {
                    LoadingOverlay.shared.hideOverlayView()
                }
                
            }else {
                self.alert("", alertMessage:"Failed to communicate to server", delegate: self, firstButton: "Ok", secondButton: "", isSingleOK: true, completion: { (title) in
                    
                })
                LoadingOverlay.shared.hideOverlayView()
            }
        }
        
        
    }

    

    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
