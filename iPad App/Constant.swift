//
//  Constant.swift
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 9/9/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

import Foundation

class Constant {
    
    static let topHeaderHeight:CGFloat = 200.0;
    static let GridHeight:CGFloat = 116.0;
    static let FridgeVwHeight:CGFloat = 60.0;
    
    static let ApplianceViewHeight:CGFloat = 115.0;
    static let ApplianceViewCellHeight:CGFloat = 230.0;
    static let ApplianceViewSectionHeaderHeight:CGFloat = 60.0;
    static let topBarHeight:CGFloat = 115.0;
    static let saveButtonHeight:CGFloat = 128.0;
    static let statusBarMargin:CGFloat = 20.0;
    
    static var isOpeningScreen:Bool = false;
    static var isCleaningScreen:Bool = false;
    static var isClosingScreen:Bool = false;
    
    
    // Define Font Size
    static let NormalfontSize:CGFloat = 25.0;
    static let SmallfontSize:CGFloat = 15.0;
    static let SaveLogfontSize:CGFloat = 35.0;
    
    // Define Menu Constants
    static let menuHeaderHeight:CGFloat = 60.0;
    static let menuHeaderViewTrailing:CGFloat = 200.0;
    static let menuLabelHeight:CGFloat = 50.0;
    static let menuLabelWidth:CGFloat = 200.0;
    static let menuLabelX:CGFloat = 140.0;
    static let menuLabelY:CGFloat = 10.0;
    static let menuImageX:CGFloat = 80.0;
    static let menuImageY:CGFloat = 22.0;
    static let menuImageWidth:CGFloat = 25.0;
    static let menuImageHeight:CGFloat = 25.0;
    static let menuRightImageTrailing:CGFloat = 120.0;
    static let menuRightImageX:CGFloat = gettingOrientationWidthHeight().width - 60;
    static let menuViewX:CGFloat = -gettingOrientationWidthHeight().width;
    static let menuViewWidth:CGFloat = gettingOrientationWidthHeight().width - 200;
    static let menuViewHeight:CGFloat = gettingOrientationWidthHeight().height;
    
    static let horizontalLeading:CGFloat = 100.0;
    static let verticalLeading:CGFloat = 0.0;

    
    
    
    // Drop Down Width
    
    static let DROPDOWN_WIDTH = CGFloat(200);
    
   static func gettingOrientationWidthHeight()-> CGRect {
        let screenSize: CGRect = UIScreen.main.bounds
        return screenSize;
    }

    
}
