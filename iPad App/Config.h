//
//  Config.h
//  KitchenLogs for iPad
//
//  Created by Manish Saini on 08/09/17.
//  Copyright © 2017 KitchenLogs Ltd. All rights reserved.
//

#ifndef Config_h
#define Config_h


#import "TPKeyboardAvoidingScrollView.h"


#define REGULAR_APPFONT             "Montserrat-Regular"
#define BOLD_APPFONT                "Montserrat-Bold"
#define MEDIUM_APPFONT              "Montserrat-Light"


#define TERM_CONDITIONS              "https://www.kitchenlo.gs/terms/"
#define PRIVACY_POLICY               "https://www.kitchenlo.gs/privacy-policy/"

#define LANDSCAPE_NOTIFY             "LandScapeNotify"
#define POTRAIT_NOTIFY               "PotraitNotify"

#define USER_TOKEN                    "Token Key"

#define LocationIdOpening                    "locationIdOpening"
#define BuisnessIdOpening                    "buisnessIdOpening"
#define LocationIdClosing                    "locationIdClosing"
#define BuisnessIdClosing                    "buisnessIdClosing"
#define LocationIdCleaning                   "locationIdCleaning"
#define BuisnessIdCleaning                   "buisnessIdCleaning"




#endif /* Config_h */
