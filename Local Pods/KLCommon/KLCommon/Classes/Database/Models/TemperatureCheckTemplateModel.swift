//
//  TemperatureCheckTemplateModel.swift
//  Pods
//
//  Created by Viren Bhandari on 9/22/17.
//
//

import UIKit
import RealmSwift

open class TemperatureCheckTemplateModel: Object {
    dynamic var  mid = ""
    dynamic var name = ""
    dynamic var desc = ""
    dynamic var cooked = false
    dynamic var reHeating = false
    dynamic var hotHolding = false
    dynamic var number = 0
    dynamic var itemType = ""
    
    override open static func primaryKey() -> String? {
        return "mid"
    }
    
}
