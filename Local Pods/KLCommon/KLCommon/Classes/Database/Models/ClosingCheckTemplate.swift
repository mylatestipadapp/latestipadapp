//
//  ClosingCheckTemplate.swift
//  Pods
//
//  Created by Viren Bhandari on 9/22/17.
//
//

import UIKit
import RealmSwift

open class ClosingCheckTemplate: Object {

    dynamic var mid = ""
    dynamic var businessId = ""
    dynamic var locationId = ""
    dynamic var name = ""
    let checkListTemplateModel = List<CheckListTemplateModel>()
    let applianceCheck = List<TemperatureCheckTemplateModel>()
    let foodCheck = List<TemperatureCheckTemplateModel>()
    
    override open static func primaryKey() -> String? {
        return "mid"
    }
}
