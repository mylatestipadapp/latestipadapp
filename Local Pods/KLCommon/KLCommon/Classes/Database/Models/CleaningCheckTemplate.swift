//
//  CleaningCheckTemplate.swift
//  Pods
//
//  Created by Viren Bhandari on 9/22/17.
//
//

import UIKit
import RealmSwift

open class CleaningCheckTemplate: Object {
    
    dynamic var mid = ""
    dynamic var businessId = ""
    dynamic var locationId = ""
    dynamic var name = ""
    let afterUse = List<CheckListTemplateModel>()
    let endOfDay = List<CheckListTemplateModel>()
    let deepClean = List<CheckListTemplateModel>()
    
    override open static func primaryKey() -> String? {
        return "mid"
    }
}

