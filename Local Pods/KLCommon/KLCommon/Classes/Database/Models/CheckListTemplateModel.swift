//
//  CheckListTemplateModel.swift
//  Pods
//
//  Created by Viren Bhandari on 9/22/17.
//
//

import UIKit
import RealmSwift

open class CheckListTemplateModel: Object {
    dynamic var mid : String = ""
    dynamic var label : String = ""
    dynamic var desc : String = ""

    override open static func primaryKey() -> String? {
        return "mid"
    }
    
}
