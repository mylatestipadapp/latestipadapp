//
//  OpeningCheckTemplate.swift
//  Pods
//
//  Created by Viren Bhandari on 9/22/17.
//
//

import UIKit
import RealmSwift

open class OpeningCheckTemplate: Object {
    
    dynamic var mid :  String = ""
    dynamic var businessId :  String = ""
    dynamic var locationId : String = ""
    dynamic var name : String = ""
    let checkListTemplateModel = List<CheckListTemplateModel>()
    let applianceCheck = List<TemperatureCheckTemplateModel>()
    let foodCheck = List<TemperatureCheckTemplateModel>()
    
    override open static func primaryKey() -> String? {
        return "mid"
    }
}
