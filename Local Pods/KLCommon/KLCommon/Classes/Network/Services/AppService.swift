//
//  AppService.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import  Alamofire

open class AppService: NSObject {
    
    
    fileprivate static let installApi : String = "api/install"
    
    public static func logAppInstalled(post: InstallPost, callback:@escaping APICallback) -> IApiOperation {
        let apiOperation = ApiOperation<InstallationResposne>(relativeUrl: AppService.installApi, methodType: Alamofire.HTTPMethod.post, InstallationResposne.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON() as! [String : AnyObject]
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }
}
