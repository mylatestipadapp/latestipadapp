//
//  HomeService.swift
//  Pods
//
//  Created by Viren Bhandari on 16/09/17.
//
//

import UIKit
import Alamofire

open class HomeService: NSObject {

    fileprivate static let getDrawerMenu : String = "api/settings/sidemenu"
    
    public static func getDrawerMenu(callback:@escaping APICallback) -> IApiOperation
    {
        let apiOperation = ApiOperation<DrawerMenuResponse>(relativeUrl: getDrawerMenu, methodType: Alamofire.HTTPMethod.get, DrawerMenuResponse.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }
}

