//
//  NotificationService.swift
//  Pods
//
//  Created by Viren Bhandari on 16/09/17.
//
//

import UIKit
import  Alamofire

open class NotificationService: NSObject {
    
    fileprivate static let getNotificationSettings : String = "api/settings/getsettings/%@/%@"
    
    public static func getViewLogs(locationId : String, businessId : String, callback:@escaping APICallback) -> IApiOperation
    {
        let apiOperation = ApiOperation<InstallationResposne>(relativeUrl: NSString.localizedStringWithFormat(getNotificationSettings as NSString, businessId, locationId) as String, methodType: Alamofire.HTTPMethod.get, InstallationResposne.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }
    
}
