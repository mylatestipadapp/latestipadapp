//
//  ChecksService.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import Alamofire

open class ChecksService: NSObject {
    
    fileprivate static let checkgetapi : String = "api/templates/default/all"
    fileprivate static let getLogs : String = "api/entries/all"
    
    fileprivate static let saveClosingCheck = "api/checks/closing"
    fileprivate static let saveOpeningCheck = "api/checks/opening"
    fileprivate static let saveCleaningCheck = "api/checks/cleaning"
    fileprivate static let saveProbeCheck = "api/checks/miscchecks/probe"
    fileprivate static let saveInjuryCheck = "api/checks/miscchecks/injury"
    fileprivate static let savePestCheck = "api/checks/miscchecks/pest"
    

    fileprivate static let saveDelivery = "api/checks/delivery/pest"
    fileprivate static let saveApplianceCheck = "api/entries"
    
    public static func getDefaultChecksTempletes(callback:@escaping APICallback)
    {
        let apiOperation = ApiOperation<AllChecksResponse>(relativeUrl: checkgetapi, methodType: Alamofire.HTTPMethod.get, AllChecksResponse.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.executeRequestWithCallback(callback: callback)
    }

    public static func getViewLogs(post: ViewLogsPost, callback:@escaping APICallback) -> IApiOperation
    {
        let apiOperation = ApiOperation<ViewLogsResponse>(relativeUrl: getLogs, methodType: Alamofire.HTTPMethod.post, ViewLogsResponse.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON() as! [String : AnyObject]
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }
    
    
    public static func postPest(post: PestPost, callback:@escaping APICallback) -> IApiOperation
    {
        let apiOperation = ApiOperation<PestResponse>(relativeUrl: savePestCheck, methodType: Alamofire.HTTPMethod.post, PestResponse.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON() as! [String : AnyObject]
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }
    
    public static func postInjury(post: InjuryPost, callback:@escaping APICallback) -> IApiOperation
    {
        let apiOperation = ApiOperation<injuryResponse>(relativeUrl: saveInjuryCheck, methodType: Alamofire.HTTPMethod.post, injuryResponse.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON() as! [String : AnyObject]
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }
    
    
    public static func postCleaning(post: CleaningPost, callback:@escaping APICallback) -> IApiOperation
    {
        let apiOperation = ApiOperation<CleaningPostResponse>(relativeUrl: saveCleaningCheck, methodType: Alamofire.HTTPMethod.post, CleaningPostResponse.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON() as! [String : AnyObject]
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }
    public static func postOpening(post:OpeningPost, callback:@escaping APICallback) -> IApiOperation
    {
        let apiOperation = ApiOperation<OpeningPostResponse>(relativeUrl: saveOpeningCheck, methodType: Alamofire.HTTPMethod.post, OpeningPostResponse.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON() as! [String : AnyObject]
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }
    public static func postClosing(post:ClosingPost, callback:@escaping APICallback) -> IApiOperation
    {
        let apiOperation = ApiOperation<ClosingPostResponse>(relativeUrl: saveClosingCheck, methodType: Alamofire.HTTPMethod.post, ClosingPostResponse.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON() as! [String : AnyObject]
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }
    public static func postAppliance(post:AppliancePost, callback:@escaping APICallback) -> IApiOperation
    {
        let apiOperation = ApiOperation<AppliancePostResponse>(relativeUrl: saveApplianceCheck, methodType: Alamofire.HTTPMethod.post, AppliancePostResponse.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON() as! [String : AnyObject]
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }
    public static func postFood(post:FoodPost, callback:@escaping APICallback) -> IApiOperation
    {
        let apiOperation = ApiOperation<FoodPostResponse>(relativeUrl: saveApplianceCheck, methodType: Alamofire.HTTPMethod.post, FoodPostResponse.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON() as! [String : AnyObject]
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }
    public static func postProbeCheck(post:ProbePost, callback:@escaping APICallback) -> IApiOperation
    {
        let apiOperation = ApiOperation<ProbeResponse>(relativeUrl: saveProbeCheck, methodType: Alamofire.HTTPMethod.post, ProbeResponse.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON() as! [String : AnyObject]
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }

    
    public static func postDelivery(post:DeliveryPost, callback:@escaping APICallback) -> IApiOperation
    {
        let apiOperation = ApiOperation<DeliveryResponse>(relativeUrl: saveDelivery, methodType: Alamofire.HTTPMethod.post, DeliveryResponse.self, encoding:JSONEncoding.default)
        apiOperation.contentType = APIConstants.MimeTypeJSON
        apiOperation.mapping = MappingType.Object
        apiOperation.requestParameters = post.toJSON() as! [String : AnyObject]
        apiOperation.executeRequestWithCallback(callback: callback)
        return apiOperation
    }
    
    
    
    
}
