//
//  GenericResponse.swift
//  Pods
//
//  Created by Viren Bhandari on 16/09/17.
//
//

import UIKit
import Gloss

open class GenericResponse: Glossy {
    
    public var code : Int?
    public var message : String?

    init() {
        
    }
    
    public required init?(json: JSON) {
        self.code = "code" <~~ json
        self.message = "message" <~~ json
    }

    public func toJSON() -> JSON? {
        return jsonify([
            "code" ~~> self.code,
            "message" ~~> self.message
            ])
    }
}
