//
//  ProbePost.swift
//  Pods
//
//  Created by Manish Saini on 24/09/17.
//
//

import UIKit
import Gloss
open class ProbePost: Decodable, Encodable {
    public var locationId : String?
    public var businessId : String?
    public var date:String?
    public var hrs:String?
    public var mins:String?
    public var isIceTestWorking:Bool?
    public var isBoilingWaterTestWorking:Bool?
    
    init() {
        
    }
    
    public init(locationId:String, businessId:String,date:String,hrs:String,mins:String,isIceTestWorking:Bool?, isBoilingWaterTestWorking:Bool?) {
        self.locationId = locationId;
        self.businessId = businessId;
        self.date = date;
        self.hrs = hrs;
        self.mins = mins;
        self.isIceTestWorking = isIceTestWorking;
        self.isBoilingWaterTestWorking = isBoilingWaterTestWorking;
        
    }
    
    public required init?(json: JSON) {
        self.locationId = "locationId" <~~ json
        self.businessId = "businessId" <~~ json
        self.date = "date" <~~ json
        self.hrs = "hrs" <~~ json
        self.mins = "mins" <~~ json
        self.isIceTestWorking = "isIceTestWorking" <~~ json
        self.isBoilingWaterTestWorking = "isBoilingWaterTestWorking" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "locationId" ~~> self.locationId,
            "businessId" ~~> self.businessId,
            "date" ~~> self.date,
            "hrs" ~~> self.hrs,
            "mins" ~~> self.mins,
            "isIceTestWorking" ~~> self.isIceTestWorking,
            "isBoilingWaterTestWorking" ~~> self.isBoilingWaterTestWorking
            ])
    }

}
