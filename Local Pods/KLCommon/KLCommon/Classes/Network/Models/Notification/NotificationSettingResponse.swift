//
//  NotificationSettingResponse.swift
//  Pods
//
//  Created by Viren Bhandari on 16/09/17.
//
//

import UIKit
import Gloss

open class NotificationSettingResponse: Decodable {
    
    public var id : String?
    public var businessID : String?
    public var locationId : String?
    public var checks : [NotiCheckModel]?
    public var userContacts : [NotiUserModel]?
    public var timings : [NotiTimeModel]?
    
    init() {
        
    }
    
    public required init?(json: JSON) {
        self.id = "_id" <~~ json
        self.businessID = "businessId" <~~ json
        self.locationId = "locationId" <~~ json
        self.checks = "checks" <~~ json
        self.userContacts = "userContacts" <~~ json
        self.timings = "timings" <~~ json
    }
    
}
