//
//  NotiCheckModel.swift
//  Pods
//
//  Created by Viren Bhandari on 16/09/17.
//
//

import UIKit
import Gloss

open class NotiCheckModel: Decodable{
    public var notificatioFreqHr : Int?
    public var notifcationCount : Int?
    public var sms : Bool?
    public var notificationMin : NoticationTime?
    public var notificationHr : NoticationTime?
    public var checktype : String?

    init() {
        
    }
    
    public required init?(json: JSON) {
            self.notificatioFreqHr = "notificatioFreqHr" <~~ json
        self.notifcationCount = "notificationCount" <~~ json
        self.sms = "sms" <~~ json
        self.notificationHr = "notificationHr" <~~ json
        self.notificationMin = "notificationMin" <~~ json
        self.checktype = "checkType" <~~ json
    }
    
    
    open class NoticationTime : Decodable {
        
        public var value : Int?
        public var label : String?
        
        init() {
            
        }
        public required init?(json: JSON) {
            self.label = "label" <~~ json
            self.value = "value" <~~ json
        }
        
    }
    
}
