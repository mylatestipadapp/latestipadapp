//
//  NotiTimeModel.swift
//  Pods
//
//  Created by Viren Bhandari on 16/09/17.
//
//

import UIKit
import Gloss

open class NotiTimeModel: Decodable {
    
    public var closingMins : String?
    public var closingHrs : String?
    public var openingMins : String?
    public var openingHrs : String?
    public var isClosed : Bool?
    public var day : String?
    
    init() {
        
    }
    
    public required init?(json: JSON) {
        self.closingHrs = "closingHrs" <~~ json
        self.closingMins = "closingMins" <~~ json
        self.openingHrs = "openingHrs" <~~ json
        self.openingMins = "openingMins" <~~ json
        self.isClosed = "isClosed" <~~ json
        self.day = "day" <~~ json
    }
}
