//
//  InstallPost.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import Gloss

open class InstallPost: Glossy {

    public var device_token : String?
    public var date_installed : String?
    public var os : String?
    public var os_version : String?
    public var device : String?
    public var manufacturer : String?
    public var app_version : String?

    init() {
        
    }
    
    public required init?(json: JSON) {
        self.device_token = "device_token" <~~ json
        self.date_installed = "date_installed" <~~ json
        self.os = "os" <~~ json
        self.os_version = "os_version" <~~ json
        self.device = "device" <~~ json
        self.manufacturer = "manufacturer" <~~ json
        self.app_version = "app_version" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "device_token" ~~> self.device_token,
            "date_installed" ~~> self.date_installed,
            "os" ~~> self.os,
            "os_version" ~~> self.os_version,
            "device" ~~> self.device,
            "manufacturer" ~~> self.manufacturer,
            "app_version" ~~> self.app_version
            ])
    }
}
