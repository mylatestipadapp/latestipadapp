//
//  Template.swift
//  Pods
//
//  Created by Fitastik on 24/09/17.
//
//

import Foundation
import Gloss

open class ApplianceParam: Decodable, Encodable {
    
    public var type : String?
    public var unit : String?

    public var temperature : String?
    public var comment : String?
    public var commentRequired : Bool?
    
    init() {
        
    }
    
    public init(type:String, temperature:String,comment:String,commentRequired:Bool,unit:String) {
        self.type = type;
        self.temperature = temperature;
        self.comment = comment
        self.commentRequired = commentRequired
        self.unit = unit
    }
    public required init?(json: JSON) {
//        self.id = "_id" <~~ json
//        self.name = "name" <~~ json
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "type" ~~> self.type,
            "unit" ~~> self.unit,"temperature" ~~> self.temperature,"comment" ~~> self.comment,"commentRequired" ~~> self.commentRequired
            
            ])
    }
}
