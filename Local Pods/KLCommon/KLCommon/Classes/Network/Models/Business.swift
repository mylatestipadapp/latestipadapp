//
//  Business.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/8/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import Gloss

public class Business: Decodable, Encodable {
    
    public var id : String?
    public var name : String?
    public var createdByEmail : String?
    public var __v : Int?
    public var customer : Customer?
    public var createdAt : String?
    
    public init() {
        
    }
    
    public required init?(json: JSON) {
        self.id = "id" <~~ json
        self.name = "name" <~~ json
        self.__v = "__v" <~~ json
        self.createdByEmail = "createdByEmail" <~~ json
        self.createdAt = "createdAt" <~~ json
        self.customer = "customer" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> self.id,
            "name" ~~> self.name,
            "customer" ~~> self.customer,
            "createdByEmail" ~~> self.createdByEmail,
            "createdAt" ~~> self.createdAt,
            "__v" ~~> self.__v
            ])
    }
}
