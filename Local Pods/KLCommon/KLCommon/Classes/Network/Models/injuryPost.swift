//
//  injuryPost.swift
//  Pods
//
//  Created by Manish Saini on 24/09/17.
//
//

import UIKit
import Gloss
open class InjuryPost: Decodable, Encodable {
    public var locationId : String?
    public var businessId : String?
    public var date:String?
    public var hrs:String?
    public var mins:String?
    public var summary:String?
    public var injuryType:String?
    
    init() {
        
    }
    
    public init(locationId:String, businessId:String,date:String,hrs:String,mins:String,summary:String,injuryType:String?) {
        self.locationId = locationId;
        self.businessId = businessId;
        self.date = date;
        self.hrs = hrs;
        self.mins = mins;
        self.summary = summary;
        self.injuryType = injuryType;
        
    }
    
    public required init?(json: JSON) {
        self.locationId = "locationId" <~~ json
        self.businessId = "businessId" <~~ json
        self.date = "date" <~~ json
        self.hrs = "hrs" <~~ json
        self.mins = "mins" <~~ json
        self.summary = "summary" <~~ json
        self.injuryType = "injuryType" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "locationId" ~~> self.locationId,
            "businessId" ~~> self.businessId,
            "date" ~~> self.date,
            "hrs" ~~> self.hrs,
            "mins" ~~> self.mins,
            "summary" ~~> self.summary,
            "injuryType" ~~> self.injuryType
            ])
    }

}
