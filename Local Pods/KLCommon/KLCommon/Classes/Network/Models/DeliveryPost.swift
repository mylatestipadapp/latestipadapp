//
//  DeliveryPost.swift
//  Pods
//
//  Created by Manish Saini on 24/09/17.
//
//

import UIKit
import Gloss

  open class DeliveryPost: Decodable, Encodable {
    public var locationId : String?
    public var entryTime:String?
    public var useByDate:String?
    public var supplier:String?
    public var packagingState:String?
    public var temperature:Temperature?
    public var entryHrs:String?
    public var entryMins:String?
    public var useByDt:String?
    public var useByHrs:String?
    public var useByMins:String?
    public var invoiceNumber:String?
    public var comment:String?
    public var entryhrs:String?
    public var entrymins:String?
    
    init() {
        
    }
    
    public init(locationId:String, entryTime:String,useByDate:String,supplier:String,packagingState:String,temperature:Temperature,entryHrs:String?,useByDt:String?,useByMins:String?,invoiceNumber:String?,comment:String?,entryhrs:String?,entrymins:String?) {
        self.locationId = locationId;
        self.entryTime = entryTime;
        self.useByDate = useByDate;
        self.supplier = supplier;
        self.packagingState = packagingState;
        self.temperature = temperature;
        self.entryHrs = entryHrs;
        self.useByDt = useByDt;
        self.useByMins = useByMins;
        self.invoiceNumber = invoiceNumber;
        self.comment = comment;
        self.entrymins = entrymins;
        
    }
    
    public required init?(json: JSON) {
        self.locationId = "locationId" <~~ json
        self.entryTime = "entryTime" <~~ json
        self.useByDate = "useByDate" <~~ json
        self.supplier = "supplier" <~~ json
        self.packagingState = "packagingState" <~~ json
        self.temperature = "temperature" <~~ json
        self.entryHrs = "entryHrs" <~~ json
        self.useByDt = "useByDt" <~~ json
        self.useByMins = "useByMins" <~~ json
        self.invoiceNumber = "invoiceNumber" <~~ json
        self.comment = "comment" <~~ json
        self.entrymins = "entrymins" <~~ json

    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "locationId" ~~> self.locationId,
            "entryTime" ~~> self.entryTime,
            "useByDate" ~~> self.useByDate,
            "supplier" ~~> self.supplier,
            "packagingState" ~~> self.packagingState,
            "temperature" ~~> self.temperature,
            "entryHrs" ~~> self.entryHrs,
            "useByDt" ~~> self.useByDt,
            "useByMins" ~~> self.useByMins,
            "invoiceNumber" ~~> self.invoiceNumber,
            "comment" ~~> self.comment,
            "entrymins" ~~> self.entrymins
            ])
    }

}

open class Temperature: Decodable, Encodable {
    public var unit : String?
    public var value : String?
    
    
    public init(unit:String, value:String) {
        self.unit = unit;
        self.value = value;

        
    }
    
    public required init?(json: JSON) {
        self.unit = "unit" <~~ json
        self.value = "value" <~~ json

    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "unit" ~~> self.unit,
            "value" ~~> self.value

            ])
    }

    
    
    
}

