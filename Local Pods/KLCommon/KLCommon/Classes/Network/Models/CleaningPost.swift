//
//  ViewLogsPost.swift
//  Pods
//
//  Created by Viren Bhandari on 16/09/17.
//
//

import UIKit
import Gloss


open class CleaningPost: Decodable, Encodable {

    public var templates : Template?
    public var categories : [CheckCategory]?
    public var entryTime : String?
    public var entryHrs : String?
    public var entryMins : String?
    public var locationId : String?
    public var entryhrs = "null"
    public var entrymins = "null"



    init() {
        
    }
    
    public init(templates:Template, categorie:[CheckCategory],entryTm:String,entryMin:String,locationIds:String,entryHr:String) {
        self.locationId = locationIds;
        self.templates = templates;
        self.categories = categorie;
        self.entryTime = entryTm
        self.entryHrs = entryHr
        self.entryMins = entryMin
    }
    
    public required init?(json: JSON) {
            self.locationId = "locationId" <~~ json
        self.categories = "categories" <~~ json
        self.templates = "template" <~~ json
        self.entryTime = "entryTime" <~~ json
        self.entryHrs = "entryHrs" <~~ json
        self.entryMins = "entryMins" <~~ json
       
        

    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "locationId" ~~> self.locationId,"entryTime" ~~> self.entryTime,"categories" ~~> self.categories,"template" ~~> self.templates,"entryHrs" ~~> self.entryHrs,"entryMins" ~~> self.entryMins
            ])
    }
}
