//
//  User.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/8/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import Gloss

open class User: Decodable, Encodable {
    
    public var id : String?
    public var name : String?
    public var phone : String?
    public var email : String?
    public var businessId : String?
    public var business : [Business]?
    public var userType : UserType?
    
    public init() {
        
    }
    
    public required init?(json: JSON) {
        self.id = "id" <~~ json
        self.name = "name" <~~ json
        self.phone = "phone" <~~ json
        self.email = "email" <~~ json
        self.businessId = "businessId" <~~ json
        self.business = "business" <~~ json
        self.userType = "userType" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> self.id,
            "name" ~~> self.name,
            "phone" ~~> self.phone,
            "email" ~~> self.email,
            "businessId" ~~> self.business,
            "business" ~~> self.business,
            "userType" ~~> self.userType
            ])
    }
}
