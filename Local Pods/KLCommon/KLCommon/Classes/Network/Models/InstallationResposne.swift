//
//  InstallationResposne.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import Gloss

open class InstallationResposne : GenericResponse {
  
    //public var data : InstallPost?
//    public var code : Int!
//    public var message : String!
    public var data : [ViewLogs]!
    
    override init( ) {
        super.init()
    }
    
    
    public required init?(json: JSON) {
        super.init(json: json)
        self.code = "code" <~~ json
        self.message = "message" <~~ json
        self.data = "data" <~~ json
    }
    
    public override func toJSON() -> JSON? {
        return jsonify([
            "code" ~~> self.code,
            "message" ~~> self.message,
            "data" ~~> self.data
            ])
    }
    
    
    public class ViewLogs : Decodable, Encodable {
        
        public var type : String?
        public var entryDate : String?
        public var name : String?
        public var entryTime : String?
        
        init() {
            
        }
        
        public required init?(json: JSON) {
            self.type = "type" <~~ json
            self.entryDate = "entryDate" <~~ json
            self.name = "name" <~~ json
            self.name = "entryTime" <~~ json

        }
        
        public func toJSON() -> JSON? {
            return jsonify([
                "type" ~~> self.type,
                "entryDate" ~~> self.entryDate,
                "name" ~~> self.name,
                "entryTime" ~~> self.entryTime
                ])
        }
    }
}
