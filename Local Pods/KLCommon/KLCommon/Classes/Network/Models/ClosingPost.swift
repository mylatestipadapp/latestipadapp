//
//  ViewLogsPost.swift
//  Pods
//
//  Created by Viren Bhandari on 16/09/17.
//
//

import UIKit
import Gloss


open class ClosingPost: Decodable, Encodable {
    
    public var templates : Template?
    public var entryTime : String?
    public var entryHrs : String?
    public var entryMins : String?
    public var locationId : String?
    public var cooking : [CookedType]?
    public var hotHolding : [CookedType]?
    public var reHeating : [CookedType]?
    public var fridges : [FridgePost]?
    public var freezers : [FreezerPost]?
    
    
    
    
    public var items : [CheckItems]?
    
    public var type : String?
    
    public var id : String?
    
    public var entryhrs = "null"
    public var entrymins = "null"
    
    
    
    init() {
        
    }
    
    public init(templates:Template, cooking:[CookedType],entryTm:String,entryMin:String,locationIds:String,entryHr:String,hotHolding:[CookedType],reHeating:[CookedType],fridges:[FridgePost],freezers:[FreezerPost],items : [CheckItems],type : String,id : String) {
        self.locationId = locationIds;
        self.templates = templates;
        self.cooking = cooking;
        self.hotHolding = hotHolding;
        
        
        self.entryTime = entryTm
        self.entryHrs = entryHr
        self.entryMins = entryMin
        self.reHeating = reHeating
        self.fridges = fridges
        self.freezers = freezers
        self.items = items
        self.type = type
        self.id = id
        
        
    }
    
    public required init?(json: JSON) {
        self.locationId = "locationId" <~~ json
        self.cooking = "cooking" <~~ json
        self.reHeating = "reHeating" <~~ json
        self.hotHolding = "hotHolding" <~~ json
        self.fridges = "fridges" <~~ json
        self.freezers = "freezers" <~~ json
        self.type = "type" <~~ json
        self.id = "_id" <~~ json
        self.items = "items" <~~ json
        
        
        
        
        self.templates = "template" <~~ json
        self.entryTime = "entryTime" <~~ json
        self.entryHrs = "entryHrs" <~~ json
        self.entryMins = "entryMins" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "locationId" ~~> self.locationId,"cooking" ~~> self.cooking,"reHeating" ~~> self.reHeating,"hotHolding" ~~> self.hotHolding,"fridges" ~~> self.fridges,"freezers" ~~> self.freezers,"type" ~~> self.type,"_id" ~~> self.id,"items" ~~> self.items,"templates" ~~> self.templates,"entryTime" ~~> self.entryTime,"entryHrs" ~~> self.entryHrs,"entryMins" ~~> self.entryMins
            ])
    }
}
