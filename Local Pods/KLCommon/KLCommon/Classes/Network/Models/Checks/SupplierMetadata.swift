//
//  SupplierMetadata.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import Gloss

open class SupplierMetadata: Glossy {
    
    public var id : String?
    public var businessId : String?
    public var createdBy : User?
    public var name : String?
    public var address : String?
    public var phone : String?
    public var salesContact : String?
    public var accountNickname : String?
    public var accountNumber : String?
    public var createdAt : String?
    
    init() {
        
    }
    
    public required init?(json: JSON) {
        self.id = "_id" <~~ json
        self.name = "name" <~~ json
        self.businessId = "businessId" <~~ json
        self.createdBy = "createdBy" <~~ json
        self.createdAt = "createdAt" <~~ json
        self.address = "address" <~~ json
        self.phone = "phone" <~~ json
        self.salesContact = "salesContact" <~~ json
        self.accountNickname = "accountNickname" <~~ json
        self.accountNumber = "accountNumber" <~~ json
    }
    
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "name" ~~> self.name,
            "businessId" ~~> self.businessId,
            "createdBy" ~~> self.createdBy,
            "createdAt" ~~> self.createdAt,
            "address" ~~> self.address,
            "phone" ~~> self.phone,
            "salesContact" ~~> self.salesContact,
            "accountNickname" ~~> self.accountNickname,
            "accountNumber" ~~> self.accountNumber
            ])
    }
}
