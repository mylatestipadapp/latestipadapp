//
//  OpeningCheckPost.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import Gloss

open class OpeningCheckPost: Decodable, Encodable {
    
    public init() {
        
    }
    
    public required init?(json: JSON) {
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            
            ])
    }
    //    "_id":"5981bc4daf6ddae5f524dfae","businessId":"595015b7cc18481c096f7a44","locationId":"5981b8be15778e7cf7d3df5a","name":"Thames Opening 1","isDefault":true,"type":"opening","__v":0,"items":[{"_id":"5981bc4d15778e7cf7d3df90","description":"Seating arrangement has been done as required"},{"_id":"5981bc4d15778e7cf7d3df8f","description":"All linen is cleaned, ironed & folded as per the standard"},{"_id":"5981bc4d15778e7cf7d3df8e","description":"Adequate mise en place has been stacked"},{"_id":"5981bc4d15778e7cf7d3df8d","description":"Check for table/chair wobbling"},{"_id":"5981bc4d15778e7cf7d3df8c","description":"Check for cleanliness & proper lay-out of table"},{"_id":"5981bc4d15778e7cf7d3df8b","description":"Check quality of glassware and discard chipped glasses"},{"_id":"5981bc4d15778e7cf7d3df8a","description":"Crockery & cutlery checked for cleanliness"},{"_id":"5981bc4d15778e7cf7d3df89","description":"Walls and curtains are checked for any visible spots"},{"_id":"5981bc4d15778e7cf7d3df88","description":"Check for fused bulbs and follow up with maintenance"},{"_id":"5981bc4d15778e7cf7d3df87","description":"All lights and electrical points operational"},{"_id":"5981bc4d15778e7cf7d3df86","description":"Air-conditioning operational"},{"_id":"5981bc4d15778e7cf7d3df85","description":"Check for the proper stacking of mineral water/soft drinks"},{"_id":"5981bc4d15778e7cf7d3df84","description":"Ensure all stores & supplies are in place with required par"},{"_id":"5981bc4d15778e7cf7d3df83","description":"Ensure the side station is stacked properly as per the standard"},{"_id":"5981bc4d15778e7cf7d3df82","description":" Check ice cube machine"},{"_id":"5981bc4d15778e7cf7d3df81","description":"Ensure that the POS system is working properly"},{"_id":"5981bc4d15778e7cf7d3df80","description":"Check if the telephone is working"},{"_id":"5981bc4d15778e7cf7d3df7f","description":"Ensure that the non-available item list is written on the notice board & everybody is aware"},{"_id":"5981bc4d15778e7cf7d3df7e","description":"Ensure that everybody has service kit & is properly groomed"},{"_id":"5981bc4d15778e7cf7d3df7d","description":"Brief staff for the operation & up-selling"},{"_id":"5981bc4d15778e7cf7d3df7c","description":"Allocate staff to their respective areas"}],"entryTime":"22-08-2017","createdAt":"2017-08-22T06:16:37.946Z","isActive":true,"status":true,"entryHrs":11,"entryMins":46,"entryhrs":null,"entrymins":null,"cooking":[{"name":"Duck","type":"cooked","temperature":8,"commentRequired":true},{"name":"Chicken","type":"cooked","temperature":90,"commentRequired":false}],"hotHolding":[{"name":"Duck","type":"hotHolding","temperature":92,"commentRequired":false},{"name":"Chicken","type":"hotHolding","temperature":89,"commentRequired":false}],"reHeating":[{"name":"Duck","type":"reHeating","temperature":100,"commentRequired":false}],"fridges":[{"fridgeNum":1,"temperature":5,"comment":"","commentRequired":false}],"freezers":[{"freezerNum":1,"temperature":-17,"comment":"","commentRequired":true}]}
}
