//
//  AllCheckMetadata.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import Gloss

open class AllCheckMetadata: Glossy {
    
    public var openingCheck : [CheckMetadata]?
    public var closingCheck : [CheckMetadata]?
    public var cleaningCheck : [CheckMetadata]?
    public var entryDefaults : [EntryDefaultMetadata]?
    public var suppliers : [SupplierMetadata]?
    
    public init() {
    }
    
    public required init?(json: JSON) {
        self.openingCheck = "opening" <~~ json
        self.closingCheck = "closing" <~~ json
        self.cleaningCheck = "cleaning" <~~ json
        self.entryDefaults = "entryDefaults" <~~ json
        self.suppliers = "suppliers" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "opening" ~~> self.openingCheck,
            "cleaning" ~~> self.cleaningCheck,
            "closing" ~~> self.closingCheck,
            "entryDefaults" ~~> self.entryDefaults,
            "suppliers" ~~> self.suppliers
            ]);
    }
}
