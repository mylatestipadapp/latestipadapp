//
//  OpeningCheckMetadata.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import Gloss

open class CheckMetadata: Glossy {
    
    public var id : String?
    public var businessId : String?
    public var locationId : String?
    public var name : String?
    public var isDefault : Bool?
    public var type : String?
    public var __v: Int?
    public var entryTime : String?
    public var createdAt : String?
    public var isActive : Bool?
    public var status : Bool?
    public var categories : [CheckCategory]?
    public var items : [CheckItems]?

    
    public init() {
        
    }
    
    public required init?(json: JSON) {
        self.id = "_id" <~~ json
        self.businessId = "bussinessId" <~~ json
        self.__v = "__v" <~~ json
        self.locationId = "locationId" <~~ json
        self.name = "name" <~~ json
        self.isDefault = "isDefault" <~~ json
        self.type = "type" <~~ json
        self.entryTime = "entryTime" <~~ json
        self.items = "items" <~~ json
        self.isActive = "isActive" <~~ json
        self.createdAt = "createdAt" <~~ json
        self.status = "status" <~~ json
        self.categories = "categories" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "businessId" ~~> self.businessId,
            "locationId" ~~> self.locationId,
            "name" ~~> self.name,
            "createdAt" ~~> self.createdAt,
            "__v" ~~> self.__v,
            "items" ~~> self.items,
            "isActive" ~~> self.isActive,
            "status" ~~> self.status,
            "entryTime" ~~> self.entryTime,
            "type" ~~> self.type,
            "isDefault" ~~> self.isDefault,
            "categories" ~~> self.categories

            ])
    }
    
}
