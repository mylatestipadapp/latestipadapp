//
//  FoodItem.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import Gloss

open class FoodItem: Decodable, Encodable {
    
    public var id: String?
    public var hotHolding : Bool?
    public var reheating : Bool?
    public var cooked : Bool?
    public var name :String?
    public var createdAt : String?
    
    init() {
        
    }
    
    public required init?(json: JSON) {
        self.id = "_id" <~~ json
        self.hotHolding = "hotHolding" <~~ json
        self.reheating = "reheating" <~~ json
        self.cooked = "cooked" <~~ json
        self.name = "name" <~~ json
        self.createdAt = "createdAt" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "hotHolding" ~~> self.hotHolding,
            "reheating" ~~> self.reheating,
            "cooked" ~~> self.cooked,
            "name" ~~> self.name,
            "createdAt" ~~> self.createdAt
            ])
    }
}
