//
//  AllChecksResponse.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import Gloss

open class AllChecksResponse: Decodable, Encodable {
    
    public var message : String?
    public var code : Int?
    public var data : AllCheckMetadata?
    
    public init() {
        
    }
    
    public required init?(json: JSON) {
        self.message = "message" <~~ json
        self.code = "code" <~~ json
        self.data = "data" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "message" ~~> self.message,
            "code" ~~> self.code,
            "data" ~~> self.data
            ])
    }
}
