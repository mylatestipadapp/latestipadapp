//
//  CheckCategory.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import  Gloss

open class CheckCategory: Decodable, Encodable {
    
    public var id : String?
    public var checkAllEnabled : Bool?

    public var name : String?
    public var camelCaseName : String?

    public var items : [CheckItems]?

    
    init() {
        
    }
    
    public init(id:String, name:String,items:[CheckItems],checkAllEnabled:Bool,camelCaseName:String) {
        self.id = id;
        self.checkAllEnabled = checkAllEnabled;
        self.name = name;
        self.items = items;
        self.camelCaseName = camelCaseName
    }
    public required init?(json: JSON) {
        self.id = "_id" <~~ json
        self.name = "name" <~~ json
        self.items = "items" <~~ json
        self.checkAllEnabled = "checkAllEnabled" <~~ json
        self.camelCaseName = "camelCaseName" <~~ json



    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "name" ~~> self.name,
            "items" ~~> self.items,
            "checkAllEnabled" ~~> self.checkAllEnabled,
            "camelCaseName" ~~> self.camelCaseName


            ])
    }
    
}
