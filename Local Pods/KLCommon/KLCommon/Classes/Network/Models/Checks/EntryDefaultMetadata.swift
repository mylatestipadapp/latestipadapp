//
//  EntryDefaultMetadata.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import Gloss

open class EntryDefaultMetadata: Glossy {
    
    public var id : String?
    public var businessId : String?
    public var locationId : String?
    public var name : String?
    public var isDefault : Bool?
    public var temperature : String?
    public var numberOfFreezers : Int?
    public var numberOfFridges : Int?
    public var minCookingTemp : Float?
    public var hotHoldingTemp : Float?
    public var reHeatingTemp : Float?
    public var fridgeTemp : Int?
    public var foods : [FoodItem]?
    public var createdAt : String?
    public var isActive : Bool?
    public var status : Bool?
    
    init() {
        
    }
    
    public required init?(json: JSON) {
        self.id = "_id" <~~ json
        self.name = "name" <~~ json
        self.businessId = "businessId" <~~ json
        self.locationId = "locationId" <~~ json
        self.isDefault = "isDefault" <~~ json
        self.temperature = "temperature" <~~ json
        self.numberOfFreezers = "numberOfFreezers" <~~ json
        self.numberOfFridges = "numberOfFridges" <~~ json
        self.minCookingTemp = "minCookingTemp" <~~ json
        self.hotHoldingTemp = "hotHoldingTemp" <~~ json
        self.reHeatingTemp = "reHeatingTemp" <~~ json
        self.fridgeTemp = "fridgeTemp" <~~ json
        self.foods = "foods" <~~ json
        self.createdAt = "createdAt" <~~ json
        self.isActive = "isActive" <~~ json
        self.status = "status" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "name" ~~> self.name,
            "businessId" ~~> self.businessId,
            "locationId" ~~> self.locationId,
            "createdAt" ~~> self.createdAt,
            "isDefault" ~~> self.isDefault,
            "numberOfFreezers" ~~> self.numberOfFreezers,
            "temperature" ~~> self.temperature,
            "numberOfFridges" ~~> self.numberOfFridges,
            "minCookingTemp" ~~> self.minCookingTemp,
            "hotHoldingTemp" ~~> self.hotHoldingTemp,
            "reHeatingTemp" ~~> self.reHeatingTemp,
            "fridgeTemp" ~~> self.fridgeTemp,
            "foods" ~~> self.foods,
            "isActive" ~~> self.isActive,
            "status" ~~> self.status
            ])
    }
}
