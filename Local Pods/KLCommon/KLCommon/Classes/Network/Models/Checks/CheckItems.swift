//
//  CheckItems.swift
//  Pods
//
//  Created by Viren Bhandari on 9/15/17.
//
//

import UIKit
import Gloss
open class CheckItems: Decodable, Encodable {
    
    public var id : String?
    public var checkDescription : String?
    public var task : String?
    public var state : String?


    
    init() {
        
    }
    public init(id:String, checkDescription:String,task:String,state:String) {
        self.id = id;
        self.checkDescription = checkDescription;
        self.task = task;
        self.state = state;
    }
    public required init?(json: JSON) {
        self.id = "_id" <~~ json
        self.checkDescription = "description" <~~ json
        self.task = "task" <~~ json

    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "description" ~~> self.checkDescription,
            "task" ~~> self.task,
            "state" ~~> self.state


            ])
    }
}
