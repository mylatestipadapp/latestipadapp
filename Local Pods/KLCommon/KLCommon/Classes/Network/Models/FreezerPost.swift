//
//  Template.swift
//  Pods
//
//  Created by Fitastik on 24/09/17.
//
//

import Foundation
import Gloss

open class FreezerPost: Decodable, Encodable {
    
    public var freezerNum : Int?
    public var temperature : String?
    public var comment : String?
    public var commentRequired : Bool?
    
    init() {
        
    }
    
    public init(freezerNum:Int, temperature:String,comment:String,commentRequired:Bool) {
        self.freezerNum = freezerNum;
        self.temperature = temperature;
        self.comment = comment
        self.commentRequired = commentRequired
    }
    public required init?(json: JSON) {
//        self.id = "_id" <~~ json
//        self.name = "name" <~~ json
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
//            "_id" ~~> self.id,
//            "name" ~~> self.name,
            
            ])
    }
}
