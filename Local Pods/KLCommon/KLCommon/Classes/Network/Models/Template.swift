//
//  Template.swift
//  Pods
//
//  Created by Fitastik on 24/09/17.
//
//

import Foundation
import Gloss

open class Template: Decodable, Encodable {
    
    public var id : String?
    public var name : String?
    
    init() {
        
    }
    
    public init(id:String, name:String) {
        self.id = id;
        self.name = name;
    }
    public required init?(json: JSON) {
        self.id = "_id" <~~ json
        self.name = "name" <~~ json
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "name" ~~> self.name,
            
            ])
    }
}
