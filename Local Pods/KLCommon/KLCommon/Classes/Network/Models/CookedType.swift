//
//  Template.swift
//  Pods
//
//  Created by Fitastik on 24/09/17.
//
//

import Foundation
import Gloss

open class CookedType: Decodable, Encodable {
    
    public var name : String?
    public var temperature : String?
    public var type : String?
    public var commentRequired : Bool?
    public var comment: String?

    
    init() {
        
    }
    
    public init(name:String, temperature:String,comment:String,commentRequired:Bool,type:String) {
        self.name = name;
        self.temperature = temperature;
        self.comment = comment
        self.commentRequired = commentRequired
        self.type = type

    }
    public required init?(json: JSON) {
//        self.id = "_id" <~~ json
//        self.name = "name" <~~ json
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
//            "_id" ~~> self.id,
//            "name" ~~> self.name,
            
            ])
    }
}
