//
//  ViewLogsPost.swift
//  Pods
//
//  Created by Viren Bhandari on 16/09/17.
//
//

import UIKit
import Gloss


open class FoodPost: Decodable, Encodable {

    public var template : String?
    public var entryTime : String?
    public var entryHrs : String?
    public var entryMins : String?
    public var locationId : String?
    public var businessId : String?

    public var cooked = "null"
    public var hotHolding = "null"
    public var reHeating = "null"
    public var assetEntries : [ApplianceParam]?
    public var foodEntries = "null"
    public var createdBy : [CreatedBy]?

    public var completed :Bool?


    public var entryhrs = "null"
    public var entrymins = "null"



    init() {
        
    }
    
    public init(templates:String,entryTm:String,entryMin:String,locationIds:String,entryHr:String,buisnessId:String,assetEntries:[ApplianceParam],completed:Bool,createdBy:[CreatedBy]) {
        
        self.locationId = locationIds;
        self.businessId = buisnessId;
        self.assetEntries = assetEntries
        self.completed = completed

        self.template = templates;
      

        self.entryTime = entryTm
        self.entryHrs = entryHr
        self.entryMins = entryMin
        self.createdBy = createdBy;
      
        
        
    }
    
    public required init?(json: JSON) {
            self.locationId = "locationId" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "locationId" ~~> self.locationId,"businessId" ~~> self.businessId,"assetEntries" ~~> self.assetEntries,"completed" ~~> self.completed,"template" ~~> self.template,"entryTime" ~~> self.entryTime,"entryHrs" ~~> self.entryHrs,"entryMins" ~~> self.entryMins,"createdBy" ~~> self.createdBy
            ])
    }
}
