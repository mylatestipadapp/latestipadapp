//
//  ViewLogsPost.swift
//  Pods
//
//  Created by Viren Bhandari on 16/09/17.
//
//

import UIKit
import Gloss

open class ViewLogsPost: Decodable, Encodable {

    public var locationId : String?
    public var businessId : String?

    init() {
        
    }
    
    public init(locationId:String, businessId:String) {
        self.locationId = locationId;
        self.businessId = businessId;
    }
    
    public required init?(json: JSON) {
            self.locationId = "locationId" <~~ json
        self.businessId = "businessId" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "locationId" ~~> self.locationId,
            "businessId" ~~> self.businessId
            ])
    }
}
