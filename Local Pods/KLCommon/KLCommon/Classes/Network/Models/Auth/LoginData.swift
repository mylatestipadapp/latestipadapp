//
//  LoginData.swift
//  Pods
//
//  Created by Viren Bhandari on 9/21/17.
//
//

import UIKit
import Gloss

open class LoginData: Encodable, Decodable {
    public var token : String?
    public var user : User?
    
    init() {
        
    }
    
    public required init?(json: JSON) {
        self.user = "user" <~~ json
        self.token = "token" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "token" ~~> self.token,
            "user" ~~> self.user
            ])
    }
}
