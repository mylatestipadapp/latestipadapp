//
//  ForgotPasswordPost.swift
//  KitchenLogs
//
//  Created by Viren Bhandari on 9/8/17.
//  Copyright © 2017 KitchenLogs. All rights reserved.
//

import UIKit
import Gloss

open class ForgotPasswordPost : Encodable, Decodable {
    
    public var email : String?
    
    public init(email:String) {
        self.email = email
    }
    
    public required init?(json: JSON) {
        self.email = "email" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "email" ~~> self.email
            ])
    }
    
}
