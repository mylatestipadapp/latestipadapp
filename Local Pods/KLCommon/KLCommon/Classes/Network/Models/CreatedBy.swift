//
//  Template.swift
//  Pods
//
//  Created by Fitastik on 24/09/17.
//
//

import Foundation
import Gloss

open class CreatedBy: Decodable, Encodable {
    
    public var authenticated : Bool?
    public var name : String?
    public var businessId : String?
    public var userType : String?
    
    init() {
        
    }
    
    public init(userType:String, name:String,businessId:String,authenticated:Bool) {
        self.userType = userType;
        self.name = name;
        self.businessId = businessId
        self.authenticated = authenticated
    }
    public required init?(json: JSON) {
//        self.id = "_id" <~~ json
//        self.name = "name" <~~ json
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
//            "_id" ~~> self.id,
//            "name" ~~> self.name,
            
            ])
    }
}
