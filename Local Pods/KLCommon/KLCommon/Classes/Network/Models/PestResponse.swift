//
//  PestResponse.swift
//  Pods
//
//  Created by Manish Saini on 24/09/17.
//
//

import UIKit
import Gloss

open class PestResponse: Decodable, Encodable {
    public var code : Int!
    public var message : String!
    //public var data : [ViewLogs]!
    
    init() {
        
    }
    public required init?(json: JSON) {
        self.code = "code" <~~ json
        self.message = "message" <~~ json
        //self.data = "data" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "code" ~~> self.code,
            "message" ~~> self.message
            //"data" ~~> self.data
            ])
    }
    
    
}
