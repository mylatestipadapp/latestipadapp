#
# Be sure to run `pod lib lint KLCommon.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'KLCommon'
  s.version          = '0.1.0'
  s.summary          = 'KitchenLogs Common is pod for common code between iPhone and iPad app.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/<GITHUB_USERNAME>/KLCommon'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Viren R. Bhandari' => 'virenrb09@gmail.com' }
  s.source           = { :git => 'https://github.com/<GITHUB_USERNAME>/KLCommon.git', :tag => s.version.to_s }

    s.ios.deployment_target = '9.0'
    s.source_files = 'KLCommon/**/*.{h,m,swift}'
    s.resource_bundles = { 'KLCommon' => 'Resources/*' }
    s.dependency 'Toast-Swift', '~> 2.0.0'
    s.dependency 'SDWebImage', '~> 3.8'
    s.dependency 'Alamofire', '~> 4.0'
    s.dependency 'Alamofire-Gloss'
    s.dependency 'DropDown'
    s.dependency 'AlamofireNetworkActivityLogger', '~> 2.0'
    s.dependency 'Firebase/Core'
    s.dependency 'Firebase/Messaging'
    s.dependency 'SwiftSpinner'
    s.dependency 'Firebase/Core'
    s.dependency 'RealmSwift'

end
